// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return prefer_self_in_static_references

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  internal enum App {
    internal enum Common {
      /// Localizable.strings
      ///   NimbleSurveys
      /// 
      ///   Created by Tuan Pham on 05/01/2024.
      internal static let title = L10n.tr("Localizable", "app.common.title", fallback: "Nimble")
      internal enum Button {
        /// Login
        internal static let login = L10n.tr("Localizable", "app.common.button.login", fallback: "Login")
      }
      internal enum EmptyState {
        internal enum Error {
          /// Reload
          internal static let actionTitle = L10n.tr("Localizable", "app.common.emptyState.error.actionTitle", fallback: "Reload")
          /// Something went wrong. Please try again!
          internal static let desc = L10n.tr("Localizable", "app.common.emptyState.error.desc", fallback: "Something went wrong. Please try again!")
          /// Oops!
          internal static let title = L10n.tr("Localizable", "app.common.emptyState.error.title", fallback: "Oops!")
        }
      }
      internal enum Error {
        internal enum InternalServerError {
          /// Internal server error
          internal static let desc = L10n.tr("Localizable", "app.common.error.internalServerError.desc", fallback: "Internal server error")
        }
        internal enum InvalidRefreshToken {
          /// Token expired
          internal static let desc = L10n.tr("Localizable", "app.common.error.invalidRefreshToken.desc", fallback: "Token expired")
        }
        internal enum UnAuthorized {
          /// UnAuthorization error
          internal static let desc = L10n.tr("Localizable", "app.common.error.unAuthorized.desc", fallback: "UnAuthorization error")
        }
        internal enum Unknown {
          /// Unknown error
          internal static let desc = L10n.tr("Localizable", "app.common.error.unknown.desc", fallback: "Unknown error")
        }
      }
      internal enum ValidationError {
        /// Invalid email
        internal static let invalidEmail = L10n.tr("Localizable", "app.common.validationError.invalidEmail", fallback: "Invalid email")
      }
    }
    internal enum Details {
      /// Start surveys
      internal static let startSurveys = L10n.tr("Localizable", "app.details.startSurveys", fallback: "Start surveys")
    }
    internal enum ForgotPassword {
      /// Enter your email to receive instructions for resetting your password.
      internal static let description = L10n.tr("Localizable", "app.forgotPassword.description", fallback: "Enter your email to receive instructions for resetting your password.")
      internal enum Button {
        /// Reset
        internal static let reset = L10n.tr("Localizable", "app.forgotPassword.button.reset", fallback: "Reset")
      }
      internal enum Fields {
        /// Email
        internal static let email = L10n.tr("Localizable", "app.forgotPassword.fields.email", fallback: "Email")
      }
      internal enum Notification {
        internal enum CheckEmail {
          /// Check your email
          internal static let title = L10n.tr("Localizable", "app.forgotPassword.notification.checkEmail.title", fallback: "Check your email")
        }
      }
    }
    internal enum Home {
      internal enum Today {
        /// Today
        internal static let title = L10n.tr("Localizable", "app.home.today.title", fallback: "Today")
      }
    }
    internal enum Login {
      internal enum Button {
        /// Forgot?
        internal static let forgotPassword = L10n.tr("Localizable", "app.login.button.forgotPassword", fallback: "Forgot?")
      }
      internal enum Fields {
        internal enum Email {
          /// Email
          internal static let placeholder = L10n.tr("Localizable", "app.login.fields.email.placeholder", fallback: "Email")
        }
        internal enum Password {
          /// Password
          internal static let placeholder = L10n.tr("Localizable", "app.login.fields.password.placeholder", fallback: "Password")
        }
      }
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg..., fallback value: String) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: value, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
