//
//  AppDelegate+Injection.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Resolver

extension Resolver: ResolverRegistering {
    
    public static func registerAllServices() {
        
        register {
            CredentialsProvider.default
        }
        .implements(CredentialsProviderProtocol.self)
        .scope(.graph)
        
        register {
            RefreshTokenTargetProvider()
        }
        .implements(RefreshTokenTargetProviderProtocol.self)
        .scope(.graph)
        
        register {
            APIClient(
                provider: .default,
                credentialsProvider: resolve(),
                refreshTokenTargetProvider: resolve()
            )
        }
        .implements(APIClientProtocol.self)
        .scope(.graph)
     
        register {
            LaunchCleaner()
        }
        .implements(LaunchCleanerProtocol.self)
        .scope(.graph)
        
        /// Navigator
        
        register {
            AppViewControllerFactory()
        }
        .implements(AppViewControllerFactoryProtocol.self)
        .scope(.graph)
        
        register {
            AppNavigator(controllerFactory: resolve())
        }
        .implements(AppNavigatorProtocol.self)
        .scope(.graph)
        
        register {
            AuthenticationService(apiClient: resolve(), credentialsProvider: resolve())
        }
        .implements(AuthenticationServiceProtocol.self)
        .scope(.graph)
        
        register {
            AuthenticationRepository(service: resolve())
        }
        .implements(AuthenticationRepositoryProtocol.self)
        .scope(.graph)
        
        register {
            DataValidator()
        }
        .implements(DataValidatorProtocol.self)
        .scope(.graph)
        
        register {
            LoginRouter(navigator: resolve())
        }
        .implements(LoginRouterProtocol.self)
        .scope(.unique)
        
        register {
            LoginViewModel(
                repository: resolve(),
                dataValidator: resolve()
            )
        }
        .implements(LoginViewModelProtocol.self)
        .scope(.unique)
        
        register {
            SurveyService(apiClient: resolve())
        }
        .implements(SurveyServiceProtocol.self)
        .scope(.graph)
        
        register {
            SurveyRepository(service: resolve())
        }
        .implements(SurveyRepositoryProtocol.self)
        .scope(.graph)
        
        register {
            HomeViewModel(surveyRepository: resolve(), authRepository: resolve())
        }
        .implements(HomeViewModelProtocol.self)
        .scope(.unique)
        
        register {
            ErrorHandler()
        }
        .implements(ErrorHandlerProtocol.self)
        .scope(.graph)
        
        register {
            HomeRouter(
                errorHandler: resolve(),
                navigator: resolve()
            )
        }
        .implements(HomeRouterProtocol.self)
        .scope(.unique)
        
        register {
            ForgotPasswordViewModel(
                authenticationRepository: resolve(),
                dataValidator: resolve()
            )
        }
        .implements(ForgotPasswordViewModelProtocol.self)
        .scope(.unique)
        
        register {
            DSMessagePresenter()
        }
        .implements(DSMessagePresenterProtocol.self)
        .scope(.graph)
        
        register {
            ForgotPasswordRouter(messagePresenter: resolve())
        }
        .implements(ForgotPasswordRouterProtocol.self)
        .scope(.unique)
    }
}
