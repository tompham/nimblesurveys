//
//  AppDelegate.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 05/01/2024.
//

import UIKit
import Resolver

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    @LazyInjected var navigator: AppNavigatorProtocol
    
    private lazy var externalDelegates: [UIApplicationDelegate] = [
        LaunchConstructor(cleaner: Resolver.resolve())
    ]
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        Resolver.registerAllServices()
        externalDelegates.forEach { _ = $0.application?(application, didFinishLaunchingWithOptions: launchOptions) }
        
        if AuthenticationState.shared.isAuthenticated {
            navigator.show(route: .home, from: nil, action: .root)
        } else {
            navigator.show(route: .login, from: nil, action: .root)
        }
        
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
}
