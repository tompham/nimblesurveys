//
//  AnyOptional.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 11/01/2024.
//

import Foundation

/// Allows to match for optionals with generics that are defined as non-optional.
public protocol AnyOptional {
    /// Returns `true` if `nil`, otherwise `false`.
    var isNil: Bool { get }
}
