//
//  LaunchCleaner.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 11/01/2024.
//

import Foundation
import Resolver

final class LaunchCleaner: LaunchCleanerProtocol {
    
    static let shared = LaunchCleaner()
    
    @LazyInjected var credentialsProvider: CredentialsProviderProtocol
    
    func clean() {
        credentialsProvider.clearCredentials()
    }
}
