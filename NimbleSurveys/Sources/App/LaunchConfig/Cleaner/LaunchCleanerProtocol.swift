//
//  LaunchCleanerProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 11/01/2024.
//

import Foundation

protocol LaunchCleanerProtocol: AnyObject {
    
    func clean()
}
