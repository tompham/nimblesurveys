//
//  LaunchConfigUserDefault.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 11/01/2024.
//

import Foundation

@propertyWrapper
struct LaunchConfigUserDefault<Value: LaunchConfigValueProtocol> {
    let key: String
    let defaultValue: Value
    var container: UserDefaults = .standard

    var wrappedValue: Value {
        get {
            guard let stringData =  container.object(forKey: key) as? String else {
                return defaultValue
            }
            
            return Value(stringData: stringData)
        }
        
        set {
            // Check whether we're dealing with an optional and remove the object if the new value is nil.
            if let optional = newValue as? AnyOptional, optional.isNil {
                container.removeObject(forKey: key)
            } else {
                container.set(newValue.stringValue, forKey: key)
            }
        }
    }

    var projectedValue: Bool {
        return true
    }
}

