//
//  LaunchConfigValueProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 11/01/2024.
//

import Foundation

protocol LaunchConfigValueProtocol {
    
    init(stringData: String)
    
    var stringValue: String { get }
}

extension Bool: LaunchConfigValueProtocol {
    
    init(stringData: String) {
        self = stringData == "true"
    }
    
    var stringValue: String {
        return self ? "true" : "false"
    }
}

extension Bool {
    func toString() -> String {
        self ? "true" : "false"
    }
}
