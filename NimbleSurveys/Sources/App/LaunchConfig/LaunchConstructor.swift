//
//  LaunchConfig.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 11/01/2024.
//

import UIKit

final class LaunchConstructor: NSObject {
    
    // MARK: - Dependencies
    
    let configs: UITestLaunchConfigs
    let cleaner: LaunchCleanerProtocol
    
    // MARK: - Initializers
    
    init(configs: UITestLaunchConfigs = UITestLaunchConfigs(), cleaner: LaunchCleanerProtocol) {
        self.configs = configs
        self.cleaner = cleaner
    }
    
    func setup() {
        setupFreshStart()
    }
    
    func setupFreshStart() {
        if configs.toFreshStart {
            cleaner.clean()
        }
    }
}

// MARK: - UIApplicationDelegate

extension LaunchConstructor: UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        setup()
        return true
    }
}
