//
//  Optional.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 11/01/2024.
//

import Foundation

extension Optional: AnyOptional {
    public var isNil: Bool { self == nil }
}
