//
//  UITestLaunchConfigs.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 11/01/2024.
//

import Foundation

final class UITestLaunchConfigs {
    
    @LaunchConfigUserDefault(key: "ui_testing_is_testing_enabled", defaultValue: false)
    var isTestingEnabled: Bool

    @LaunchConfigUserDefault(key: "ui_testing_to_fresh_start", defaultValue: false)
    var toFreshStart: Bool
    
    /// Build launch arguments for
    public func launchArguments() -> [String] {
        var arguments = [String]()
        arguments += ["-ui_testing_to_fresh_start", toFreshStart.toString()]
        arguments += ["-ui_testing_is_testing_enabled", isTestingEnabled.toString()]
        return arguments
    }
}

