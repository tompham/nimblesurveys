//
//  TokenResponse.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import Japx

struct CredentialsResponse {
    
    // MARK: - Properties

    /// The unique identifier of the resource.
    let id: String

    /// The type identifier of the resource.
    let type: String

    /// The access token.
    let accessToken: String?

    /// The token type.
    let tokenType: String?

    /// The number of seconds until the access token expires.
    let expiresIn: Double?

    /// The refresh token.
    let refreshToken: String?

    /// The time the access token was created.
    let createdAt: Double?
}

// MARK: - Decodable

extension CredentialsResponse: JapxCodable {
    
    private enum CodingKeys: String, CodingKey {
        case id
        case type
        case accessToken = "access_token"
        case tokenType = "token_type"
        case expiresIn = "expires_in"
        case refreshToken = "refresh_token"
        case createdAt = "created_at"
    }
}

extension CredentialsResponse: Encodable {}
