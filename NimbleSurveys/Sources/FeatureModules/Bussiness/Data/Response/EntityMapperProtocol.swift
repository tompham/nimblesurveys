//
//  EntityMapperProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import Foundation

protocol EntityMapperProtocol {
    
    associatedtype EntityType
    func map() -> EntityType
}
