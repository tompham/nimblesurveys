//
//  ListMetaDataResponse.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 08/01/2024.
//

import Foundation

struct ListMetaDataResponse {
    
    // MARK: - Properties
    
    /// The current page number
    let page: Int?
    
    /// The total number of pages
    let pages: Int?
    
    /// The number of records per page
    let pageSize: Int?
    
    /// The total number of records
    let records: Int?
}

// MARK: - Decodable

extension ListMetaDataResponse: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case page
        case pages
        case pageSize = "page_size"
        case records
    }
}

// MARK: - EntityMapperProtocol

extension ListMetaDataResponse: EntityMapperProtocol {
    
    func map() -> ListMetaDataEntity {
        return ListMetaDataEntity(
            page: page.or(.zero),
            pages: pages.or(.zero),
            pageSize: pageSize.or(.zero),
            records: records.or(.zero)
        )
    }
}
