//
//  MetaDataResponse.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 09/01/2024.
//

import Foundation

struct MetaDataResponse {
    
    let message: String?
}

extension MetaDataResponse: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case message
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        message = container.decodeIfPossible(key: .message)
    }
}

extension MetaDataResponse: EntityMapperProtocol {
    
    func map() -> MetaDataEntity {
        return .init(message: message)
    }
}
