//
//  SurveyListResponse.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import Foundation

struct SurveyListResponse {
    let data: [SurveyResponse]?
    let meta: ListMetaDataResponse?
}

extension SurveyListResponse: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case data
        case meta
    }
}

extension SurveyListResponse: EntityMapperProtocol {
    func map() -> SurveyListEntity {
        return .init(
            data: data.or([]).map { $0.map() },
            meta: meta?.map()
        )
    }
}
