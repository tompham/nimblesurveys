//
//  SurveyResponse.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import Foundation
import Japx

struct SurveyResponse {
    
    let id: String
    let type: String
    let title: String?
    let description: String?
    let isActive: Bool?
    let coverImageUrl: String?
    let surveyType: String?
}

// MARK: - JapxDecodable

extension SurveyResponse: JapxDecodable {
    
    private enum CodingKeys: String, CodingKey {
        case id
        case type
        case title
        case description
        case isActive = "is_active"
        case coverImageUrl = "cover_image_url"
        case surveyType = "survey_type"
    }
}

// MARK: - EntityMapperProtocol

extension SurveyResponse: EntityMapperProtocol {
    
    func map() -> SurveyEntity {
        return .init(
            title: title,
            description: description,
            isActive: isActive,
            coverImageUrl: coverImageUrl,
            surveyType: surveyType
        )
    }
}
