//
//  UserProfileResponse.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import Foundation
import Japx

struct UserProfileResponse: JapxDecodable {
    var type: String
    var id: String
    let email: String?
    let name: String?
    let avatarUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case type
        case email
        case name
        case avatarUrl = "avatar_url"
    }
}

extension UserProfileResponse: EntityMapperProtocol {
    
    func map() -> UserProfileEntity {
        return .init(
            type: type,
            id: id,
            email: email,
            name: name,
            avatarUrl: avatarUrl
        )
    }
}
