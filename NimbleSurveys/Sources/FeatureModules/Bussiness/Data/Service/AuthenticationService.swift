//
//  AuthenticationService.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import RxSwift
import Japx

final class AuthenticationService {
    
    static let `default` = AuthenticationService(
        apiClient: APIClient.default,
        credentialsProvider: CredentialsProvider.default
    )
    
    // MARK: - Dependencies

    private let apiClient: APIClientProtocol
    private let credentialsProvider: CredentialsProviderProtocol
    
    // MARK: - Initializers
    
    init(
        apiClient: APIClientProtocol,
        credentialsProvider: CredentialsProviderProtocol
    ) {
        self.apiClient = apiClient
        self.credentialsProvider = credentialsProvider
    }
}

// MARK: - AuthenticationServiceProtocol

extension AuthenticationService: AuthenticationServiceProtocol {
    
    func authenticate(email: String, password: String) -> Observable<CredentialsResponse> {
        let target = LoginTarget(email: email, password: password)
        return apiClient.request(target: target)
            .asObservable()
            .do(onNext: { [weak self] credentialsData in
                guard let self else { return }
                self.credentialsProvider.store(
                    credentials: credentialsData
                )
            })
            .decode(key: .data, decoder: JapxDecoder())
    }
    
    func fetchUserProfile() -> Observable<UserProfileResponse> {
        let target = UserProfileTarget()
        return apiClient.request(target: target)
            .asObservable()
            .decode(key: .data, decoder: JapxDecoder())
    }
    
    func forgotPassword(for email: String) -> Observable<MetaDataResponse> {
        let target = ForgotPasswordTarget(email: email)
        return apiClient.request(target: target)
            .asObservable()
            .decode(key: .meta)
    }
}
