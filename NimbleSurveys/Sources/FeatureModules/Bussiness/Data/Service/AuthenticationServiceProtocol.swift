//
//  AuthenticationServiceProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import RxSwift

protocol AuthenticationServiceProtocol: AnyObject {
    
    /// Login with email and password
    /// 
    /// - Parameters:
    ///  - email: The email.
    /// - password: The password.
    /// 
    /// - Returns: The observable of `CredentialsResponse`
    func authenticate(email: String, password: String) -> Observable<CredentialsResponse>
    
    func fetchUserProfile() -> Observable<UserProfileResponse>
    
    func forgotPassword(for email: String) -> Observable<MetaDataResponse>
}
