//
//  APIPath.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation

enum APIPath {
    
    enum V1 {
        
        private static let prefix = "/api/v1"
        static let login = "\(prefix)/oauth/token"
    }
}
