//
//  APISecretInfo.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation

enum APISecretInfo {
    
    static let clientID = getInfoPlistStringBy(key: "APP_CLIENT_ID")
    static let clientSecret = getInfoPlistStringBy(key: "APP_CLIENT_SECRET")
    
    static private func getInfoPlistStringBy(key: String) -> String {
        guard let value = Bundle.main.object(forInfoDictionaryKey: key) as? String
        else { return .empty }
        
        return value
    }
}
