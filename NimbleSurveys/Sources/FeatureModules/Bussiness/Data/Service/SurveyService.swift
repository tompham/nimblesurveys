//
//  SurveyService.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import Foundation
import RxSwift
import Japx

final class SurveyService {
    
    static let `default` = SurveyService(
        apiClient: APIClient.default
    )
    
    // MARK: - Dependencies

    private let apiClient: APIClientProtocol
    
    // MARK: - Initializers
    
    init(
        apiClient: APIClientProtocol
    ) {
        self.apiClient = apiClient
    }
}

extension SurveyService: SurveyServiceProtocol {
    
    func fetchSurveyList(pageNumber: Int, pageSize: Int) -> Observable<SurveyListResponse> {
        let target = FetchSurveyListTarget(pageNumber: pageNumber, pageSize: pageSize)
        return apiClient.request(target: target)
            .asObservable()
            .decode(decoder: JapxDecoder())
    }
}
