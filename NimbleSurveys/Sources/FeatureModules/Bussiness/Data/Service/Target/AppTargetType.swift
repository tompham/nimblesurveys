//
//  AppTargetType.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import Moya

protocol AppTargetType: TargetType {}

extension AppTargetType {
    
    var isUITestingEnabled: Bool {
        return UITestLaunchConfigs().isTestingEnabled
    }
    
    var baseURL: URL {
        if isUITestingEnabled {
            return URL(string: "http://localhost:8080")!
        }
        
        return URL(string: "https://survey-api.nimblehq.co")!
    }
    
    var headers: [String: String]? {
        return nil
    }
}
