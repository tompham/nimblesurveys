//
//  FetchSurveyListTarget.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import Foundation
import Moya

struct FetchSurveyListTarget {
    
    let pageNumber: Int
    let pageSize: Int
}

extension FetchSurveyListTarget: AppTargetType {
    var path: String {
        return "api/v1/surveys"
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Task {
        return .requestParameters(
            parameters: [
                "page[number]": pageNumber,
                "page[size]": pageSize
            ],
            encoding: URLEncoding.queryString
        )
    }
}

extension FetchSurveyListTarget: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType? {
        return .bearer
    }
}
