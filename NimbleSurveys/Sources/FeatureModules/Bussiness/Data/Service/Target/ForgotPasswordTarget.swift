//
//  ForgotPasswordTarget.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 09/01/2024.
//

import Foundation
import Moya

struct ForgotPasswordTarget: AppTargetType {
    
    let email: String
    
    var path: String {
        return "api/v1/passwords"
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var task: Task {
        return .requestParameters(
            parameters: [
                "user": [
                    "email": email
                ],
                "client_id": APISecretInfo.clientID,
                "client_secret": APISecretInfo.clientSecret
            ],
            encoding: URLEncoding.httpBody
        )
    }
}
