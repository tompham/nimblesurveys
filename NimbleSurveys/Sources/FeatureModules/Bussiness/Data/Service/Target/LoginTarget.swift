//
//  LoginTarget.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import Moya

struct LoginTarget: AppTargetType {
    
    let email: String
    let password: String
    
    var path: String {
        return APIPath.V1.login
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var task: Task {
        return .requestParameters(
            parameters: [
                "grant_type": APIRequestGrantType.password,
                "email": email,
                "password": password,
                "client_id": APISecretInfo.clientID,
                "client_secret": APISecretInfo.clientSecret
            ],
            encoding: URLEncoding.httpBody
        )
    }
}
