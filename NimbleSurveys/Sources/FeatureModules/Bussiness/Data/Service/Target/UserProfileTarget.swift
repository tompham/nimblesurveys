//
//  UserProfileTarget.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import Foundation
import Moya

struct UserProfileTarget: AppTargetType, AccessTokenAuthorizable {
    
    var path: String {
        return "api/v1/me"
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var authorizationType: AuthorizationType? {
        return .bearer
    }
}
