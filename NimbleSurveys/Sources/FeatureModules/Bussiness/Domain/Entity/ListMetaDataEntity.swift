//
//  ListMetaDataEntity.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 08/01/2024.
//

import Foundation

struct ListMetaDataEntity {
    
    let page: Int
    let pages: Int
    let pageSize: Int
    let records: Int
}
