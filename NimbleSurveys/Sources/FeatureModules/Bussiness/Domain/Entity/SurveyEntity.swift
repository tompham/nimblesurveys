//
//  SurveyEntity.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import Foundation

struct SurveyEntity {
    
    let title: String?
    let description: String?
    let isActive: Bool?
    let coverImageUrl: String?
    let surveyType: String?
    
    init(
        title: String?,
        description: String?,
        isActive: Bool?,
        coverImageUrl: String?,
        surveyType: String?
    ) {
        self.title = title
        self.description = description
        self.isActive = isActive
        self.coverImageUrl = coverImageUrl
        self.surveyType = surveyType
    }
}

extension SurveyEntity: Equatable {}
