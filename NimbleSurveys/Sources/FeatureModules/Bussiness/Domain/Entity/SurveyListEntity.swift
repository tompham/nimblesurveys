//
//  SurveyListEntity.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import Foundation

struct SurveyListEntity {
    let data: [SurveyEntity]?
    let meta: ListMetaDataEntity?
}
