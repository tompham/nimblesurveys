//
//  UserProfileEntity.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import Foundation

struct UserProfileEntity {
    var type: String
    var id: String
    let email: String?
    let name: String?
    let avatarUrl: String?
    
    init(type: String, id: String, email: String?, name: String?, avatarUrl: String?) {
        self.type = type
        self.id = id
        self.email = email
        self.name = name
        self.avatarUrl = avatarUrl
    }
}
