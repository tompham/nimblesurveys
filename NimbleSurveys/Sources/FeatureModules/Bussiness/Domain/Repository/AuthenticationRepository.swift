//
//  AuthenticationRepository.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import RxSwift

final class AuthenticationRepository {
    
    let service: AuthenticationServiceProtocol
    
    init(service: AuthenticationServiceProtocol) {
        self.service = service
    }
}

extension AuthenticationRepository: AuthenticationRepositoryProtocol {
    
    func authenticate(email: String, password: String) -> Observable<Void> {
        return service.authenticate(email: email, password: password)
            .map { _ in () }
    }
    
    func fetchUserProfile() -> Observable<UserProfileEntity> {
        return service.fetchUserProfile().map { $0.map() }
    }
    
    func forgotPassword(for email: String) -> Observable<MetaDataEntity> {
        return service.forgotPassword(for: email).map { $0.map() }
    }
}
