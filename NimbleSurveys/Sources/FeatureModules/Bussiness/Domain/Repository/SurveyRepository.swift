//
//  SurveyRepository.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import Foundation
import RxSwift

final class SurveyRepository {
    
    let service: SurveyServiceProtocol
    
    init(service: SurveyServiceProtocol) {
        self.service = service
    }
}

extension SurveyRepository: SurveyRepositoryProtocol {
 
    func fetchSurveyList(pageNumber: Int, pageSize: Int) -> Observable<SurveyListEntity> {
        return service.fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
            .map { $0.map() }
    }
}
