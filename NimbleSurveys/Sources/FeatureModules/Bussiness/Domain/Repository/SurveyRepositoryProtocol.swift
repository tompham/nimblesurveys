//
//  SurveyRepositoryProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import Foundation
import RxSwift

protocol SurveyRepositoryProtocol: AnyObject {
    
    func fetchSurveyList(pageNumber: Int, pageSize: Int) -> Observable<SurveyListEntity>
}
