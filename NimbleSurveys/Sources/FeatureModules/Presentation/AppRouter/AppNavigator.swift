//
//  AppNavigator.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import UIKit
import Resolver

final class AppNavigator {
    
    let controllerFactory: AppViewControllerFactoryProtocol
    
    init(controllerFactory: AppViewControllerFactoryProtocol) {
        self.controllerFactory = controllerFactory
    }
}

extension AppNavigator: AppNavigatorProtocol {
    
    func show(route: Route, from parent: UIViewController?, action: NavigateAction) {
        
        let viewController = controllerFactory.createController(for: route)
        
        switch action {
        case .push:
            parent?.navigationController?.pushViewController(viewController, animated: true)
            
        case .modal:
            parent?.navigationController?.present(
                DSNavigationController(rootViewController: viewController),
                animated: true
            )
        case .root:
            UIApplication.shared.keyWindow?.rootViewController = DSNavigationController(rootViewController: viewController)
        }
    }
}
