//
//  AppNavigatorProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 08/01/2024.
//

import UIKit

protocol AppNavigatorProtocol: AnyObject {
    
    func show(route: Route, from parent: UIViewController?, action: NavigateAction)
}
