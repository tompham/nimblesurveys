//
//  AppViewControllerFactory.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 08/01/2024.
//

import UIKit
import Resolver

final class AppViewControllerFactory: AppViewControllerFactoryProtocol {
    
    init() {}
    
    func createController(for route: Route) -> UIViewController {
        switch route {
        case .login:
            return LoginViewController(
                viewModel: Resolver.resolve(),
                router: Resolver.resolve()
            )
            
        case .forgotPassword:
            return ForgotPasswordViewController(
                viewModel: Resolver.resolve(),
                router: Resolver.resolve()
            )
            
        case .home:
            return HomeViewController(
                viewModel: Resolver.resolve(),
                router: Resolver.resolve()
            )
            
        case .details(let entity):
            return SurveyDetailsViewController(
                viewModel: SurveyDetailsViewModel(entity: entity)
            )
        }
    }
}
