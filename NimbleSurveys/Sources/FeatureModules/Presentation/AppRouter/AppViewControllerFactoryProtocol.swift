//
//  AppViewControllerFactoryProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 08/01/2024.
//

import UIKit

protocol AppViewControllerFactoryProtocol: AnyObject {
    
    func createController(for route: Route) -> UIViewController
}
