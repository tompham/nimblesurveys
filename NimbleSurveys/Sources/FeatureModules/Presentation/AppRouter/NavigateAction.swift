//
//  NavigateAction.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 08/01/2024.
//

import Foundation

enum NavigateAction: Int, Equatable {
    
    case push = 0
    case modal
    case root
}
