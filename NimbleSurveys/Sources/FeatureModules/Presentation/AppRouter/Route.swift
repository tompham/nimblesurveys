//
//  Route.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 08/01/2024.
//

import Foundation

enum Route {
    case login
    case forgotPassword
    case home
    case details(entity: SurveyEntity)
}

extension Route: Equatable {}
