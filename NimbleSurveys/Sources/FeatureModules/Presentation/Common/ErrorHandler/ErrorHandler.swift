//
//  ErrorHandler.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 08/01/2024.
//

import Foundation
import Resolver

final class ErrorHandler {
    
    // MARK: - Dependencies
    
    @LazyInjected var navigator: AppNavigatorProtocol
}

// MARK: - ErrorHandlerProtocol

extension ErrorHandler: ErrorHandlerProtocol {
    
    func handleError(error: Error, on controller: UIViewController) {
        if let apiError = error as? APIError {
            handleAPIError(error: apiError, on: controller)
            return
        }
        
        /// give the error back to the controller to handle the error by itself
    }
    
    private func handleAPIError(error: APIError, on controller: UIViewController) {
//        swi
//        switch error {
//        case .badRequest:
//            break
//            
//        case .unAuthorized:
//            navigator.show(route: .login, from: controller, action: .root)
//            
//        case .notFound:
//            break
//            
//        case .parsing:
//            break
//            
//        case .unknown:
//            break
//            
//        case .underlying(let error):
//            break
//        }
    }
}
