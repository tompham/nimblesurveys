//
//  ErrorHandlerProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 08/01/2024.
//

import Foundation
import Resolver

protocol ErrorHandlerProtocol: AnyObject {
    
    func handleError(error: Error, on controller: UIViewController)
}
