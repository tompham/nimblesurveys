//
//  ForgotPasswordRouter.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 09/01/2024.
//
import SwiftMessages

final class ForgotPasswordRouter {
    
    let messagePresenter: DSMessagePresenterProtocol
    init(messagePresenter: DSMessagePresenterProtocol) {
        self.messagePresenter = messagePresenter
    }
}

extension ForgotPasswordRouter: ForgotPasswordRouterProtocol {
    
    func showErrorMessage(_ message: String) {
        messagePresenter.showErrorMessage(message)
    }
    
    func showConfirmForgotPasswordMessage(_ message: String) {
        messagePresenter.showNotificationView(title: L10n.App.ForgotPassword.Notification.CheckEmail.title, message: message)
    }
}
