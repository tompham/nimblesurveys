//
//  ForgotPasswordRouterProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 09/01/2024.
//

import UIKit

protocol ForgotPasswordRouterProtocol: AnyObject {
    
    func showErrorMessage(_ message: String)
    
    func showConfirmForgotPasswordMessage(_ message: String)
}
