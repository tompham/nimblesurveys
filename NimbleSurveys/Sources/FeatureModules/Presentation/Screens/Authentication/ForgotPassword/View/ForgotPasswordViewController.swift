//
//  ForgotPasswordViewController.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 09/01/2024.
//

import UIKit
import RxSwift
import RxCocoa

class ForgotPasswordViewController: UIViewController {

    // MARK: - UI
    
    private lazy var backgroundImageView = makeBackgroundImageView()
    private lazy var gradientBackgroundView = makeGradientBackgroundView()
    private lazy var blurView = makeBlurView()
    private lazy var logoImageContainerView = makeLogoImageContainerView()
    private lazy var headerContainerView = makeHeaderContainerView()
    private lazy var logoImageView = makeLogoImageView()
    private lazy var descriptionLabel = makeDescriptionLabel()
    private lazy var formStackView = makeFormStackView()
    private lazy var emailTextField = makeEmailTextField()
    private lazy var resetButton = makeResetButton()
    
    // MARK: - Constraints
    
    private lazy var formStackViewCenterYConstraint = makeFormStackViewCenterYConstraint()
    private lazy var formStackViewBottomConstraint = makeFormStackViewBottomConstraint()
    
    // MARK: - Dependencies
    
    private let viewModel: ForgotPasswordViewModelProtocol
    private let router: ForgotPasswordRouterProtocol
    private let disposeBag = DisposeBag()
    
    // MARK: - Initializers
    
    init(viewModel: ForgotPasswordViewModelProtocol, router: ForgotPasswordRouterProtocol) {
        self.viewModel = viewModel
        self.router = router
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - View - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        registerForKeyboardNotifications()
        bindingViewModel()
    }
}

extension ForgotPasswordViewController {
    
    private func bindingViewModel() {
        
        // Output
        
        viewModel.output.isForgotButtonEnabled
            .drive(onNext: { [weak self] isEnabled in
                guard let self else { return }
                self.resetButton.isEnabled = isEnabled
            })
            .disposed(by: disposeBag)
        
        viewModel.output.description
            .drive(onNext: { [weak self] description in
                guard let self else { return }
                self.descriptionLabel.text = description
            })
            .disposed(by: disposeBag)
        
        viewModel.output.showErrorMessage
            .do(onNext: { [weak self] _  in
                guard let self else { return }
                self.resetButton.hideLoading()
            })
            .drive(onNext: { [weak self] message in
                guard let self else { return }
                self.router.showErrorMessage(message)
            })
            .disposed(by: disposeBag)
        
        viewModel.output.showCheckEmailMessage
            .do(onNext: { [weak self] _  in
                guard let self else { return }
                self.resetButton.hideLoading()
            })
            .drive(onNext: { [weak self] message in
                guard let self else { return }
                self.router.showConfirmForgotPasswordMessage(message)
            })
            .disposed(by: disposeBag)
        
        // Input
        
        resetButton.rx.tap
            .do(onNext: { [weak self] _  in
                guard let self else { return }
                self.resetButton.showLoading()
            })
            .subscribe(onNext: { [weak self] in
                guard let self else { return }
                self.viewModel.input.forgotPasswordTrigger.accept(())
            })
            .disposed(by: disposeBag)
        
        emailTextField.rx.text.orEmpty
            .subscribe(onNext: { [weak self] email in
                guard let self else { return }
                self.viewModel.input.email.accept(email)
            })
            .disposed(by: disposeBag)
    }
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension ForgotPasswordViewController {
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShown(_:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillBeHidden(_:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    @objc func keyboardWillBeHidden(_ notification: NSNotification) {
        activateFormStackViewCenterYConstraint()
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillShown(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              let keyboardRect = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
        else { return }
        let bottomSpacing = keyboardRect.height + .spacing32
        activateFormStackViewBottomConstraint()
        formStackViewBottomConstraint.constant = -bottomSpacing
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
}

extension ForgotPasswordViewController {
    
    private func setupViews() {
        setupConstraints()
    }
    
    private func setupConstraints() {
        var constraints = [NSLayoutConstraint]()
        
        constraints += constraintsBackgroundImageView()
        constraints += constraintsGradientBackgroundView()
        constraints += constraintsBlurView()
        constraints += constraintsFormStackView()
        constraints += constraintLogoImageContainerView()
        constraints += constraintsHeaderContainerView()
        constraints += constraintsLogoImageView()
        
        NSLayoutConstraint.activate(constraints)
    }
    
    private func constraintsBackgroundImageView() -> [NSLayoutConstraint] {
        view.addSubview(backgroundImageView)
        return backgroundImageView.edgesAnchor.constraint(equalTo: view.edgesAnchor)
    }
    
    private func constraintsGradientBackgroundView() -> [NSLayoutConstraint] {
        view.addSubview(gradientBackgroundView)
        return gradientBackgroundView.edgesAnchor.constraint(equalTo: view.edgesAnchor)
    }
    
    private func constraintsBlurView() -> [NSLayoutConstraint] {
        view.addSubview(blurView)
        return blurView.edgesAnchor.constraint(equalTo: view.edgesAnchor)
    }
    
    private func constraintLogoImageContainerView() -> [NSLayoutConstraint] {
        view.addSubview(logoImageContainerView)
        return [
            logoImageContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: .spacing8),
            logoImageContainerView.bottomAnchor.constraint(equalTo: formStackView.topAnchor, constant: -.spacing8),
            logoImageContainerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .spacing20),
            logoImageContainerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.spacing20)
        ]
    }
    
    private func constraintsHeaderContainerView() -> [NSLayoutConstraint] {
        logoImageContainerView.addSubview(headerContainerView)
        headerContainerView.addArrangedSubview(logoImageView)
        headerContainerView.addArrangedSubview(descriptionLabel)
        return [
            headerContainerView.centerYAnchor.constraint(equalTo: logoImageContainerView.centerYAnchor),
            headerContainerView.leadingAnchor.constraint(equalTo: logoImageContainerView.leadingAnchor),
            headerContainerView.trailingAnchor.constraint(equalTo: logoImageContainerView.trailingAnchor)
        ]
    }
    
    private func constraintsLogoImageView() -> [NSLayoutConstraint] {
        return [
            logoImageView.heightAnchor.constraint(equalToConstant: 40)
        ]
    }
    
    private func constraintsFormStackView() -> [NSLayoutConstraint] {
        view.addSubview(formStackView)
        formStackView.addArrangedSubview(emailTextField)
        formStackView.addArrangedSubview(resetButton)
        
        return [
            formStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .spacing24),
            formStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.spacing24),
            formStackViewCenterYConstraint,
            formStackViewBottomConstraint
        ]
    }
    
    private func activateFormStackViewCenterYConstraint() {
        formStackViewCenterYConstraint.priority = .required - 1
        formStackViewBottomConstraint.priority = .defaultLow + 1
    }
    
    private func activateFormStackViewBottomConstraint() {
        formStackViewBottomConstraint.priority = .required - 1
        formStackViewCenterYConstraint.priority = .defaultLow + 1
    }

    private func makeFormStackViewCenterYConstraint() -> NSLayoutConstraint {
        let constraint = formStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        constraint.priority = .required - 1
        return constraint
    }
    
    private func makeFormStackViewBottomConstraint() -> NSLayoutConstraint {
        let constraint = formStackView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        constraint.priority = .defaultLow + 1
        return constraint
    }
    
    private func makeBackgroundImageView() -> UIImageView {
        let imageView = UIImageView(image: Asset.Bg.bgSplashLogin.image)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
    
    private func makeGradientBackgroundView() -> DSGradientView {
        let gradientView = DSGradientView()
        gradientView.translatesAutoresizingMaskIntoConstraints = false
        return gradientView
    }
    
    private func makeBlurView() -> UIVisualEffectView {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        return blurView
    }
    
    private func makeLogoImageContainerView() -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    private func makeHeaderContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fill
        view.alignment = .fill
        view.translatesAutoresizingMaskIntoConstraints = false
        view.spacing = .spacing24
        return view
    }
    
    
    private func makeLogoImageView() -> UIImageView {
        let imageView = UIImageView(image: Asset.Image.imLogo.image)
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
    
    private func makeDescriptionLabel() -> UILabel {
        let label = UILabel()
        label.font = .paragrapRegular
        label.textColor = .white100.withAlphaComponent(0.7)
        label.textAlignment = .center
        label.numberOfLines = .zero
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
    
    private func makeFormStackView() -> UIStackView {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = .spacing20
        view.distribution = .fill
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    private func makeEmailTextField() -> DSTextField {
        let textField = DSTextField(placeholder: L10n.App.ForgotPassword.Fields.email)
        textField.autocapitalizationType = .none
        textField.delegate = self
        textField.accessibilityIdentifier = AccessibilityIdentifier.emailTextField
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }
    private func makeResetButton() -> DSButton {
        let button = DSButton()
        button.setTitle(L10n.App.ForgotPassword.Button.reset, for: .normal)
        button.accessibilityIdentifier = AccessibilityIdentifier.resetButton
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
}

extension ForgotPasswordViewController {
    
    enum AccessibilityIdentifier {
        
        static let emailTextField = "forgot_password_email_textfield"
        static let resetButton = "forgot_password_reset_button"
    }
}
