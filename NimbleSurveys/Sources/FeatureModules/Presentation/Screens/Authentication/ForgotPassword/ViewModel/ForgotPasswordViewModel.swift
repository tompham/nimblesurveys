//
//  ForgotPasswordViewModel.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 09/01/2024.
//

import Foundation
import RxSwift
import RxCocoa
import RxRelay

final class ForgotPasswordViewModel: ForgotPasswordViewModelInputProtocol {
    
    let forgotPasswordTrigger = PublishRelay<Void>()
    let email = BehaviorRelay<String>(value: .empty)
    let showErrorMessageRelay = PublishRelay<String>()
    let showCheckEmailMessageRelay = PublishRelay<String>()

    private let authenticationRepository: AuthenticationRepositoryProtocol
    private let dataValidator: DataValidatorProtocol
    private let disposeBag = DisposeBag()
    
    init(
        authenticationRepository: AuthenticationRepositoryProtocol,
        dataValidator: DataValidatorProtocol
    ) {
        self.authenticationRepository = authenticationRepository
        self.dataValidator = dataValidator
        
        bindingInputs()
    }
}

extension ForgotPasswordViewModel {
    
    private func bindingInputs() {
        forgotPasswordTrigger
            .withLatestFrom(email)
            .subscribe(onNext: { [weak self] email in
                guard let self, self.validateEmail(email) else { return }
                self.performForgotPassword(for: email)
            })
            .disposed(by: disposeBag)
    }
    
    private func validateEmail(_ email: String) -> Bool {
        do {
            return try dataValidator.validateEmail(email)
        } catch {
            if let validationError = error as? EmailValidationError {
                showErrorMessageRelay.accept(validationError.errorMessage)
            }
            return false
        }
    }
    
    private func performForgotPassword(for email: String) {
        authenticationRepository.forgotPassword(for: email)
            .subscribe(
                onNext: { [weak self] meta in
                    guard let self = self else {  return }
                    self.showCheckEmailMessageRelay.accept(meta.message.orEmpty)
                },
                onError: { [weak self] error in
                    guard let self else { return }
                    return self.handleError(error)
                }
            )
            .disposed(by: disposeBag)
    }
    
    private func handleError(_ error: Error) {
        guard let appError = error as? AppError else { return }
        showErrorMessageRelay.accept(appError.message)
    }
}

extension ForgotPasswordViewModel: ForgotPasswordViewModelOutputProtocol {
    
    var isForgotButtonEnabled: RxCocoa.Driver<Bool> {
        return email.map { $0.isNotEmpty }
            .asDriver(onErrorJustReturn: false)
    }
    
    var description: Driver<String> {
        return .just(L10n.App.ForgotPassword.description)
    }
    
    var showErrorMessage: Driver<String> {
        return showErrorMessageRelay.asDriver(onErrorDriveWith: .never())
    }
    
    var showCheckEmailMessage: Driver<String> {
        return showCheckEmailMessageRelay
            .asDriver(onErrorDriveWith: .never())
    }
}

extension ForgotPasswordViewModel: ForgotPasswordViewModelProtocol {
    
    var input: ForgotPasswordViewModelInputProtocol {
        return self
    }
    
    var output: ForgotPasswordViewModelOutputProtocol {
        return self
    }
}
