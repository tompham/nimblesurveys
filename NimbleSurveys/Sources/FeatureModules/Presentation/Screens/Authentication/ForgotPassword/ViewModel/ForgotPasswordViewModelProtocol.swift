//
//  ForgotPasswordViewModelProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 09/01/2024.
//

import Foundation
import RxSwift
import RxCocoa

protocol ForgotPasswordViewModelOutputProtocol: AnyObject {
    var isForgotButtonEnabled: Driver<Bool> { get }
    
    var description: Driver<String> { get }
    
    var showErrorMessage: Driver<String> { get }
    
    var showCheckEmailMessage: Driver<String> { get }
}

protocol ForgotPasswordViewModelInputProtocol: AnyObject {
    
    var forgotPasswordTrigger: PublishRelay<Void> { get }
    
    var email: BehaviorRelay<String> { get }
}

protocol ForgotPasswordViewModelProtocol {
    
    var input: ForgotPasswordViewModelInputProtocol { get }
    
    var output: ForgotPasswordViewModelOutputProtocol { get }
}
