//
//  LoginRouter.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 09/01/2024.
//

import UIKit
import SwiftMessages

final class LoginRouter {
    
    let navigator: AppNavigatorProtocol
    
    init(navigator: AppNavigatorProtocol) {
        self.navigator = navigator
    }
}

extension LoginRouter: LoginRouterProtocol {
    
    func showHomePage(from parent: UIViewController) {
        navigator.show(route: .home, from: parent, action: .root)
    }
    
    func showErrorMessage(_ message: String) {
        
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureTheme(.error)
        view.button?.isHidden = true
        view.configureContent(title: .empty, body: message)
        view.configureDropShadow()
        SwiftMessages.show(view: view)
    }
    
    func navigateToForgotPassword(from parent: UIViewController) {
        navigator.show(route: .forgotPassword, from: parent, action: .push)
    }
}
