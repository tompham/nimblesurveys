//
//  LoginRouterProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 09/01/2024.
//

import UIKit

protocol LoginRouterProtocol: AnyObject {
    
    func showHomePage(from parent: UIViewController)
    
    func showErrorMessage(_ message: String)
    
    func navigateToForgotPassword(from parent: UIViewController) 
}
