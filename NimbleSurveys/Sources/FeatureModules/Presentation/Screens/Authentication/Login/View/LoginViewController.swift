//
//  LoginViewController.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import UIKit
import RxSwift
import RxCocoa
import Resolver

class LoginViewController: UIViewController {

    // MARK: - UI
    
    private lazy var backgroundImageView = makeBackgroundImageView()
    private lazy var gradientBackgroundView = makeGradientBackgroundView()
    private lazy var blurView = makeBlurView()
    private lazy var logoImageContainerView = makeLogoImageContainerView()
    private lazy var logoImageView = makeLogoImageView()
    private lazy var formStackView = makeFormStackView()
    private lazy var emailTextField = makeEmailTextField()
    private lazy var passwordTextField = makePasswordTextField()
    private lazy var forgotPasswordButton = makeForgotPasswordButton()
    private lazy var loginButton = makeLoginButton()
    
    // MARK: - Constraints
    
    private lazy var formStackViewCenterYConstraint = makeFormStackViewCenterYConstraint()
    private lazy var formStackViewBottomConstraint = makeFormStackViewBottomConstraint()
    
    private let viewModel: LoginViewModelProtocol
    private let router: LoginRouterProtocol
    
    private let disposeBag = DisposeBag()
    
    // MARK: - Initializers
    
    init(viewModel: LoginViewModelProtocol, router: LoginRouterProtocol) {
        self.viewModel = viewModel
        self.router = router
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - View - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        startMovingInAnimation()
        bindingViewModel()
        registerForKeyboardNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = ""
    }
}

extension LoginViewController {
    
    private func bindingViewModel() {
        
        // Outputs
        
        viewModel.output.isLoginEnabled
            .drive(loginButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        viewModel.output.showHomePage
            .do(onNext: { [weak self] _ in
                guard let self else { return }
                self.loginButton.hideLoading()
            })
            .drive(onNext: { [weak self] result in
                guard let self else { return }
                self.router.showHomePage(from: self)
            })
            .disposed(by: disposeBag)
        
        viewModel.output.showErrorMessage
            .do(onNext: { [weak self] _ in
                guard let self else { return }
                self.loginButton.hideLoading()
            })
            .drive(onNext: { [weak self] message in
                guard let self else { return }
                self.router.showErrorMessage(message)
            })
            .disposed(by: disposeBag)
        
        // Inputs
        emailTextField.rx.text
            .map { $0.orEmpty }
            .subscribe(onNext: { [weak self] email in
                guard let self else { return }
                self.viewModel.input.email.accept(email)
            })
            .disposed(by: disposeBag)
        
        passwordTextField.rx.text
            .map { $0.orEmpty }
            .subscribe(onNext: { [weak self] email in
                guard let self else { return }
                self.viewModel.input.password.accept(email)
            })
            .disposed(by: disposeBag)
        
        loginButton.rx.tap
            .do(onNext: { [weak self] _ in
                guard let self else { return }
                self.loginButton.showLoading()
            })
            .subscribe(onNext: { [weak self] _ in
                guard let self else { return }
                self.viewModel.input.loginTrigger.accept(())
            })
            .disposed(by: disposeBag)
        
        forgotPasswordButton
            .rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let self else { return }
                self.resignTextFields()
                self.router.navigateToForgotPassword(from: self)
            })
            .disposed(by: disposeBag)
    }
 }

// MARK: - Animations

extension LoginViewController {
    
    private func startMovingInAnimation() {
        UIView.animateKeyframes(withDuration: 1.25, delay: 0) {
            self.animateMovingAndScaleLogoImageView()
            self.animateFadeInBlurView()
            self.animateFadeInFormStackView()
        }
    }
    
    private func animateMovingAndScaleLogoImageView() {
        logoImageView.alpha = .zero
        logoImageView.transform = CGAffineTransform(scaleX: 1.25, y: 1.25).translatedBy(x: .zero, y: UIScreen.main.bounds.size.height / 2)
        UIView.addKeyframe(withRelativeStartTime: .zero, relativeDuration: 1) {
            self.logoImageView.alpha = 1
            self.logoImageView.transform = .identity
        }
    }
    
    private func animateFadeInBlurView() {
        blurView.alpha = .zero
        UIView.addKeyframe(withRelativeStartTime: .zero, relativeDuration: 1) {
            self.blurView.alpha = 1
        }
    }
    
    private func animateFadeInFormStackView() {
        formStackView.alpha = .zero
        UIView.addKeyframe(withRelativeStartTime: 0.75, relativeDuration: 0.5) {
            self.formStackView.alpha = 1
        }
    }
}

extension LoginViewController {
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShown(_:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillBeHidden(_:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    @objc func keyboardWillBeHidden(_ notification: NSNotification) {
        activateFormStackViewCenterYConstraint()
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillShown(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo, 
            let keyboardRect = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
        else { return }
        let bottomSpacing = keyboardRect.height + .spacing32
        activateFormStackViewBottomConstraint()
        formStackViewBottomConstraint.constant = -bottomSpacing
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
}

// MARK:  - Privates

extension LoginViewController {
    
    private func setupViews() {
        setupConstraints()
    }
    
    private func setupConstraints() {
        var constraints = [NSLayoutConstraint]()
        
        constraints += constraintsBackgroundImageView()
        constraints += constraintsGradientBackgroundView()
        constraints += constraintsBlurView()
        constraints += constraintsFormStackView()
        constraints += constraintLogoImageContainerView()
        constraints += constraintsLogoImageView()
        
        NSLayoutConstraint.activate(constraints)
    }
    
    private func constraintsBackgroundImageView() -> [NSLayoutConstraint] {
        view.addSubview(backgroundImageView)
        return backgroundImageView.edgesAnchor.constraint(equalTo: view.edgesAnchor)
    }
    
    private func constraintsGradientBackgroundView() -> [NSLayoutConstraint] {
        view.addSubview(gradientBackgroundView)
        return gradientBackgroundView.edgesAnchor.constraint(equalTo: view.edgesAnchor)
    }
    
    private func constraintsBlurView() -> [NSLayoutConstraint] {
        view.addSubview(blurView)
        return blurView.edgesAnchor.constraint(equalTo: view.edgesAnchor)
    }
    
    private func constraintLogoImageContainerView() -> [NSLayoutConstraint] {
        view.addSubview(logoImageContainerView)
        return [
            logoImageContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: .spacing8),
            logoImageContainerView.bottomAnchor.constraint(equalTo: formStackView.topAnchor, constant: -.spacing8),
            logoImageContainerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .spacing8),
            logoImageContainerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.spacing8)
        ]
    }
    
    private func constraintsLogoImageView() -> [NSLayoutConstraint] {
        logoImageContainerView.addSubview(logoImageView)
        return [
            logoImageView.heightAnchor.constraint(equalToConstant: 40),
            logoImageView.centerYAnchor.constraint(equalTo: logoImageContainerView.centerYAnchor),
            logoImageView.centerXAnchor.constraint(equalTo: logoImageContainerView.centerXAnchor),
        ]
    }
    
    private func constraintsFormStackView() -> [NSLayoutConstraint] {
        view.addSubview(formStackView)
        formStackView.addArrangedSubview(emailTextField)
        formStackView.addArrangedSubview(passwordTextField)
        formStackView.addArrangedSubview(loginButton)
        
        return [
            formStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .spacing24),
            formStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.spacing24),
            formStackViewCenterYConstraint,
            formStackViewBottomConstraint
        ]
    }
    
    private func activateFormStackViewCenterYConstraint() {
        formStackViewCenterYConstraint.priority = .required - 1
        formStackViewBottomConstraint.priority = .defaultLow + 1
    }
    
    private func activateFormStackViewBottomConstraint() {
        formStackViewBottomConstraint.priority = .required - 1
        formStackViewCenterYConstraint.priority = .defaultLow + 1
    }
    
    private func resignTextFields() {
        [
            emailTextField,
            passwordTextField
        ].forEach { textField in
            textField.resignFirstResponder()
            textField.endEditing(true)
        }
    }
    
    private func makeFormStackViewCenterYConstraint() -> NSLayoutConstraint {
        let constraint = formStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        constraint.priority = .required - 1
        return constraint
    }
    
    private func makeFormStackViewBottomConstraint() -> NSLayoutConstraint {
        let constraint = formStackView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        constraint.priority = .defaultLow + 1
        return constraint
    }
    
    private func makeBackgroundImageView() -> UIImageView {
        let imageView = UIImageView(image: Asset.Bg.bgSplashLogin.image)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
    
    private func makeGradientBackgroundView() -> DSGradientView {
        let gradientView = DSGradientView()
        gradientView.translatesAutoresizingMaskIntoConstraints = false
        return gradientView
    }
    
    private func makeBlurView() -> UIVisualEffectView {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        return blurView
    }
    
    private func makeLogoImageContainerView() -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    private func makeLogoImageView() -> UIImageView {
        let imageView = UIImageView(image: Asset.Image.imLogo.image)
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
    
    private func makeFormStackView() -> UIStackView {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = .spacing20
        view.distribution = .fill
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    private func makeEmailTextField() -> DSTextField {
        let textField = DSTextField(placeholder: L10n.App.Login.Fields.Email.placeholder)
        textField.autocapitalizationType = .none
        textField.accessibilityIdentifier = AccessibilityIdentifier.emailTextField
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }
    
    private func makePasswordTextField() -> DSTextField {
        let textField = DSTextField(placeholder: L10n.App.Login.Fields.Password.placeholder)
        textField.isSecureTextEntry = true
        textField.rightView = forgotPasswordButton
        textField.rightViewMode = .always
        textField.accessibilityIdentifier = AccessibilityIdentifier.passwordTextField
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }
    
    private func makeForgotPasswordButton() -> UIButton {
        let button = UIButton()
        button.titleLabel?.font = .small
        button.setTitleColor(.white.withAlphaComponent(0.5), for: .normal)
        button.setTitle(L10n.App.Login.Button.forgotPassword, for: .normal)
        button.contentEdgeInsets = UIEdgeInsets(top: .zero, left: .spacing12, bottom: .zero, right: .spacing12)
        button.accessibilityIdentifier = AccessibilityIdentifier.forgotPasswordButton
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
    
    private func makeLoginButton() -> DSButton {
        let button = DSButton()
        button.setTitle(L10n.App.Common.Button.login, for: .normal)
        button.accessibilityIdentifier = AccessibilityIdentifier.loginButton
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
}

extension LoginViewController {
    
    enum AccessibilityIdentifier {
        
        static let emailTextField = "login_email_textfield"
        static let passwordTextField = "login_password_textfield"
        static let loginButton = "login_login_button"
        static let forgotPasswordButton = "login_forgot_password_button"
    }
}
