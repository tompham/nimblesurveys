//
//  LoginViewModel.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import RxSwift
import RxCocoa

final class LoginViewModel: LoginViewModeInputProtocol {
    
    // MARK: - Inputs
    
    let email = BehaviorRelay<String>(value: .empty)
    let password = BehaviorRelay<String>(value: .empty)
    let loginTrigger = PublishRelay<Void>()
    
    private let showHomePageRelay = PublishRelay<Void>()
    private let showErrorMessageRelay = PublishRelay<String>()
    
    // MARK: - Dependencies
    
    private let repository: AuthenticationRepositoryProtocol
    private let dataValidator: DataValidatorProtocol
    private let disposeBag = DisposeBag()
    
    init(
        repository: AuthenticationRepositoryProtocol,
        dataValidator: DataValidatorProtocol
    ) {
        self.repository = repository
        self.dataValidator = dataValidator
        
        bindingInputs()
    }
}

extension LoginViewModel {
    
    private func bindingInputs() {
        loginTrigger
            .withLatestFrom(Observable.combineLatest(email, password))
            .subscribe(onNext: { [weak self] email, password in
                guard let self, self.validateLoginData(email: email, password: password)
                else { return }
                self.performAuthenticate(email: email, password: password)
            })
            .disposed(by: disposeBag)
    }
    
    private func performAuthenticate(email: String, password: String) {
        repository.authenticate(email: email, password: password)
            .subscribe(
                onNext: { [weak self] _ in
                    guard let self else { return }
                    self.showHomePageRelay.accept(())
                },
                onError: { [weak self] error in
                    guard let self = self else { return }
                    self.handleLoginError(error)
                }
            )
            .disposed(by: disposeBag)
    }
    
    private func handleLoginError(_ error: Error) {
        guard let appError = error as? AppError else {
            return
        }
        
        showErrorMessageRelay.accept(appError.message)
    }
    
    private func validateLoginData(email: String, password: String) -> Bool {
        do {
            return try dataValidator.validateEmail(email)
        } catch {
            if let validationError = error as? EmailValidationError {
                showErrorMessageRelay.accept(validationError.errorMessage)
            }
            return false
        }
    }
}

extension LoginViewModel: LoginViewModelOutputProtocol {
    
    var isLoginEnabled: RxCocoa.Driver<Bool> {
        return Observable.combineLatest(email, password)
            .map { email, password -> Bool in
                return email.isNotEmpty && password.isNotEmpty
            }
            .asDriver(onErrorDriveWith: .never())
    }
    
    var showHomePage: Driver<Void> {
        return showHomePageRelay
            .asDriver(onErrorDriveWith: .never())
    }
    
    var showErrorMessage: Driver<String> {
        return showErrorMessageRelay
            .asDriver(onErrorDriveWith: .never())
    }
}

extension LoginViewModel: LoginViewModelProtocol {
    
    var input: LoginViewModeInputProtocol {
        return self
    }
    
    var output: LoginViewModelOutputProtocol {
        return self
    }
}
