//
//  LoginViewModelProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import RxSwift
import RxCocoa

protocol LoginViewModelOutputProtocol: AnyObject {

    var isLoginEnabled: Driver<Bool> { get }
    
    var showHomePage: Driver<Void> { get }
    
    var showErrorMessage: Driver<String> { get }
}

protocol LoginViewModeInputProtocol: AnyObject {
    
    var email: BehaviorRelay<String> { get }
    var password: BehaviorRelay<String> { get }
    var loginTrigger: PublishRelay<Void> { get }
}

protocol LoginViewModelProtocol {
    
    var input: LoginViewModeInputProtocol { get }
    
    var output: LoginViewModelOutputProtocol { get }
}
