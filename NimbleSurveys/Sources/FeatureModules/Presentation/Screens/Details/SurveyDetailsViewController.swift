//
//  SurveyDetailsViewController.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 08/01/2024.
//

import UIKit
import SDWebImage
import RxCocoa
import RxSwift

final class SurveyDetailsViewController: UIViewController {
    
    // MARK: - UI
    
    private lazy var backgroundImageView = makeBackgroundImageView()
    private lazy var gradientBackgroundView = makeGradientBackgroundView()
    private lazy var infoContainerView = makeInfoContainerView()
    private lazy var titleLabel = makeTitleLabel()
    private lazy var descriptionLabel = makeDescriptionLabel()
    private lazy var startButton = makeStartButton()
    
    private let viewModel: SurveyDetailsViewModelProtocol
    private let disposeBag = DisposeBag()

    init(viewModel: SurveyDetailsViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        bindingViewModel()
    }
}

extension SurveyDetailsViewController {
    
    private func bindingViewModel() {
        viewModel.output.title
            .drive(titleLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.output.description
            .drive(descriptionLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.output.backgroundImageUrl
            .drive(
                onNext: { [weak self] imageUrl in
                    guard let self else { return }
                    self.backgroundImageView.sd_setImage(with: URL(string: imageUrl))
                }
            )
            .disposed(by: disposeBag)
    }
}

extension SurveyDetailsViewController {
    
    private func setupViews() {
        view.backgroundColor = .white
        setupConstraints()
    }
    
    private func setupConstraints() {
        var constraints = [NSLayoutConstraint]()
        
        constraints += constraintsBackgroundImageView()
        constraints += constraintsGradientBackgroundView()
        constraints += constraintsInfoContainerView()
        constraints += constraintsStartButton()
        
        NSLayoutConstraint.activate(constraints)
    }
    
    private func constraintsBackgroundImageView() -> [NSLayoutConstraint] {
        view.addSubview(backgroundImageView)
        return backgroundImageView.edgesAnchor.constraint(equalTo: view.edgesAnchor)
    }
    
    private func constraintsGradientBackgroundView() -> [NSLayoutConstraint] {
        view.addSubview(gradientBackgroundView)
        return gradientBackgroundView.edgesAnchor.constraint(equalTo: view.edgesAnchor)
    }
    
    private func constraintsInfoContainerView() -> [NSLayoutConstraint] {
        view.addSubview(infoContainerView)
        infoContainerView.addArrangedSubview(titleLabel)
        infoContainerView.addArrangedSubview(descriptionLabel)
        
        return [
            infoContainerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: .spacing20),
            infoContainerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: .spacing20),
            infoContainerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -.spacing20),
        ]
    }
    
    private func constraintsStartButton() -> [NSLayoutConstraint] {
        view.addSubview(startButton)
        return [
            startButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.spacing20),
            startButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -.spacing20)
        ]
    }
    
    private func makeBackgroundImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.sd_imageTransition = .fade
        imageView.sd_imageIndicator = SDWebImageActivityIndicator()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
    
    private func makeGradientBackgroundView() -> DSGradientView {
        let gradientView = DSGradientView(colors: [
            .black.withAlphaComponent(0),
            .black.withAlphaComponent(0.75)
        ])
        gradientView.translatesAutoresizingMaskIntoConstraints = false
        return gradientView
    }
    
    private func makeInfoContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .equalCentering
        view.alignment = .fill
        view.spacing = .spacing16
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    
    private func makeTitleLabel() -> UILabel {
        let label = UILabel()
        label.textAlignment = .left
        label.font = .display2
        label.numberOfLines = .zero
        label.textColor = .white100
        label.accessibilityIdentifier = AccessibilityIdentifier.titleLabel
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
    
    private func makeDescriptionLabel() -> UILabel {
        let label = UILabel()
        label.textAlignment = .left
        label.font = .paragrapRegular
        label.numberOfLines = .zero
        label.textColor = .white100.withAlphaComponent(0.7)
        label.accessibilityIdentifier = AccessibilityIdentifier.descLabel
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
    
    private func makeStartButton() -> DSButton {
        let button = DSButton()
        button.setTitle(L10n.App.Details.startSurveys, for: .normal)
        button.contentEdgeInsets = .init(top: .zero, left: .spacing16, bottom: .zero, right: .spacing16)
        button.accessibilityIdentifier = AccessibilityIdentifier.startButton
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
}

extension SurveyDetailsViewController {
    
    enum AccessibilityIdentifier {
        
        static let titleLabel = "details_title_label"
        static let descLabel = "details_description_label"
        static let startButton = "details_start_button"
    }
}
