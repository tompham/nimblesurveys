//
//  SurveyDetailsViewModel.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 10/01/2024.
//

import Foundation
import RxSwift
import RxCocoa

final class SurveyDetailsViewModel {
    
    // MARK: - Properties
    
    let entity: SurveyEntity
    
    // MARK: - Initializers
    
    init(entity: SurveyEntity) {
        self.entity = entity
    }
}

// MARK: - SurveyDetailsViewModelInputProtocol

extension SurveyDetailsViewModel: SurveyDetailsViewModelInputProtocol {}

// MARK: - SurveyDetailsViewModelOutputProtocol

extension SurveyDetailsViewModel: SurveyDetailsViewModelOutputProtocol {
    var title: Driver<String> {
        return .just((entity.title).orEmpty)
    }
    
    var description: Driver<String> {
        return .just((entity.description).orEmpty)
    }
    
    var backgroundImageUrl: Driver<String> {
        return .just((entity.coverImageUrl).orEmpty)
    }
}

// MARK: - SurveyDetailsViewModelProtocol

extension SurveyDetailsViewModel: SurveyDetailsViewModelProtocol {
    
    var input: SurveyDetailsViewModelInputProtocol {
        return self
    }
    
    var output: SurveyDetailsViewModelOutputProtocol {
        return self
    }
}
