//
//  SurveyDetailsViewModelProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 10/01/2024.
//

import Foundation
import RxSwift
import RxCocoa

protocol SurveyDetailsViewModelInputProtocol: AnyObject {}

protocol SurveyDetailsViewModelOutputProtocol: AnyObject {
    
    var title: Driver<String> { get }
    
    var description: Driver<String> { get }
    
    var backgroundImageUrl: Driver<String> { get }
}

protocol SurveyDetailsViewModelProtocol {
    
    var input: SurveyDetailsViewModelInputProtocol { get }
    
    var output: SurveyDetailsViewModelOutputProtocol { get }
}
