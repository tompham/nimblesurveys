//
//  HomeRouter.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 08/01/2024.
//

import UIKit

final class HomeRouter {

    private let errorHandler: ErrorHandlerProtocol
    private let navigator: AppNavigatorProtocol
    
    init(errorHandler: ErrorHandlerProtocol, navigator: AppNavigatorProtocol) {
        self.errorHandler = errorHandler
        self.navigator = navigator
    }
}

extension HomeRouter: HomeRouterProtocol {
    
    func handleError(_ error: Error, on controller: UIViewController) {
        errorHandler.handleError(error: error, on: controller)
    }
    
    func navigateToDetails(surveyEntity: SurveyEntity, from parent: UIViewController) {
        navigator.show(route: .details(entity: surveyEntity), from: parent, action: .push)
    }
    
    func showLogin(from parent: UIViewController) {
        navigator.show(route: .login, from: parent, action: .root)
    }
}
