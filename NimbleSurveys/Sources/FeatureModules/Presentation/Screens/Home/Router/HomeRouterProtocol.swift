//
//  HomeRouterProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 08/01/2024.
//

import UIKit

protocol HomeRouterProtocol: AnyObject {
    
    func handleError(_ error: Error, on controller: UIViewController)
    
    func navigateToDetails(surveyEntity: SurveyEntity, from parent: UIViewController)
    
    func showLogin(from parent: UIViewController)
}
