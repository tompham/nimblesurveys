//
//  HomeDescriptionView.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import UIKit
import RxCocoa
import RxSwift

final class HomeDescriptionView: UIView {
    
    private lazy var contentContainerView = makeContentContainerView()
    private lazy var infoContainerView = makeInfoContainerView()
    private lazy var titleLabel = makeTitleLabel()
    private lazy var descriptionLabel = makeDescriptionLabel()
    private lazy var continueButton = makeContinueButton()
    
    var onContinue: VoidCallBack?
    private let disposeBag = DisposeBag()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(title: String, description: String) {
        fade(label: titleLabel, toText: title)
        fade(label: descriptionLabel, toText: description)
    }
    
    private func fade(label: UILabel, toText: String) {
        UIView.transition(
            with: label,
            duration: 0.75,
            options: .transitionCrossDissolve,
            animations: {
                label.text = toText
            },
            completion: nil
        )
    }
}

extension HomeDescriptionView {
    
    private func setupViews() {
        setupConstraints()
        
        continueButton
            .rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.onContinue?()
            })
            .disposed(by: disposeBag)
    }
    
    private func setupConstraints() {
        var constraints = [NSLayoutConstraint]()
        
        constraints += constraintsContentContainerView()
        constraints += constraintsContinueButton()
        
        NSLayoutConstraint.activate(constraints)
    }
    
    private func constraintsContentContainerView() -> [NSLayoutConstraint] {
        addSubview(contentContainerView)
        
        
        contentContainerView.addArrangedSubview(titleLabel)
        contentContainerView.addArrangedSubview(infoContainerView)
        
        infoContainerView.addArrangedSubview(descriptionLabel)
        infoContainerView.addArrangedSubview(continueButton)
        
        return contentContainerView.edgesAnchor.constraint(equalTo: edgesAnchor)
    }
    
    private func constraintsContinueButton() -> [NSLayoutConstraint] {
        return [
            continueButton.widthAnchor.constraint(equalTo: continueButton.heightAnchor),
            continueButton.heightAnchor.constraint(equalToConstant: 56)
        ]
    }
    
    private func makeContentContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .fill
        view.spacing = .spacing16
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    private func makeInfoContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .horizontal
        view.distribution = .equalCentering
        view.alignment = .fill
        view.spacing = .spacing20
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    
    private func makeTitleLabel() -> UILabel {
        let label = UILabel()
        label.textAlignment = .left
        label.font = .display2
        label.numberOfLines = .zero
        label.textColor = .white100
        label.accessibilityIdentifier = AccessibilityIdentifier.titleLabel
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
    
    private func makeDescriptionLabel() -> UILabel {
        let label = UILabel()
        label.textAlignment = .left
        label.font = .paragrapRegular
        label.numberOfLines = 2
        label.textColor = .white100.withAlphaComponent(0.7)
        label.accessibilityIdentifier = AccessibilityIdentifier.descLabel
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
    
    private func makeContinueButton() -> DSButton {
        let button = DSButton()
        button.setImage(Asset.Icon.icArrowRight.image, for: .normal)
        button.layer.cornerRadius = 28
        button.contentEdgeInsets = .init(top: .spacing12, left: .spacing12, bottom: .spacing12, right: .spacing12)
        button.accessibilityIdentifier = AccessibilityIdentifier.continueButton
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
}

extension HomeDescriptionView {
    
    enum AccessibilityIdentifier {
        
        static let titleLabel = "home_desc_title_label"
        static let descLabel = "home_desc_description_label"
        static let continueButton = "home_desc_continue_button"
    }
}
