//
//  HomeHeaderView.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import UIKit
import SDWebImage

final class HomeHeaderView: UIView {
    
    private lazy var containerView = makeContainerView()
    private lazy var subTitleLabel = makeSubTitleLabel()
    private lazy var titleInfoContainerView = makeTitleInfoContainerView()
    private lazy var titleLabel = makeTitleLabel()
    private lazy var avatarImageView = makeAvatarImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setSubTitle(_ title: String) {
        subTitleLabel.text = title
    }
    
    func setTitle(_ title: String) {
        titleLabel.text = title
    }
    
    func setProfileAvatar(_ imageUrl: String) {
        avatarImageView.sd_setImage(with: URL(string: imageUrl))
    }
}

extension HomeHeaderView {
    
    private func setupViews() {
        setupConstraints()
    }
    
    private func setupConstraints() {
        var constraints = [NSLayoutConstraint]()
        
        constraints += constraintsContainerView()
        constraints += constraintsAvatarImageView()
        
        NSLayoutConstraint.activate(constraints)
    }
    
    private func constraintsContainerView() -> [NSLayoutConstraint] {
        addSubview(containerView)
        
        containerView.addArrangedSubview(subTitleLabel)
        containerView.addArrangedSubview(titleInfoContainerView)
        
        titleInfoContainerView.addArrangedSubview(titleLabel)
        titleInfoContainerView.addArrangedSubview(avatarImageView)
        
        return containerView.edgesAnchor.constraint(equalTo: edgesAnchor)
    }
    
    private func constraintsAvatarImageView() -> [NSLayoutConstraint] {
        return [
            avatarImageView.widthAnchor.constraint(equalTo: avatarImageView.heightAnchor),
            avatarImageView.heightAnchor.constraint(equalToConstant: 36)
        ]
    }
    
    private func makeContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fill
        view.spacing = .spacing4
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    private func makeSubTitleLabel() -> UILabel {
        let label = UILabel()
        label.numberOfLines = 1
        label.textAlignment = .left
        label.font = .xSmall
        label.skeletonCornerRadius = 16
        label.textColor = .white100
        return label
    }
    
    private func makeTitleInfoContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .horizontal
        view.distribution = .equalCentering
        view.alignment = .fill
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    private func makeTitleLabel() -> UILabel {
        let label = UILabel()
        label.numberOfLines = 1
        label.textAlignment = .left
        label.font = .largeTitle
        label.textColor = .white100
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
    
    private func makeAvatarImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.sd_imageTransition = .fade
        imageView.layer.cornerRadius = 18
        imageView.backgroundColor = .gray
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
}

extension Date {
    
    func formattedDateTime(format: String = "EEEE, MMM dd") -> String {
        let formatter = DateFormatter()
        formatter.locale = .current
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
