//
//  HomeLoadingView.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import UIKit
import SkeletonView

final class HomeLoadingView: UIView {
    
    private lazy var subTitlePlaceholder = makePlaceholderView()
    private lazy var titlePlaceholder = makePlaceholderView()
    private lazy var avatarPlaceHolder = makePlaceholderView()
    private lazy var bottomContainerView = makeBottomContainerView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var skeletonViews: [UIView] {
        return [
            subTitlePlaceholder,
            titlePlaceholder,
            avatarPlaceHolder
        ] + bottomContainerView.arrangedSubviews
    }
}

extension HomeLoadingView {
    
    private func setupViews() {
        backgroundColor = .black
        setupConstraints()
        skeletonViews.forEach {
            $0.showAnimatedGradientSkeleton(
                usingGradient: SkeletonGradient(
                    baseColor: UIColor(hex: "3F3F41"),
                    secondaryColor: UIColor(hex: "29292A")
                ),
                animation: nil,
                transition: .crossDissolve(0.25)
            )
        }
    }
    
    private func setupConstraints() {
        var constraints = [NSLayoutConstraint]()
        
        constraints += constraintsSubTitlePlaceholder()
        constraints += constraintsTitlePlaceholder()
        constraints += constraintsAvatarPlaceholder()
        constraints += constraintsBottomContainer()
        
        NSLayoutConstraint.activate(constraints)
    }
    
    private func constraintsSubTitlePlaceholder() -> [NSLayoutConstraint] {
        addSubview(subTitlePlaceholder)
        return [
            subTitlePlaceholder.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            subTitlePlaceholder.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .spacing20),
            subTitlePlaceholder.heightAnchor.constraint(equalToConstant: 24),
            subTitlePlaceholder.widthAnchor.constraint(equalToConstant: 128)
        ]
    }
    
    private func constraintsTitlePlaceholder() -> [NSLayoutConstraint] {
        addSubview(titlePlaceholder)
        return [
            titlePlaceholder.topAnchor.constraint(equalTo: subTitlePlaceholder.bottomAnchor, constant: .spacing12),
            titlePlaceholder.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .spacing20),
            titlePlaceholder.heightAnchor.constraint(equalToConstant: 24),
            titlePlaceholder.widthAnchor.constraint(equalToConstant: 100)
        ]
    }
    
    private func constraintsAvatarPlaceholder() -> [NSLayoutConstraint] {
        avatarPlaceHolder.layer.cornerRadius = 18
        addSubview(avatarPlaceHolder)
        return [
            avatarPlaceHolder.widthAnchor.constraint(equalToConstant: 36),
            avatarPlaceHolder.heightAnchor.constraint(equalToConstant: 36),
            avatarPlaceHolder.centerYAnchor.constraint(equalTo: titlePlaceholder.centerYAnchor),
            avatarPlaceHolder.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.spacing20)
        ]
    }

    private func constraintsBottomContainer() -> [NSLayoutConstraint] {
        addSubview(bottomContainerView)
        
        let pageControlPlaceholder = makePlaceholderView()
        bottomContainerView.addArrangedSubview(pageControlPlaceholder)
        
        let titleFirstLinePlaceHolder = makePlaceholderView()
        bottomContainerView.addArrangedSubview(titleFirstLinePlaceHolder)
        
        let titleSecondLinePlaceHolder = makePlaceholderView()
        bottomContainerView.addArrangedSubview(titleSecondLinePlaceHolder)
        
        let descFirstLinePlaceHolder = makePlaceholderView()
        bottomContainerView.addArrangedSubview(descFirstLinePlaceHolder)
        
        let descSecondLinePlaceHolder = makePlaceholderView()
        bottomContainerView.addArrangedSubview(descSecondLinePlaceHolder)
        
        return [
            bottomContainerView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor, constant: -.spacing12),
            bottomContainerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .spacing20),
            bottomContainerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.spacing20),
            pageControlPlaceholder.heightAnchor.constraint(equalToConstant: 24),
            pageControlPlaceholder.widthAnchor.constraint(equalToConstant: 56),
            titleFirstLinePlaceHolder.heightAnchor.constraint(equalToConstant: 24),
            titleFirstLinePlaceHolder.widthAnchor.constraint(equalToConstant: 253),
            titleSecondLinePlaceHolder.heightAnchor.constraint(equalToConstant: 24),
            titleSecondLinePlaceHolder.widthAnchor.constraint(equalToConstant: 128),
            descFirstLinePlaceHolder.heightAnchor.constraint(equalToConstant: 24),
            descFirstLinePlaceHolder.widthAnchor.constraint(equalToConstant: 300),
            descSecondLinePlaceHolder.heightAnchor.constraint(equalToConstant: 24),
            descSecondLinePlaceHolder.widthAnchor.constraint(equalToConstant: 200)
        ]
    }
    
    private func makeBottomContainerView() -> UIStackView {
        let view = UIStackView()
        view.isSkeletonable = true
        view.axis = .vertical
        view.distribution = .fillEqually
        view.alignment = .leading
        view.translatesAutoresizingMaskIntoConstraints = false
        view.spacing = .spacing12
        return view
    }
    
    private func makePlaceholderView() -> UIView {
        let view = UIView()
        view.isSkeletonable = true
        view.backgroundColor = UIColor(hex: "#47474A")
        view.layer.cornerRadius = 12
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
}
