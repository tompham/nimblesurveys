//
//  SurveyCollectionViewCell.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import UIKit
import SDWebImage

class SurveyCollectionViewCell: UICollectionViewCell {
    
    private lazy var backgroundImageView = makeBackgroundImageView()
    private lazy var gradientBackgroundView = makeGradientBackgroundView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureView(viewModel: SurveyViewModel) {
        backgroundImageView.sd_setImage(
            with: URL(string: viewModel.coverImageUrl)
        )
    }
}

extension SurveyCollectionViewCell {
    
    private func setupViews() {
        setupConstraints()
    }
    
    private func setupConstraints() {
        var constraints = [NSLayoutConstraint]()
        
        constraints += constraintsBackgroundImageView()
        constraints += constraintsGradientBackgroundView()
        
        NSLayoutConstraint.activate(constraints)
    }
    
    private func constraintsBackgroundImageView() -> [NSLayoutConstraint] {
        contentView.addSubview(backgroundImageView)
        return backgroundImageView.edgesAnchor.constraint(equalTo: contentView.edgesAnchor)
    }
    
    private func constraintsGradientBackgroundView() -> [NSLayoutConstraint] {
        contentView.addSubview(gradientBackgroundView)
        return gradientBackgroundView.edgesAnchor.constraint(equalTo: contentView.edgesAnchor)
    }
    
    private func makeBackgroundImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.sd_imageTransition = .fade
        imageView.sd_imageIndicator = SDWebImageActivityIndicator()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
    
    private func makeGradientBackgroundView() -> DSGradientView {
        let gradientView = DSGradientView(colors: [
            .black.withAlphaComponent(0),
            .black.withAlphaComponent(0.75)
        ])
        gradientView.translatesAutoresizingMaskIntoConstraints = false
        return gradientView
    }
}
