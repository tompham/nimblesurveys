//
//  SurveyViewModel.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import Foundation

final class SurveyViewModel {
    
    let entity: SurveyEntity
    
    init(entity: SurveyEntity) {
        self.entity = entity
    }
    
    var coverImageUrl: String {
        if let url = entity.coverImageUrl {
            return "\(url)l"
        }
        
        return .empty
    }
    
    var title: String {
        return (entity.title).orEmpty
    }
    
    var description: String {
        return (entity.description).orEmpty
    }
}
