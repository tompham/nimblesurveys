//
//  HomeViewController.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import UIKit
import RxSwift
import RxCocoa

class HomeViewController: UIViewController {
    
    private lazy var collectionView = makeCollectionView()
    private lazy var headerView = makeHeaderView()
    private lazy var pageControl = makePageControl()
    private lazy var descriptionView = makeDescriptionView()
    
    private let viewModel: HomeViewModelProtocol
    private let router: HomeRouterProtocol
    private var dataSource: [SurveyViewModel] = []
    private let disposeBag = DisposeBag()
    
    private let currentPageIndexRelay = BehaviorRelay<Int>(value: .zero)
    private let focusingSurveyRelay = PublishRelay<SurveyViewModel>()
    
    private lazy var loadingView = makeLoadingView()
    private lazy var errorStateView = makeErrorStateView()
    private lazy var statePresenter = makeStatePresenter()
    
    init(
        viewModel: HomeViewModelProtocol,
        router: HomeRouterProtocol
    ) {
        self.viewModel = viewModel
        self.router = router
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        defer {
            viewModel.input.viewDidLoadTrigger.accept(())
            statePresenter.startLoading(animated: false, completionHandler: nil)
        }
        super.viewDidLoad()
        setupViews()
        bindingViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = .empty
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
}

// MARK: - Privates

extension HomeViewController {
    
    private func bindingViewModel() {
        
        // Output
        
        viewModel.output.headerTitle
            .drive(onNext: { [weak self] title in
                guard let self else { return }
                self.headerView.setTitle(title)
            })
            .disposed(by: disposeBag)
        
        viewModel.output.headerSubTitle
            .drive(onNext: { [weak self] subTitle in
                guard let self else { return }
                self.headerView.setSubTitle(subTitle)
            })
            .disposed(by: disposeBag)
        
        viewModel.output.profileImageUrl
            .drive(onNext: { [weak self] imageUrl in
                guard let self else { return }
                self.headerView.setProfileAvatar(imageUrl)
            })
            .disposed(by: disposeBag)
        
        viewModel.output.surveys
            .drive(onNext: { [weak self] surveys in
                guard let self else { return }
                self.dataSource = surveys
                self.collectionView.reloadData()
            })
            .disposed(by: disposeBag)
        
        viewModel.output.surveys
            .map { $0.isNotEmpty }
            .drive(onNext: { [weak self] hasContent in
                guard let self = self else { return }
                self.statePresenter.endLoading(
                    hasError: false,
                    hasContent: hasContent,
                    completionHandler: nil
                )
            })
            .disposed(by: disposeBag)
        
        viewModel.output.totalPage
            .drive(pageControl.rx.numberOfPages)
            .disposed(by: disposeBag)
        
        viewModel.output.showErrorState
            .drive(onNext: { [weak self] error in
                guard let self else { return }
                self.statePresenter.endLoading(
                    hasError: true,
                    hasContent: false,
                    completionHandler: nil
                )
            })
            .disposed(by: disposeBag)
        
        viewModel.output.logoutUser
            .drive(onNext: { [weak self] _ in
                guard let self else { return }
                self.router.showLogin(from: self)
            })
            .disposed(by: disposeBag)
        
        currentPageIndexRelay
            .distinctUntilChanged()
            .bind(to: pageControl.rx.currentPage)
            .disposed(by: disposeBag)
        
        focusingSurveyRelay
            .asDriver(onErrorDriveWith: .never())
            .drive(onNext: { [weak self] surveyModel in
                guard let self else { return }
                self.descriptionView.update(
                    title: surveyModel.title,
                    description: surveyModel.description
                )
            })
            .disposed(by: disposeBag)
        
        collectionView.rx.loadMoreTrigger()
            .subscribe { [weak self] _ in
                self?.viewModel.input.loadMoreTrigger.accept(())
            }
            .disposed(by: disposeBag)

        Observable.combineLatest(
            currentPageIndexRelay.distinctUntilChanged().asObservable(),
            viewModel.output.surveys.asObservable()
        )
        .asDriver(onErrorDriveWith: .never())
        .drive(
            onNext: { [weak self]currentPage, surveys in
                guard let self, let focusingSurvey = surveys[safe: currentPage] else {
                    return
                }
                let focusingIndexPath = IndexPath(item: currentPage, section: .zero)
                
                self.focusingSurveyRelay.accept(focusingSurvey)
                self.collectionView.scrollToItem(at: focusingIndexPath, at: .right, animated: true)
            }
        )
        .disposed(by: disposeBag)
        
        pageControl.rx.controlEvent(.valueChanged)
            .asDriver()
            .compactMap { [weak self] _ -> Int? in
                guard let currentPage = self?.pageControl.currentPage else {
                    return nil
                }
                return currentPage
            }
            .drive(currentPageIndexRelay)
            .disposed(by: disposeBag)
    }
}

// MARK: - UICollectionViewDataSource

extension HomeViewController: UICollectionViewDataSource {
    func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int
    ) -> Int {
        return dataSource.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Config.cellID, for: indexPath) as? SurveyCollectionViewCell
        else {
            return UICollectionViewCell()
        }
        
        if let model = dataSource[safe: indexPath.item] {
            cell.configureView(viewModel: model)
        }
        
        return cell
    }
}

// MARk: - UICollectionViewDelegate

extension HomeViewController: UICollectionViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let index = Int(round(scrollView.contentOffset.x / scrollView.frame.width))
        currentPageIndexRelay.accept(index)
    }
}

// MARK: - Layouts

extension HomeViewController {
    
    private func setupViews() {
        setupConstraints()
        registerCells()
    }
    
    private func registerCells() {
        collectionView.register(SurveyCollectionViewCell.self, forCellWithReuseIdentifier: Config.cellID)
    }
    
    private func setupConstraints() {
        var constraints = [NSLayoutConstraint]()
        
        constraints += constraintsCollectionView()
        constraints += constraintsHeaderView()
        constraints += constraintsDescriptionView()
        constraints += constraintsPageControl()
        
        NSLayoutConstraint.activate(constraints)
    }
    
    private func constraintsCollectionView() -> [NSLayoutConstraint] {
        view.addSubview(collectionView)
        return collectionView.edgesAnchor.constraint(equalTo: view.edgesAnchor)
    }
    
    private func constraintsHeaderView() -> [NSLayoutConstraint] {
        view.addSubview(headerView)
        return [
            headerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: .spacing16),
            headerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .spacing20),
            headerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.spacing20)
        ]
    }
    
    private func constraintsDescriptionView() -> [NSLayoutConstraint] {
        view.addSubview(descriptionView)
        return [
            descriptionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .spacing20),
            descriptionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.spacing20),
            descriptionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ]
    }
    
    private func constraintsPageControl() -> [NSLayoutConstraint] {
        view.addSubview(pageControl)
        return [
            pageControl.bottomAnchor.constraint(equalTo: descriptionView.topAnchor, constant: -.spacing8),
            pageControl.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: -.spacing16),
            pageControl.heightAnchor.constraint(equalToConstant: 44)
        ]
    }
    
    private func makeCollectionView() -> UICollectionView {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = .init(width: view.frame.width, height: view.frame.height)
        layout.minimumLineSpacing = .zero
        layout.minimumInteritemSpacing = .zero
        layout.sectionInset = .zero
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.contentInsetAdjustmentBehavior = .never
        collectionView.backgroundColor = .white100
        collectionView.isPagingEnabled = true
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.accessibilityIdentifier = AccessibilityIdentifier.collectionView
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }
    
    private func makeHeaderView() -> HomeHeaderView {
        let view = HomeHeaderView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    private func makePageControl() -> UIPageControl {
        let control = UIPageControl()
        control.hidesForSinglePage = true
        control.numberOfPages = 3
        control.pageIndicatorTintColor = .white100.withAlphaComponent(0.2)
        control.currentPageIndicatorTintColor = .white100
        control.translatesAutoresizingMaskIntoConstraints = false
        return control
    }
    
    private func makeDescriptionView() -> HomeDescriptionView {
        let view = HomeDescriptionView()
        view.onContinue = { [weak self] in
            guard let self, let model = dataSource[safe: currentPageIndexRelay.value] else { return }
            self.router.navigateToDetails(surveyEntity: model.entity, from: self)
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
}

// MARK: - StatePresenter

extension HomeViewController {
    
    private func makeLoadingView() -> UIView {
        let view = HomeLoadingView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    private func makeErrorStateView() -> UIView {
        let view = DSEmptyStateView()
        view.configure(configuration: .init(
            title: L10n.App.Common.EmptyState.Error.title,
            description: L10n.App.Common.EmptyState.Error.desc,
            ctaTitle: L10n.App.Common.EmptyState.Error.actionTitle,
            ctaAction: { [weak self] in
                guard let self = self else { return }
                self.statePresenter.startLoading(animated: false, completionHandler: nil)
                self.viewModel.input.refreshTrigger.accept(())
            }
        ))
        view.backgroundColor = .black
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    private func makeStatePresenter() -> DSStatePresentable {
        return DSStatePresenter(
            stateContainerView: view,
            loadingStateView: loadingView,
            errorStateView: errorStateView
        )
    }
}

// MARK: - Config

extension HomeViewController {
    
    enum Config {
        static let cellID = "survey_overview_cell"
    }
}

// MARK: - AccessibilityIdentifier

extension HomeViewController {
    
    enum AccessibilityIdentifier {
        
        static let collectionView = "home_collection_view"
    }
}
