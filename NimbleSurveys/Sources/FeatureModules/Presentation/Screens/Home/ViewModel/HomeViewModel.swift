//
//  HomeViewModel.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import Foundation
import RxSwift
import RxCocoa

final class HomeViewModel: HomeViewModeInputProtocol {
    
    // MARK: Input Properties
    
    let viewDidLoadTrigger = PublishRelay<Void>()
    let loadMoreTrigger = PublishRelay<Void>()
    let refreshTrigger = PublishRelay<Void>()
    
    // MARK: Output Adapters
    
    private let surveysRelay = BehaviorRelay<[SurveyViewModel]?>(value: nil)
    private let userProfileRelay = PublishRelay<UserProfileEntity>()
    private let totalPageRelay = BehaviorRelay<Int>(value: .zero)
    private let showErrorStateRelay = PublishRelay<Error>()
    private let logoutUserRelay = PublishRelay<Void>()
    
    // MARK: - Properties
    
    private var pageInfo = PageInfo(pageNumber: 1, pageSize: 10)
    
    // MARK: - Dependencies
    
    private let surveyRepository: SurveyRepositoryProtocol
    private let authRepository: AuthenticationRepositoryProtocol
    private let disposeBag = DisposeBag()
    
    // MARK: - Initializers
    
    init(
        surveyRepository: SurveyRepositoryProtocol,
        authRepository: AuthenticationRepositoryProtocol
    ) {
        self.surveyRepository = surveyRepository
        self.authRepository = authRepository
        bindingInputs()
    }
}

// MARK: - Privates

extension HomeViewModel {
    
    private func bindingInputs() {
        viewDidLoadTrigger
            .subscribe(onNext: { [weak self] _ in
                guard let self = self else { return }
                self.fetchSurveyList(for: self.pageInfo)
            })
            .disposed(by: disposeBag)
        
        viewDidLoadTrigger
            .subscribe(onNext: { [weak self] in
                guard let self else { return }
                self.fetchUserProfile()
            })
            .disposed(by: disposeBag)
        
        loadMoreTrigger
            .take(while: { [weak self] _ in
                guard let self else { return false }
                let totalPage = totalPageRelay.value
                return totalPage > self.pageInfo.currentPageOffset
            })
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                self.pageInfo.moveToNextPage()
                self.fetchSurveyList(for: pageInfo)
            })
            .disposed(by: disposeBag)
        
        refreshTrigger
            .subscribe(onNext: { [weak self] in
                guard let self else { return }
                self.reload()
            })
            .disposed(by: disposeBag)
    }
    
    private func reload() {
        fetchSurveyList(for: pageInfo)
        fetchUserProfile()
    }
    
    private func fetchSurveyList(for page: PageInfo) {
        surveyRepository.fetchSurveyList(pageNumber: page.pageNumber, pageSize: page.pageSize)
            .subscribe(
                onNext: { [weak self] entity in
                    guard let self else { return }
                    self.totalPageRelay.accept((entity.meta?.records).or(.zero))
                    self.performUpdateSurveys(entity.data.or([]))
                },
                onError: { [weak self] error in
                    guard let self else { return }
                    self.handleAPIError(error)
                }
            )
            .disposed(by: disposeBag)
    }
    
    private func performUpdateSurveys(_ surveys: [SurveyEntity]) {
        
        let newSurveyViewModel = surveys.map { SurveyViewModel(entity: $0) }
        guard let currentSurveys = surveysRelay.value else {
            surveysRelay.accept(newSurveyViewModel)
            return
        }
        
        let updatedSurveys = currentSurveys + newSurveyViewModel
        surveysRelay.accept(updatedSurveys)
    }
    
    private func fetchUserProfile() {
        authRepository.fetchUserProfile()
            .subscribe(
                onNext: { [weak self] profileEntity in
                    guard let self else { return }
                    self.userProfileRelay.accept(profileEntity)
                },
                onError: { [weak self] error in
                    guard let self else { return }
                    self.handleAPIError(error)
                }
            )
            .disposed(by: disposeBag)
    }
    
    private func handleAPIError(_ error: Error) {
        guard let appError = error as? AppError else {
            // TOOD: Add another
            return
        }
        
        switch appError {
        case .unAuthorized, .invalidRefreshToken:
            logoutUserRelay.accept(())
            
        default:
            if pageInfo.isFirstPage {
                showErrorStateRelay.accept(appError)
            }
        }
    }
}

// MARK: - HomeViewModelOutputProtocol

extension HomeViewModel: HomeViewModelOutputProtocol {
    var surveys: Driver<[SurveyViewModel]> {
        return surveysRelay.compactMap { $0 }
            .asDriver(onErrorDriveWith: .never())
    }
    
    var headerSubTitle: Driver<String> {
        return .just(Date().formattedDateTime().uppercased())
    }
    
    var headerTitle: Driver<String> {
        return .just(L10n.App.Home.Today.title)
    }
    
    var profileImageUrl: Driver<String> {
        return userProfileRelay
            .compactMap {  $0.avatarUrl }
            .asDriver(onErrorDriveWith: .never())
    }
    
    var totalPage: Driver<Int> {
        return surveysRelay.compactMap { $0?.count }
            .distinctUntilChanged()
            .asDriver(onErrorDriveWith: .never())
    }
    
    var showErrorState: Driver<Error> {
        return showErrorStateRelay
            .asDriver(onErrorDriveWith: .never())
    }
    
    var logoutUser: Driver<Void> {
        return logoutUserRelay
            .asDriver(onErrorDriveWith: .never())
    }
}

// MARK: - HomeViewModelProtocol

extension HomeViewModel: HomeViewModelProtocol {
    
    var input: HomeViewModeInputProtocol {
        return self
    }
    
    var output: HomeViewModelOutputProtocol {
        return self
    }
}
