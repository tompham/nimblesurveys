//
//  HomeViewModelProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import Foundation
import RxSwift
import RxCocoa

protocol HomeViewModelOutputProtocol: AnyObject {
    var surveys: Driver<[SurveyViewModel]> { get }
    
    var headerSubTitle: Driver<String> { get }
    
    var headerTitle: Driver<String> { get }
    
    var profileImageUrl: Driver<String> { get }
    
    var totalPage: Driver<Int> { get }
    
    var showErrorState: Driver<Error> { get }
    
    var logoutUser: Driver<Void> { get }
}

protocol HomeViewModeInputProtocol: AnyObject {
    
    var viewDidLoadTrigger: PublishRelay<Void> { get }
    
    var loadMoreTrigger: PublishRelay<Void> { get }
    
    var refreshTrigger: PublishRelay<Void> { get }
}

protocol HomeViewModelProtocol {
    
    var input: HomeViewModeInputProtocol { get }
    
    var output: HomeViewModelOutputProtocol { get }
}
