//
//  KeyedDecodingContainer+Extensions.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation

// MARK: - KeyedDecodingContainerDateType

public enum KeyedDecodingContainerDateType {
    case timeIntervalSince1970
    case dateFormatter(DateFormatter)
}

// MARK: - Date

public extension KeyedDecodingContainer {
    func decode(_ dateType: KeyedDecodingContainerDateType, forKey key: KeyedDecodingContainer<K>.Key) throws -> Date {
        switch dateType {
        case .timeIntervalSince1970:
            let timestamp = try decode(TimeInterval.self, forKey: key)
            return Date(timeIntervalSince1970: timestamp)
        case .dateFormatter(let dateFormatter):
            let string = try decode(String.self, forKey: key)
            guard let date = dateFormatter.date(from: string) else {
                throw NSError(domain: "com.shopback.shopbackcore.error", code: -1, userInfo: [
                    NSDebugDescriptionErrorKey: "Cannot convert date from `\(string)`"
                    ])
            }
            return date
        }
    }

    func decodeIfPresent(_ dateType: KeyedDecodingContainerDateType, forKey key: KeyedDecodingContainer<K>.Key) throws -> Date? {
        switch dateType {
        case .timeIntervalSince1970:
            guard let timestamp = try decodeIfPresent(TimeInterval.self, forKey: key) else { return nil }
            return Date(timeIntervalSince1970: timestamp)
        case .dateFormatter(let dateFormatter):
            guard let string = try decodeIfPresent(String.self, forKey: key) else { return nil }
            return dateFormatter.date(from: string)
        }
    }
}


// MARK: - Decode

extension KeyedDecodingContainer {
    public func decodeIfPresent<T: Decodable>(key: Key) throws -> T? {
        return try decodeIfPresent(T.self, forKey: key)
    }

    public func decode<T: Decodable>(key: Key) throws -> T {
        return try decode(T.self, forKey: key)
    }

    /// Same as decodeIfPresent<T>(key:) but will catch the error and return nil
    public func decodeIfPossible<T: Decodable>(key: Key) -> T? {
        do {
            return try decodeIfPresent(T.self, forKey: key)
        } catch {
            return nil
        }
    }

    public func decodeIfPossible(
        _ dateType: KeyedDecodingContainerDateType,
        forKey key: Key
    ) -> Date? {
        do {
            return try decodeIfPresent(dateType, forKey: key)
        } catch {
            return nil
        }
    }
}
