//
//  NSError+Exntensions.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation

extension NSError {

    public convenience init(message: String) {
        self.init(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: message])
    }

    public var message: String {
        return localizedDescription
    }

    /// Describe some unhandled network error code that will fallback to generic error message display
    /// Read more: https://developer.apple.com/documentation/foundation/urlerror/code
    public static let genericErrors: [URLError.Code] = [
        .unknown, .cancelled, .badURL, .timedOut, .unsupportedURL,
        .cannotFindHost, .cannotConnectToHost, .dnsLookupFailed,
        .httpTooManyRedirects, .resourceUnavailable, .notConnectedToInternet,
        .redirectToNonExistentLocation, .badServerResponse,
        .userCancelledAuthentication, .userAuthenticationRequired
    ]

}
