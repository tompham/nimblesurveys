//
//  Observable+Extensions.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import RxSwift
import Japx

struct GroupsResponseWrapper<CodableType: Decodable>: Decodable {
    var groups: CodableType
}

public struct DataResponseWrapper<CodableType: Decodable>: Decodable {
    public internal(set) var data: CodableType
}

public struct MetaDataResponseWrapper<CodableType: Decodable>: Decodable {
    
    public internal(set) var meta: CodableType
}

public enum ResponseWrapperKey {
    case groups
    case data
    case meta
    case none
}

public extension Observable where Element == Data {
    func decode<DecodeType: Decodable>(
        key: ResponseWrapperKey = .none,
        decoder: DecoderProtocol = JSONDecoder()
    ) -> Observable<DecodeType> {
        return flatMap { data -> Observable<DecodeType> in
            do {
                let casted: DecodeType
                switch key {
                case .groups:
                    casted = try decoder.decode(GroupsResponseWrapper<DecodeType>.self, from: data).groups
                case .data:
                    casted = try decoder.decode(DataResponseWrapper<DecodeType>.self, from: data).data
                    
                case .meta:
                    casted = try decoder.decode(MetaDataResponseWrapper<DecodeType>.self, from: data).meta
                    
                case .none:
                    casted = try decoder.decode(DecodeType.self, from: data)
                }
                return .just(casted)
           } catch {
                return .error(error)
            }
        }
    }
}
