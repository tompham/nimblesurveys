//
//  String+Extensions.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation

/// String extensions

extension String {

    /// An "" string.
    static let empty = ""
}
