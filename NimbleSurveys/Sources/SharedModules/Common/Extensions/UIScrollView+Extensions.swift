//
//  UIScrollView+Extensions.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 08/01/2024.
//

import Foundation
import RxSwift
import RxCocoa

public extension Reactive where Base: UIScrollView {
    
    /// Observable sequence of content offset changes.
    ///
    /// This observable sequence never completes.
    ///
    ///  - Parameter availableRemainingScreen: The remaining screen to trigger load more.
    ///
    ///  - Returns: Observable sequence of content offset changes.
    func loadMoreTrigger(
        availableRemainingScreen: CGFloat = 5
    ) -> Observable<Void> {
        
        return contentOffset
            .map { $0.x }
            .distinctUntilChanged()
            .filter { [weak base] _ -> Bool in
                
                guard let scrollView = base else { return false }
                
                var refreshControl: UIRefreshControl?
                if #available(iOS 10.0, *) {
                    refreshControl = scrollView.refreshControl
                } else {
                    refreshControl = scrollView.subviews.first(where: { $0 is UIRefreshControl }) as? UIRefreshControl
                }
                guard refreshControl?.isRefreshing != true else {
                    return false
                }
                
                let currentOffsetX = scrollView.contentOffset.x
                guard currentOffsetX > 0 else { return false }
                
                let contentWidth = scrollView.contentSize.width
                let visibleWidth = scrollView.frame.width
                let remainingScreen = (contentWidth - currentOffsetX) / visibleWidth
                
                return remainingScreen <= availableRemainingScreen
            }
            .map { _ in () }
            .throttle(.milliseconds(500), latest: false, scheduler: MainScheduler.instance)
    }
}
