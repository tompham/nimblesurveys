//
//  UIView+Extensions.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import UIKit

extension UIView {
    func setHidden(
        _ isHidden: Bool,
        animated: Bool,
        duration: TimeInterval = 0.3,
        delay: TimeInterval = 0.1,
        forced: Bool = false,
        completion: (() -> Void)? = nil
    ) {

        guard self.isHidden != isHidden || forced else {
            // Avoid to flashing while UIView is hidden and attempt to set hidden
            completion?()
            return
        }

        if animated {
            let startAlpha: CGFloat
            let animatingAlpha: CGFloat
            if isHidden {
                startAlpha = 1
                animatingAlpha = 0
            } else {
                startAlpha = 0
                animatingAlpha = 1
            }
            alpha = startAlpha
            self.isHidden = false
            UIView.animate(
                withDuration: duration,
                delay: delay,
                options: .curveEaseOut,
                animations: { [weak self] in
                    self?.alpha = animatingAlpha
                },
                completion: { [weak self] _ in
                    self?.isHidden = isHidden
                    self?.alpha = 1.0
                    completion?()
                }
            )
        } else {
            self.isHidden = isHidden
            completion?()
        }
    }
}
