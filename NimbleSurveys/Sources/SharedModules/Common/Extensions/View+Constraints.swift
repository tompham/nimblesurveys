//
//  View+Constraints.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import UIKit

public extension UIView {
    struct EdgesAnchor {
        var topAnchor: NSLayoutYAxisAnchor
        var leadingAnchor: NSLayoutXAxisAnchor
        var bottomAnchor: NSLayoutYAxisAnchor
        var trailingAnchor: NSLayoutXAxisAnchor
    }

    var edgesAnchor: UIView.EdgesAnchor {
        return UIView.EdgesAnchor(topAnchor: topAnchor,
                                  leadingAnchor: leadingAnchor,
                                  bottomAnchor: bottomAnchor,
                                  trailingAnchor: trailingAnchor)
    }
}

public extension UIView.EdgesAnchor {
    func constraint(equalTo edges: UIView.EdgesAnchor, insets: UIEdgeInsets = .zero) -> [NSLayoutConstraint] {
        return [
            topAnchor.constraint(equalTo: edges.topAnchor, constant: insets.top),
            leadingAnchor.constraint(equalTo: edges.leadingAnchor, constant: insets.left),
            trailingAnchor.constraint(equalTo: edges.trailingAnchor, constant: insets.right),
            bottomAnchor.constraint(equalTo: edges.bottomAnchor, constant: insets.bottom)
        ]
    }
}
