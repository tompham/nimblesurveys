//
//  PageInfo.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 07/01/2024.
//

import Foundation

struct PageInfo {
    
    var pageNumber: Int
    let pageSize: Int
    
    init(pageNumber: Int = 1, pageSize: Int = 10) {
        self.pageNumber = pageNumber
        self.pageSize = pageSize
    }
    
    mutating func moveToNextPage() {
        pageNumber += 1
    }
    
    mutating func reset() {
        pageNumber = 1
    }
    
    var isFirstPage: Bool {
        return pageNumber == 1
    }
    
    var currentPageOffset: Int {
        return pageNumber * pageSize
    }
}
