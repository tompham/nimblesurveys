//
//  Sample.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation

protocol SampleProtocol: AnyObject {
    
    func run() 
}
