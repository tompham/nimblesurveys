//
//  DataValidator.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 11/01/2024.
//

import Foundation

final class DataValidator {}

extension DataValidator: DataValidatorProtocol {
    
    func validateEmail(_ email: String) throws -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        if !emailPredicate.evaluate(with: email) {
            throw EmailValidationError.invalid
        }
        
        return true
    }
}
