//
//  DataValidatorProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 11/01/2024.
//

import Foundation

protocol DataValidatorProtocol: AnyObject {
    
    /// Validate email
    /// - Parameter email: email to validate
    ///
    /// - Throws: EmailValidationError
    ///
    /// - Returns: true if email is valid
    func validateEmail(_ email: String) throws -> Bool
}
