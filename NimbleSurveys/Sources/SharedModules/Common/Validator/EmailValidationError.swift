//
//  EmailValidationError.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 11/01/2024.
//

import Foundation

enum EmailValidationError: Error {
    /// Invalid email
    case invalid
    
    var errorMessage: String {
        switch self {
        case .invalid:
            return L10n.App.Common.ValidationError.invalidEmail
        }
    }
}
