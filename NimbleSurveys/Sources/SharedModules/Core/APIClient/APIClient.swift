//
//  APIClient.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import Moya
import RxSwift

class APIProvider: MoyaProvider<MultiTarget> {
    
    static let `default` = APIProvider(
        tokenProvider: CredentialsProvider.default
    )
    
    public init(
        tokenProvider: CredentialsProviderProtocol
    ) {
        super.init(
            plugins: [
                NetworkLoggerPlugin(
                    configuration: .init(
                        logOptions: .verbose
                    )
                ),
                AccessTokenPlugin(tokenClosure: { target in
                    guard target is AccessTokenAuthorizable else {
                        return .empty
                    }
                    
                    return tokenProvider.getAccessToken().orEmpty
                })
            ]
        )
    }
}

final class APIClient {
    
    // MARK: - Defaults
    
    static let `default` = APIClient(
        provider: .default,
        credentialsProvider: CredentialsProvider.default,
        refreshTokenTargetProvider: RefreshTokenTargetProvider()
    )
    
    // MARK: - Dependencies
    
    let provider: APIProvider
    let credentialsProvider: CredentialsProviderProtocol
    let refreshTokenTargetProvider: RefreshTokenTargetProviderProtocol
    
    // MARK: - Initializers
    
    init(
        provider: APIProvider,
        credentialsProvider: CredentialsProviderProtocol,
        refreshTokenTargetProvider: RefreshTokenTargetProviderProtocol
    ) {
        self.provider = provider
        self.credentialsProvider = credentialsProvider
        self.refreshTokenTargetProvider = refreshTokenTargetProvider
    }
}

// MARK: - APIClientProtocol

extension APIClient: APIClientProtocol {
    
    func request(target: TargetType) -> Single<Data> {
        return provider.rx.request(MultiTarget(target))
            .flatMap { [weak self] response in
                guard let self else { return .just(response) }
                return self.refreshTokenIfNeeded(response, target: target)
            }
            .flatMap { [weak self] response -> Single<Data> in
                guard let self else { return .error(AppError.unknown) }
                do {
                    let data = try self.validate(response)
                    return .just(data)
                } catch {
                    return .error(error)
                }
            }
            .catch { error in
                
                if let apiError = error as? APIError {
                    return .error(apiError)
                }
                
                if let moyaError = error as? MoyaError {
                    return .error(AppError.underlyingError(error: moyaError))
                }
                
                return .error(error)
            }
    }
}

// MARK: - Privates

extension APIClient {
    
    private func refreshTokenIfNeeded(_ response: Response, target: TargetType) -> Single<Response> {
        guard response.statusCode == APIErrorCode.unauthorized else {
            return .just(response)
        }
        
        return refreshToken()
            .flatMap { [weak self] isSuccess in
                guard let self, isSuccess else {
                    return .just(response)
                }
                
                return self.provider.rx.request(MultiTarget(target))
            }
    }
    
    private func refreshToken() -> Single<Bool> {
        guard let refreshToken = credentialsProvider.getRefreshToken(), refreshToken.isNotEmpty else {
            return .error(AppError.invalidRefreshToken)
        }
        
        let refreshTokenTarget = refreshTokenTargetProvider.createRefreshTokenTarget(refreshToken: refreshToken)
        return provider.rx.request(MultiTarget(refreshTokenTarget))
            .map { [weak self] response -> Data? in
                guard let self else { return nil }
                do {
                    return try self.validate(response)
                } catch {
                    throw error
                }
            }
            .do(onSuccess: { [weak self] data in
                guard let self, let data else { return }
                self.credentialsProvider.store(credentials: data)
            })
            .map { _ in true }
            .catch { [weak self] error in
                guard let self else { return .error(error) }
                self.credentialsProvider.clearCredentials()
                return .error(error)
            }
    }
    
    private func validate(_ response: Response) throws -> Data {
        switch response.statusCode {
        case 200..<300:
            return response.data
            
        case 401:
            throw AppError.unAuthorized
            
        case 500:
            throw AppError.internalServerError
            
        default:
            guard let apiError = try? response.map([APIError].self, atKeyPath: "errors").first
            else {
                throw AppError.unknown
            }
            
            throw AppError.apiError(error: apiError)
        }
    }
}
