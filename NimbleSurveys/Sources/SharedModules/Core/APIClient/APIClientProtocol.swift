//
//  APIClientProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import Moya
import RxSwift

/// Protocol for API client
/// 
/// - Note: This protocol is used to define API client.
/// 
/// - Example:
///  ```
/// let apiClient = APIClient()
/// apiClient.request(target: target)
///     .subscribe(
///         onSuccess: { (response: O) in
///
///         },
///         onError: { (error: Error) in
///            // handle Error
///         })
/// ```
/// 
/// - SeeAlso: `APIClient`
protocol APIClientProtocol: AnyObject {
    
    /// Request to call API
    /// 
    /// - Parameters:
    ///    - target: The target to request
    
    /// - Returns: The data from response
    func request(target: TargetType) -> Single<Data>
}
