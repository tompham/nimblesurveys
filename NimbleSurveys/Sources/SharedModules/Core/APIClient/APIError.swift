//
//  APIError.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation

struct APIError: Error {
    let source: String?
    let code: String?
    let message: String?
    
    init(
        source: String? = nil,
        code: String? = nil,
        message: String? = nil
    ) {
        self.source = source
        self.code = code
        self.message = message
    }
}

extension APIError: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case source
        case message = "detail"
        case code
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        source = container.decodeIfPossible(key: .source)
        code = container.decodeIfPossible(key: .code)
        message = container.decodeIfPossible(key: .message)
    }
}

extension APIError: Equatable {}
