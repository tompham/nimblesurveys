//
//  APIErrorCode.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation

/// API error code
/// 
/// - Note: This enum is used to define API error code.
enum APIErrorCode {
    
    /// The unauthorized error code.
    static let unauthorized = 401
}
