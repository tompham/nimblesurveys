//
//  CredentialsProvider.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import KeychainAccess
import Japx

enum KeychainAccessStorageKey {
    
    static let credentials = "credentials"
}

final class CredentialsProvider {
    
    static let `default` = CredentialsProvider(
        keychain: Keychain(
            service: Bundle.main.bundleIdentifier.or("tompham.nimble.surveys")
        ),
        decoder: JapxDecoder()
    )
    
    private let keychain: Keychain
    private let decoder: DecoderProtocol
    
    init(keychain: Keychain, decoder: DecoderProtocol) {
        self.keychain = keychain
        self.decoder = decoder
    }
}

// MARK: - AccessTokenProviderProtocol\

extension CredentialsProvider: CredentialsProviderProtocol {
    
    func getAccessToken() -> String? {
        return retrieveCredentials()?.accessToken
    }
    
    func getRefreshToken() -> String? {
        return retrieveCredentials()?.refreshToken
    }
    
    @discardableResult
    func store(credentials: Data) -> Bool {
        guard let _ = try? keychain.set(credentials, key: KeychainAccessStorageKey.credentials) else {
            return false
        }
        
        return true
    }
    
    func retrieveCredentials() -> CredentialsResponse? {
        guard let data = try? keychain.getData(KeychainAccessStorageKey.credentials) else {
            return nil
        }
        
        return try? decoder.decode(DataResponseWrapper<CredentialsResponse>.self, from: data).data
    }
    
    
    @discardableResult
    func clearCredentials() -> Bool {
        guard let _ = try? keychain.remove(KeychainAccessStorageKey.credentials) else {
            return false
        }
        
        return true
    }
}
