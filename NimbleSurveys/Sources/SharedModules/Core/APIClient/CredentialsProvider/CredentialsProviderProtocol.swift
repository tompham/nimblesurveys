//
//  CredentialsProviderProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation

/// Protocol for providing access token and refresh token
/// 
/// - Note: This protocol is used to provide access token and refresh token for API client.
/// 
/// - Example:
///   ```
///   let accessToken = credentialsProvider.getAccessToken()
///   let refreshToken = credentialsProvider.getRefreshToken()
///   ```
protocol CredentialsProviderProtocol {
    
    /// Get access token on the Keychain
    /// 
    /// - Returns: Access token string
    func getAccessToken() -> String?

    /// Get refresh token on the Keychain
    /// 
    /// - Returns: Refresh token string
    func getRefreshToken() -> String?
    
    @discardableResult
    func store(credentials: Data) -> Bool
    
    func retrieveCredentials() -> CredentialsResponse?
    
    
    @discardableResult
    func clearCredentials() -> Bool
}
