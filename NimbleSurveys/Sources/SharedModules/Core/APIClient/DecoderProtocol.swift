//
//  DecoderProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import Japx

/// A protocol that defines a type that can decode values from external representations.
///
/// You can use a decoder to decode any type that conforms to the `Decodable` protocol, including your own custom types.
///
/// For example, to decode a custom type from JSON, create a `JSONDecoder` instance, and call its `decode(_:from:)` method:
///
///     let json = """
///     {
///       "name": "Taylor Swift",
///      "age": 26
///     }
///     """.data(using: .utf8)!
///
///     struct User: Codable {
///         var name: String
///         var age: Int
///     }
///
///    let decoder = JSONDecoder()
///    let user = try decoder.decode(User.self, from: json)
public protocol DecoderProtocol {
 
    /// Decode the given data into the provided type.
    ///
    /// - Parameters:
    ///    - type: The type to decode into.
    ///   - data: The data to decode.
    /// - Returns: The decoded value.
    func decode<T>(_ type: T.Type, from data: Data) throws -> T where T : Decodable
}

// MARK: - JSONDecoder

extension JSONDecoder: DecoderProtocol {}

// MARK: - JapxDecoder

extension JapxDecoder: DecoderProtocol {
    public func decode<T>(_ type: T.Type, from data: Data) throws -> T where T : Decodable {
        return try decode(type, from: data, includeList: nil)
    }
}
