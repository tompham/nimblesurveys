//
//  EncoderProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation

/// Encoder protocol

public protocol EncoderProtocol {
    
    /// Encode the given top-level value into JSON data.
    /// 
    /// - Parameters:
    ///  - value: The value to encode.
    /// 
    /// - Returns: The encoded JSON data.
    func encode<T>(_ value: T) throws -> Data where T : Encodable
}

// MARK: - JSONEncoder

extension JSONEncoder: EncoderProtocol {}
