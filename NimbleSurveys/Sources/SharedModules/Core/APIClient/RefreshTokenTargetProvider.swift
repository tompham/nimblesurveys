//
//  RefreshTokenTargetProvider.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import Moya

/// Protocol for providing refresh token target
/// 
/// - Note: This protocol is used to provide refresh token target for API client.
final class RefreshTokenTargetProvider {
    
}

// MARK: - RefreshTokenTargetProviderProtocol

extension RefreshTokenTargetProvider: RefreshTokenTargetProviderProtocol {
    
    func createRefreshTokenTarget(refreshToken: String) -> TargetType {
        return RefreshTokenTarget(refreshToken: refreshToken)
    }
}

// MARK: - RefreshTokenTarget

struct RefreshTokenTarget: TargetType {
    
    let refreshToken: String
    
    var baseURL: URL {
        return URL(string: "https://survey-api.nimblehq.co")!
    }
    
    var path: String {
        return "api/v1/oauth/token"
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var task: Task {
        let parameters = [
            "grant_type": "refresh_token",
            "refresh_token": refreshToken,
            "client_id": APISecretInfo.clientID,
            "client_secret": APISecretInfo.clientSecret
        ]
        return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }
    
    var headers: [String : String]? {
        return nil
    }
}
