//
//  AuthenticationState.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation

final class AuthenticationState {
    
    static let `shared` = AuthenticationState(
        credentialsProvider: CredentialsProvider.default
    )
    
    // MARK: - Dependencies
    
    private let credentialsProvider: CredentialsProviderProtocol
    
    // MARK: - Initializers

    init(credentialsProvider: CredentialsProviderProtocol) {
        self.credentialsProvider = credentialsProvider
    }
    
    /// Returns the authentication state.
    /// 
    /// - Note: This property is used to check the authentication state.
    var isAuthenticated: Bool {
        
        if let credentials = credentialsProvider.retrieveCredentials(),
           credentials.accessToken.orEmpty.isNotEmpty,
           credentials.refreshToken.orEmpty.isNotEmpty {
            return true
        }
        
        return false
    }
}
