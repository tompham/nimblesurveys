//
//  DSButton.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import UIKit

class DSButton: UIButton {

    private lazy var loadingIndicator = makeLoadingIndicator()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public APIs to control loading

    func showLoading() {
        showIndicator(true)
    }

    func hideLoading() {
        showIndicator(false)
    }
}

extension DSButton {
    private func setupView() {
        
        backgroundColor = .secondaryBackground
        titleLabel?.font = .title
        setTitleColor(.black, for: .normal)
        setTitleColor(.gray, for: .disabled)
        layer.cornerRadius = .spacing12
        setupConstraints()
    }
    
    private func setupConstraints() {
        var constraints = [NSLayoutConstraint]()
        
        constraints += [
            heightAnchor.constraint(equalToConstant: 56)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }

    private func constraintsLoadingIndicator() -> [NSLayoutConstraint] {
        
        addSubview(loadingIndicator)
        return [
            loadingIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            loadingIndicator.centerYAnchor.constraint(equalTo: centerYAnchor),
            loadingIndicator.heightAnchor.constraint(equalTo: loadingIndicator.widthAnchor),
            loadingIndicator.widthAnchor.constraint(equalToConstant: 40)
        ]
    }
    
    private func addLoadingIndicator() {
        addSubview(loadingIndicator)
        NSLayoutConstraint.activate(constraintsLoadingIndicator())
    }
    
    private func showIndicator(_ isLoading: Bool) {
        
        guard isLoading else {
            titleLabel?.alpha = 1
            loadingIndicator.stopAnimating()
            loadingIndicator.removeFromSuperview()
            return
        }
        
        titleLabel?.alpha = .zero
        addLoadingIndicator()
        loadingIndicator.startAnimating()
    }
    
    
    private func makeLoadingIndicator() -> UIActivityIndicatorView {
        let view = UIActivityIndicatorView()
        view.isHidden = true
        view.hidesWhenStopped = false
        view.color = .gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
}
