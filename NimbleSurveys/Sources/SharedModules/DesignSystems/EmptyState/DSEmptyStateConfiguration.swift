//
//  DSEmptyStateConfiguration.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 08/01/2024.
//

import Foundation

struct DSEmptyStateConfiguration {
    
    let asset: AssetType?
    let title: String?
    let description: String?
    let ctaTitle: String?
    let ctaAction: VoidCallBack?
    
    init(
        asset: AssetType? = nil,
        title: String? = nil,
        description: String? = nil,
        ctaTitle: String? = nil,
        ctaAction: VoidCallBack? = nil
    ) {
        self.asset = asset
        self.title = title
        self.description = description
        self.ctaTitle = ctaTitle
        self.ctaAction = ctaAction
    }
}
