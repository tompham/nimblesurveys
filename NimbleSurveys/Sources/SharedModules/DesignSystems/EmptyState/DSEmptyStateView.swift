//
//  DSEmptyStateView.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 08/01/2024.
//

import UIKit
import SDWebImage

final class DSEmptyStateView: UIView {
    
    // MARK: - UI
    
    private lazy var containerView = makeContainerView()
    private lazy var infoContainerView = makeInfoContainerView()
    private lazy var imageView = makeImageView()
    private lazy var titleLabel = makeTitleLabel()
    private lazy var descriptionLabel = makeDescriptionLabel()
    private lazy var ctaButton = makeCTAButton()
    
    // MARK: - Callback
    
    var onCTA: VoidCallBack?
    
    // MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(configuration config: DSEmptyStateConfiguration) {
        
        configureImage(asset: config.asset)
        configureTitle(title: config.title)
        configureDescription(description: config.description)
        configureCTAButton(title: config.ctaTitle, ctaAction: config.ctaAction)
    }
}

// MARK: - Data Configuration

extension DSEmptyStateView {
    
    private func configureImage(asset: AssetType?) {
        
        guard let asset else {
            imageView.isHidden = true
            return
        }
        
        switch asset {
        case .local(let image):
            imageView.image = image
            
        case .remote(let imageURLString):
            imageView.sd_setImage(with: URL(string: imageURLString))
        }
        
        imageView.isHidden = false
    }
    
    private func configureTitle(title: String?) {
        
        guard let title else {
            titleLabel.isHidden = true
            return
        }
        
        titleLabel.text = title
        titleLabel.isHidden = false
    }
    
    private func configureDescription(description: String?) {
        guard let description else {
            descriptionLabel.isHidden = true
            return
        }
        
        descriptionLabel.text = description
        descriptionLabel.isHidden = false
    }
    
    private func configureCTAButton(title: String?, ctaAction: VoidCallBack?) {
        guard let title, let ctaAction else {
            ctaButton.isHidden = true
            return
        }
        
        ctaButton.setTitle(title, for: .normal)
        self.onCTA = ctaAction
    }
}

// MARK: - Private

extension DSEmptyStateView {
    
    private func setupViews() {
        setupConstraints()
    }
    
    private func setupConstraints() {
        var constraints = [NSLayoutConstraint]()
        
        constraints += constraintsContainerView()
        NSLayoutConstraint.activate(constraints)
    }
    
    private func constraintsContainerView() -> [NSLayoutConstraint] {
        
        addSubview(containerView)
        
        containerView.addArrangedSubview(imageView)
        
        containerView.addArrangedSubview(infoContainerView)
        infoContainerView.addArrangedSubview(titleLabel)
        infoContainerView.addArrangedSubview(descriptionLabel)
        
        containerView.addArrangedSubview(ctaButton)
        
        return [
            containerView.topAnchor.constraint(greaterThanOrEqualTo: topAnchor, constant: .spacing16),
            containerView.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: -.spacing16),
            containerView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: .spacing16),
            containerView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -.spacing16),
            containerView.centerYAnchor.constraint(equalTo: centerYAnchor),
            containerView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ]
    }
    
    private func makeContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = .spacing28
        view.alignment = .center
        view.distribution = .fill
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    private func makeInfoContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .center
        view.distribution = .fill
        view.spacing = .spacing16
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    private func makeImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
    
    private func makeTitleLabel() -> UILabel {
        let label = UILabel()
        label.font = UIFont.display2
        label.textColor = .white100
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
    
    private func makeDescriptionLabel() -> UILabel {
        let label = UILabel()
        label.font = UIFont.paragrapRegular
        label.textColor = .white100
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
    
    private func makeCTAButton() -> DSButton {
        let button = DSButton()
        button.setTitle("Reload", for: .normal)
        button.addTarget(self, action: #selector(didTapCTAButton), for: .touchUpInside)
        button.contentEdgeInsets = .init(top: .zero, left: .spacing16, bottom: .zero, right: .spacing16)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
    
    @objc private func didTapCTAButton() {
        onCTA?()
    }
    
}
