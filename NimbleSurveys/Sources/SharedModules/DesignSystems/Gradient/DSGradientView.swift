//
//  DSGradientView.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import UIKit

class DSGradientView: UIView {
    
    private let colors: [CGColor]
    private lazy var gradientLayer = CAGradientLayer()
    
    init(
        colors: [UIColor] =  [
            UIColor.black.withAlphaComponent(0),
            UIColor.black
        ]
    ) {
        self.colors = colors.map(\.cgColor)
        super.init(frame: .zero)
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }
}

// MARK: - Privates

extension DSGradientView {
    
    private func setupView() {
        gradientLayer.colors = colors
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at: .zero)
    }
}
