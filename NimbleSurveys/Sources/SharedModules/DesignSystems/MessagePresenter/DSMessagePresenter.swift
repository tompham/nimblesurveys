//
//  DSMessagePresenter.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 10/01/2024.
//

import SwiftMessages

final class DSMessagePresenter {}

extension DSMessagePresenter: DSMessagePresenterProtocol {
    
    func showNotificationView(title: String, message: String) {
        
        var configuration: SwiftMessages.Config {
            var config = SwiftMessages.Config()
            config.presentationContext = .window(windowLevel: .statusBar)
            config.duration = .automatic
            return config
        }
        
        let view = DSMessageView()
        view.configure(
            icon: Asset.Icon.icNotification.image,
            title: L10n.App.ForgotPassword.Notification.CheckEmail.title,
            description: message
        )
        SwiftMessages.show(config: configuration,  view: view)
    }
    
    func showErrorMessage(_ message: String) {
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureTheme(.error)
        view.button?.isHidden = true
        view.configureContent(title: .empty, body: message)
        view.configureDropShadow()
        SwiftMessages.show(view: view)
    }
}
