//
//  DSMessagePresenterProtocol.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 10/01/2024.
//

import Foundation

protocol DSMessagePresenterProtocol {
    
    func showNotificationView(title: String, message: String)
    
    func showErrorMessage(_ message: String) 
}
