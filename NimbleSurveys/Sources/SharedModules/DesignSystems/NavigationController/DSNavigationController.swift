//
//  DSNavigationController.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 08/01/2024.
//

import UIKit

class DSNavigationController: UINavigationController, UIGestureRecognizerDelegate, UINavigationControllerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.delegate = self
        delegate = self
        
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
        navigationBar.tintColor = .white
    }

      override func pushViewController(_ viewController: UIViewController, animated: Bool) {
          super.pushViewController(viewController, animated: animated)
          interactivePopGestureRecognizer?.isEnabled = false
      }

      func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
          interactivePopGestureRecognizer?.isEnabled = true
      }

      func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
          return viewControllers.count > 1
      }
}

