//
//  DSMessageView.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 10/01/2024.
//

import UIKit

final class DSMessageView: UIView {

    private lazy var containerView = makeContainerView()
    private lazy var iconImageView = makeIconImageView()
    private lazy var infoContainerView = makeInfoContentContainerView()
    private lazy var titleLabel = makeTitleLabel()
    private lazy var descriptionLabel = makeDescriptionLabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(icon: UIImage? = nil, title: String? = nil, description: String? = nil) {
        iconImageView.image = icon
        titleLabel.text = title
        descriptionLabel.text = description
    }
}

extension DSMessageView {
    
    private func setupViews() {
        backgroundColor = .gray60
        setupConstraints()
    }
    
    private func setupConstraints() {
        var constraints = [NSLayoutConstraint]()
        
        constraints += constraintsContainerView()
        constraints += constraintsIconImageView()
        
        NSLayoutConstraint.activate(constraints)
    }
    
    private func constraintsContainerView() -> [NSLayoutConstraint] {
        addSubview(containerView)
        
        containerView.addArrangedSubview(iconImageView)
        containerView.addArrangedSubview(infoContainerView)
        
        infoContainerView.addArrangedSubview(titleLabel)
        infoContainerView.addArrangedSubview(descriptionLabel)
        return [
            containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: .spacing16),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -.spacing20),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .spacing20),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.spacing20)
        ]
    }
    
    private func constraintsIconImageView() -> [NSLayoutConstraint] {
        return [
            iconImageView.widthAnchor.constraint(equalTo: iconImageView.heightAnchor),
            iconImageView.heightAnchor.constraint(equalToConstant: 32)
        ]
    }
    
    private func makeContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .horizontal
        view.distribution = .fill
        view.alignment = .top
        view.spacing = .spacing12
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    private func makeIconImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
    
    private func makeInfoContentContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = .spacing4
        view.distribution = .fill
        view.alignment = .leading
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    private func makeTitleLabel() -> UILabel {
        let label = UILabel()
        label.font = .smallSemibold
        label.textColor = .white100
        label.textAlignment = .left
        label.numberOfLines = .zero
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
    
    private func makeDescriptionLabel() -> UILabel {
        let label = UILabel()
        label.font = .xSmallRegular
        label.textColor = .white100.withAlphaComponent(0.7)
        label.textAlignment = .left
        label.numberOfLines = .zero
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
}
