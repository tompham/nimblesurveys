//
//  DSStatePresentable.swift
//  EarnMore
//
//  Created by Tuan Pham on 13/04/2023.
//

import UIKit

/// This protocol represents the all states of a view that have to load data from a server.
/// At this time this protocol will support to show 3 states of a UIView or UIViewController which will need a show a placeholder when they are loading data
/// from server.
/// - Note: 3 states this protocol propose is `Loading`, `Error` and `Empty`
///
/// To use this protocol you must have to make the `UIView` or the `UIViewController` confirm the protocol `EMStatePresentable.
/// and also make sure that your have provided the loading view  by confirm `loadingStateView` from `UIView` or `UIViewController`.
///
/// Example for UIViewController:
///
///     class OrderListingViewController: UIViewController, StatePresentable {
///         var loadingStateView: UIView {
///             return ProgressiveLoadingView()
///         }
///      }
///
/// Example for UIView:
///
///     class OrderListingViewController: UIView, StatePresentable {
///         var loadingStateView: UIView {
///             return ProgressiveLoadingView()
///         }
///     }
public protocol DSStatePresentable: AnyObject {

    /// [This property is] the container which will keep all of the state views
    var stateContainerView: UIView { get }

    /// [This property is] the  view will show when start fetching data
    var loadingStateView: UIView { get }

    /// [This property is]  the view will show when fetching data run into errors
    var errorStateView: UIView? { get }

    /// [This property is] the view will show when fetching empty data
    var emptyStateView: UIView? { get }

    /// The action for trigger loading
    /// - Parameters:
    ///    - animated: the flag to enable or disable the animation when hidden a view
    ///    - completionHandler: Void call back that will be trigger when start loading finished
    func startLoading(animated: Bool, completionHandler: VoidCallBack?)

    /// The action for trigger after got the data after fetching data.
    ///
    /// Use this method to hidden all of the states view inside a view or a controller.
    /// By default we supported for both of `UIView` and `UIViewController`
    ///
    /// - Parameters:
    ///    - hasContent:  if this parameter is `true` then hidden the states view
    ///    - hasError: if this parameters is `true` and `hasContent` is `false` then will show the error state.
    ///    if this parameters is `false` and `hasContent` is `false` the will show empty state
    ///    - completionHandler: Void call back that will be trigger when end loading finished
    ///
    func endLoading(hasError: Bool, hasContent: Bool, completionHandler: VoidCallBack?)
}
