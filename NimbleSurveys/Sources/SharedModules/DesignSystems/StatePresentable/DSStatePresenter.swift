//
//  DSStatePresenter.swift
//  EarnMore
//
//  Created by Tuan Pham on 17/04/2023.
//

import UIKit

public typealias VoidCallBack = () -> Void

public final class DSStatePresenter {

    public let stateContainerView: UIView
    public let loadingStateView: UIView
    public let emptyStateView: UIView?
    public let errorStateView: UIView?

    public init(
        stateContainerView: UIView,
        loadingStateView: UIView,
        emptyStateView: UIView?  = nil,
        errorStateView: UIView? = nil
    ) {
        self.stateContainerView = stateContainerView
        self.loadingStateView = loadingStateView
        self.emptyStateView = emptyStateView
        self.errorStateView = errorStateView
    }
}

// MARK: - EMStatePresentable

extension DSStatePresenter: DSStatePresentable {

    public func startLoading(animated: Bool, completionHandler: VoidCallBack?) {
        addLoadingViewIfNeeded()
        removeEmptyStateViewIfNeeded()
        removeErrorStateViewIfNeeded()

        loadingStateView.setHidden(false, animated: animated, completion: completionHandler)
    }

    public func endLoading(hasError: Bool, hasContent: Bool, completionHandler: VoidCallBack?) {

        if hasContent {

            handleEndLoadingWithContent(completionHandler: completionHandler)
        } else if hasError {

            handleEndLoadingWithError(completionHandler: completionHandler)
        } else {

            handleEndLoadingWithEmptyData(completionHandler: completionHandler)
        }
    }
}

// MARK: - Privates

extension DSStatePresenter {

    private func handleEndLoadingWithContent(completionHandler: VoidCallBack?) {
        hideLoadingView(completionHandler: completionHandler)
    }

    private func handleEndLoadingWithError(completionHandler: VoidCallBack?) {

        guard let errorStateView = errorStateView else {
            hideLoadingView(completionHandler: completionHandler)
            return
        }

        addErrorStateViewIfNeeded()
        errorStateView.setHidden(
            false,
            animated: true,
            completion: { [weak self] in
                self?.hideLoadingView(completionHandler: completionHandler)
            }
        )
    }

    private func handleEndLoadingWithEmptyData(completionHandler: VoidCallBack?) {

        guard let emptyStateView = emptyStateView else {
            hideLoadingView(completionHandler: completionHandler)
            return
        }

        addEmptyStateViewIfNeeded()
        emptyStateView.setHidden(
            false,
            animated: true,
            completion: { [weak self] in
                self?.hideLoadingView(completionHandler: completionHandler)
            }
        )
    }

    private func addLoadingViewIfNeeded() {

        guard loadingStateView.superview == nil else { return }
        loadingStateView.isHidden = true
        loadingStateView.layer.zPosition = UIConfig.loadingStateViewZPosition
        stateContainerView.addSubview(loadingStateView)
        NSLayoutConstraint.activate(
            loadingStateView.edgesAnchor.constraint(equalTo: stateContainerView.edgesAnchor)
        )
    }

    private func addEmptyStateViewIfNeeded() {
        guard let emptyStateView = emptyStateView, emptyStateView.superview == nil else { return }
        emptyStateView.isHidden = true
        emptyStateView.layer.zPosition = UIConfig.emptyStateViewZPosition
        stateContainerView.addSubview(emptyStateView)
        NSLayoutConstraint.activate(
            emptyStateView.edgesAnchor.constraint(equalTo: stateContainerView.edgesAnchor)
        )
    }

    private func addErrorStateViewIfNeeded() {
        guard let errorStateView = errorStateView, errorStateView.superview == nil else { return }
        errorStateView.layer.zPosition = UIConfig.errorStateViewZPosition
        errorStateView.isHidden = true
        stateContainerView.addSubview(errorStateView)
        NSLayoutConstraint.activate(
            errorStateView.edgesAnchor.constraint(equalTo: stateContainerView.edgesAnchor)
        )
    }

    private func removeLoadingViewIfNeeded() {
        guard loadingStateView.superview != nil else { return }
        loadingStateView.isHidden = true
    }

    private func removeEmptyStateViewIfNeeded() {
        guard emptyStateView?.superview != nil else { return }
        emptyStateView?.isHidden = true
    }

    private func removeErrorStateViewIfNeeded() {
        guard errorStateView?.superview != nil else { return }
        errorStateView?.isHidden = true
    }

    private func hideLoadingView(completionHandler: VoidCallBack?) {
        loadingStateView.setHidden(
            true,
            animated: true,
            completion: { [weak self] in
                self?.loadingStateView.removeFromSuperview()
                completionHandler?()
            }
        )
    }
}

// MARK: - EMStatePresenter.UIConfig

extension DSStatePresenter {

    enum UIConfig {

        static let loadingStateViewZPosition: CGFloat = 100
        static let emptyStateViewZPosition: CGFloat = loadingStateViewZPosition + 1
        static let errorStateViewZPosition: CGFloat = emptyStateViewZPosition + 1

    }
}
