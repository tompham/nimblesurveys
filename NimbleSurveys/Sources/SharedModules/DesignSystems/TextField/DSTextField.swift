//
//  DSTextField.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import UIKit

class DSTextField: UITextField {
    
    convenience init(placeholder: String) {
        self.init()
        
        configureTextField(placeholder: placeholder)
    }
    
    private func configureTextField(placeholder: String) {
        backgroundColor = .secondaryBackground.withAlphaComponent(0.18)
        textColor = .white100
        layer.cornerRadius = .spacing12
        font = .body
        
        attributedPlaceholder = NSAttributedString(
            string: placeholder,
            attributes: [
                .foregroundColor: UIColor.white.withAlphaComponent(0.3),
                .font: UIFont.body
            ]
        )
        
        leftView = UIView(frame: .init(x: .zero, y: .zero, width: .spacing12, height: frame.height))
        leftViewMode = .always
        let constraint = heightAnchor.constraint(equalToConstant: 56)
        NSLayoutConstraint.activate([constraint])
    }
}
