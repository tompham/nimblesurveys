//
//  ColorTokens.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import UIKit

extension UIColor {
    static let white100 = UIColor.white
    static let gray60 = UIColor(hex: "#727272")
    static let primaryBackground = UIColor(red: 21 / 255, green: 21 / 255, blue: 26 / 255, alpha: 1)
    static let secondaryBackground = UIColor.white
    
}
