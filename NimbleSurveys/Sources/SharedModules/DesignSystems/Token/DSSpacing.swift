//
//  DSSpacing.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation

extension CGFloat {

    /// 🍀 0 px 
    static let spacing0: CGFloat = 0

    /// 🍀 1 px
    static let spacing1: CGFloat = 1

    /// 🍀 2 px
    static let spacing2: CGFloat = 2

    /// 🍀 4 px
    static let spacing4: CGFloat = 4

    /// 🍀 8 px
    static let spacing8: CGFloat = 8

    /// 🍀 12 px
    static let spacing12: CGFloat = 12

    /// 🍀 16 px
    static let spacing16: CGFloat = 16

    /// 🍀 20 px
    static let spacing20: CGFloat = 20

    /// 🍀 24 px
    static let spacing24: CGFloat = 24

    /// 🍀 28 px
    static let spacing28: CGFloat = 28
    
    /// 🍀 32 px
    static let spacing32: CGFloat = 32
}
