//
//  FontTokens.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 06/01/2024.
//

import UIKit

extension UIFont {
    static let largeTitle = UIFont.systemFont(ofSize: 34, weight: .bold)
    static let display2 = UIFont.systemFont(ofSize: 28, weight: .bold)
    static let mediumBold = UIFont.systemFont(ofSize: 20, weight: .bold)
    static let title = UIFont.systemFont(ofSize: 17, weight: .semibold)
    static let paragrapRegular = UIFont.systemFont(ofSize: 17, weight: .regular)
    static let xSmall = UIFont.systemFont(ofSize: 13, weight: .semibold)
    static let xSmallRegular = UIFont.systemFont(ofSize: 13, weight: .regular)
    static let body = UIFont.systemFont(ofSize: 17, weight: .regular)
    static let small = UIFont.systemFont(ofSize: 15, weight: .regular)
    static let smallSemibold = UIFont.systemFont(ofSize: 15, weight: .semibold)
}
