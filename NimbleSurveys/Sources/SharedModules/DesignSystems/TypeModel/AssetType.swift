//
//  AssetType.swift
//  NimbleSurveys
//
//  Created by Tuan Pham on 08/01/2024.
//

import UIKit

enum AssetType {
    case local(image: UIImage)
    case remote(imageURLString: String)
}
