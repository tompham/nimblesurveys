//
//  AuthenticationRepositoryTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 07/01/2024.
//

import XCTest
import RxSwift
import Cuckoo
@testable import NimbleSurveys

final class AuthenticationRepositoryTests: XCTestCase {
    
    var sut: AuthenticationRepository!
    var service: MockAuthenticationServiceProtocol!
    
    override func setUpWithError() throws {
        service = MockAuthenticationServiceProtocol()
        sut = AuthenticationRepository(service: service)
    }
    
    override func tearDownWithError() throws {
        Cuckoo.reset(service)
        service = nil
        sut = nil
    }
    
    func testAuthenticateSuccess() {
        // Given:
        let expected = expectation(description: #function)
        let email = "phamanhtuancross@gmail.com"
        let password = "123456"
        let credentialsResponse = makeCredentialsResponse(
            id: "mockID",
            type: "token"
        )
        stubAuthenticate(
            email: email,
            password: password,
            expectationResult: .just(credentialsResponse)
        )
        
        // When:
        let subscription = sut.authenticate(email: email, password: password)
            .subscribe(
                onNext: { entity in
                    expected.fulfill()
                }
            )
        
        // Then:
        verify(service).authenticate(email: email, password: password)
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testAuthenticateFailure() {
        // Given:
        let expected = expectation(description: #function)
        let email = "phamanhtuancross@gmail.com"
        let password = "123456"
        let error = NSError(message: "unexpectation error message")
        stubAuthenticate(
            email: email,
            password: password,
            expectationResult: .error(error)
        )
        
        // When:
        let subscription = sut.authenticate(email: email, password: password)
            .subscribe(
                onNext: { _ in
                    XCTFail("It should be failure")
                },
                onError: { actuaError in
                    XCTAssertEqual(actuaError.localizedDescription, error.localizedDescription)
                    expected.fulfill()
                }
            )
        
        // Then:
        verify(service).authenticate(email: email, password: password)
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testFetchUserProfileSuccess() {
        // Given:
        let expected = expectation(description: #function)
        let userProfile = UserProfileResponse(
            type: "mock_type",
            id: "mock_id",
            email: "mock_email",
            name: "fake_name",
            avatarUrl: "https://icon.png"
        )
        stubFetchUserProfile(expectationResult: .just(userProfile))
        
        // When:
        let subscription = sut.fetchUserProfile()
            .subscribe(onNext: { entity in
                XCTAssertEqual(entity.id, "mock_id")
                XCTAssertEqual(entity.type, "mock_type")
                XCTAssertEqual(entity.email, "mock_email")
                XCTAssertEqual(entity.name, "fake_name")
                XCTAssertEqual(entity.avatarUrl, "https://icon.png")
                expected.fulfill()
            })
        
        // Then:
        verify(service).fetchUserProfile()
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
        
    }
    
    func testFetchUserProfileFailure() {
        // Given:
        let expected = expectation(description: #function)
        let error = NSError(message: "error message")
        stubFetchUserProfile(expectationResult: .error(error))
        
        // When:
        let subscription = sut.fetchUserProfile()
            .subscribe(
                onNext: { _ in
                    XCTFail("It should be failure")
                },
                onError: { actuaError in
                    XCTAssertEqual(actuaError.localizedDescription, error.localizedDescription)
                    expected.fulfill()
                }
            )
        
        // Then:
        verify(service).fetchUserProfile()
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
        
    }
}

// MARK: - Privates

extension AuthenticationRepositoryTests {
    
    private func stubAuthenticate(email: String, password: String, expectationResult: Observable<CredentialsResponse>) {
        stub(service) { stub in
            when(
                stub.authenticate(email: email, password: password)
            )
            .thenReturn(expectationResult)
        }
    }
    
    private func stubFetchUserProfile(expectationResult: Observable<UserProfileResponse>) {
        Cuckoo.stub(service) {
            when($0.fetchUserProfile()).thenReturn(expectationResult)
        }
    }
}
