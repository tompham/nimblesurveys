//
//  SurveyRepositoryTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 07/01/2024.
//

import XCTest
import RxSwift
import Cuckoo
@testable import NimbleSurveys

final class SurveyRepositoryTests: XCTestCase {
    
    var sut: SurveyRepository!
    var service: MockSurveyServiceProtocol!

    override func setUpWithError() throws {
        service = MockSurveyServiceProtocol()
        sut = SurveyRepository(service: service)
    }

    override func tearDownWithError() throws {
        Cuckoo.reset(service)
        service = nil
        sut = nil
    }
    
    func testFetchSurveyListSuccess() {
        // Given:
        let expected = expectation(description: #function)
        let pageNumber = 1
        let pageSize = 10
        let surveyListResponse = SurveyListResponse(
            data: [
                makeSurveyResponse(id: "1", type: "survey")
            ],
            meta: nil
        )
        stubFetchSurveyList(
            pageNumber: pageNumber,
            pageSize: pageSize,
            expectationResult: .just(surveyListResponse)
        )
        
        // When:
        let subscription = sut.fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
            .subscribe(
                onNext: { entity in
                    XCTAssertEqual(entity.data?.count, 1)
                    expected.fulfill()
                },
                onError: { _  in
                    XCTFail("It should be success")
                }
            )
        
        // When:
        verify(service).fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testFetchSurveyListFailure() {
        // Given:
        let expected = expectation(description: #function)
        let pageNumber = 1
        let pageSize = 10
        let error = NSError(message: "error message")
        
        stubFetchSurveyList(
            pageNumber: pageNumber,
            pageSize: pageSize,
            expectationResult: .error(error)
        )
        
        // When:
        let subscription = sut.fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
            .subscribe(
                onNext: { _ in
                    XCTFail("It should be failure")
                },
                onError: { actuaError in
                    XCTAssertEqual(actuaError.localizedDescription, error.localizedDescription)
                    expected.fulfill()
                }
            )
        
        // When:
        verify(service).fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
}

extension SurveyRepositoryTests {
    
    func stubFetchSurveyList(
        pageNumber: Int,
        pageSize: Int,
        expectationResult: Observable<SurveyListResponse>
    ) {
        stub(service) { stub in
            when(stub.fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize))
                .thenReturn(expectationResult)
        }
    }
}
