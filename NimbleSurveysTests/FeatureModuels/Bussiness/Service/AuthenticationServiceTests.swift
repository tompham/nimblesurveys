//
//  AuthenticationServiceTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 06/01/2024.
//

import XCTest
import Cuckoo
import RxSwift
@testable import NimbleSurveys

final class AuthenticationServiceTests: XCTestCase {
    
    var sut: AuthenticationService!
    
    var apiClient: MockAPIClientProtocol!
    var credentialsProvider: MockCredentialsProviderProtocol!
    
    override func setUpWithError() throws {
        
        apiClient = MockAPIClientProtocol()
        credentialsProvider = MockCredentialsProviderProtocol()
        
        sut = AuthenticationService(
            apiClient: apiClient,
            credentialsProvider: credentialsProvider
        )
    }
    
    override func tearDownWithError() throws {
        
        Cuckoo.reset(apiClient, credentialsProvider)
        
        apiClient = nil
        credentialsProvider = nil
        
        sut = nil
    }
    
    func testAuthenticateSuccess() {
        // Given:
        let expected = expectation(description: #function)
        let email = "test@gmail.com"
        let password = "123456"
        let target = LoginTarget(email: email, password: password)
        stubRequest(
            apiClient: apiClient,
            target: sameInstanceAs(target),
            fromJSONFile: "oauth_token_response"
        )
        stubStoreCredentials(expectationResult: true)
        
        // when:
        let subscription = sut.authenticate(email: email, password: password)
            .subscribe(
                onNext: { response in
                    self.assert(response: response)
                    expected.fulfill()
                }
            )
        
        verify(credentialsProvider).store(credentials: any(Data.self))
        verify(apiClient).request(target: sameInstanceAs(target))
        
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testAuthenticateFailure() {
        // Given:
        let expected = expectation(description: #function)
        let email = "test@gmail.com"
        let password = "123456"
        let error = NSError(message: "un expectation error message")
        let target = LoginTarget(email: email, password: password)
        stubRequest(
            apiClient: apiClient,
            target: sameInstanceAs(target),
            expectationResult: .error(error)
        )
        
        // when:
        let subscription = sut.authenticate(email: email, password: password)
            .subscribe(
                onNext: { response in
                    XCTFail("It should not be called")
                },
                onError: { actualError in
                    XCTAssertEqual(actualError.localizedDescription, error.localizedDescription)
                    expected.fulfill()
                }
            )
        
        verify(apiClient).request(target: sameInstanceAs(target))
        
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testFetchUserProfileSuccess() {
        // Given:
        let expected = expectation(description: #function)
        let target = UserProfileTarget()
        stubRequest(
            apiClient: apiClient,
            target: sameInstanceAs(target),
            fromJSONFile: "user_profile_response"
        )
        
        // When:
        let subscription = sut.fetchUserProfile()
            .subscribe(
                onNext: { response in
                    self.assert(response: response)
                    expected.fulfill()
                },
                onError: { _  in
                    XCTFail("It should be success")
                }
            )
        
        // When:
        verify(apiClient).request(target: sameInstanceAs(target))
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testFetchUserProfileFailure() {
        // Given:
        let expected = expectation(description: #function)
        let error = NSError(message: "unexpectation error message")
        let target = UserProfileTarget()
        stubRequest(
            apiClient: apiClient,
            target: sameInstanceAs(target),
            expectationResult: .error(error)
        )
        
        // When:
        let subscription = sut.fetchUserProfile()
            .subscribe(
                onNext: { response in
                    XCTFail("It should be Failure")
                },
                onError: { actualError  in
                    XCTAssertEqual(actualError.localizedDescription, error.localizedDescription)
                    expected.fulfill()
                }
            )
        
        // When:
        verify(apiClient).request(target: sameInstanceAs(target))
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
}

// MARK: - Privates

extension AuthenticationServiceTests {
    

    private func assert(response: CredentialsResponse) {
        XCTAssertEqual(response.id, "31054")
        XCTAssertEqual(response.type, "token")
        XCTAssertEqual(response.accessToken, "7vkV9rZizoZ8MR5zNzwsFvfr23IaAYzMo3zdS5y_tj4")
        XCTAssertEqual(response.tokenType, "Bearer")
        XCTAssertEqual(response.expiresIn, 7200)
        XCTAssertEqual(response.refreshToken, "uABczFfqIlv6HcVGDVrxH2qE9fyXjV_SJn4lEMcluVc")
        XCTAssertEqual(response.createdAt, 1704514268)
    }
    
    private func assert(response: UserProfileResponse) {
        XCTAssertEqual(response.id, "1")
        XCTAssertEqual(response.type, "user")
        XCTAssertEqual(response.email, "your_email@example.com")
        XCTAssertEqual(response.name, "Your Name")
        XCTAssertEqual(response.avatarUrl, "https://secure.gravatar.com/avatar/6733d09432e89459dba795de8312ac2d")
    }
    
    private func stubStoreCredentials(
        expectationResult: Bool = true
    ) {
        Cuckoo.stub(credentialsProvider) {
            when(
                $0.store(credentials: any(Data.self))
            )
            .thenReturn(expectationResult)
        }
    }
}
