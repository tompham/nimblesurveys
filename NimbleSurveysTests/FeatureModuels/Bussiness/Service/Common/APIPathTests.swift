//
//  APIPathTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 06/01/2024.
//

import XCTest
@testable import NimbleSurveys

final class APIPathTests: XCTestCase {
    
    func testV1LoginAPIPath() {
        // When:
        let result = APIPath.V1.login
        
        // Then:
        XCTAssertEqual(result, "/api/v1/oauth/token")
    }
}
