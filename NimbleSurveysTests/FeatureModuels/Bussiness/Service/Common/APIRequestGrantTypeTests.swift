//
//  APIRequestGrantTypeTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 06/01/2024.
//

import XCTest
@testable import NimbleSurveys

final class APIRequestGrantTypeTests: XCTestCase {

    func testPasswordGrantType() {
        // When:
        let result = APIRequestGrantType.password
        
        // Then:
        XCTAssertEqual(result, "password")
    }
}
