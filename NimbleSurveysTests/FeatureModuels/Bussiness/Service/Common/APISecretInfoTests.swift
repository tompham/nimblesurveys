//
//  APISecretInfoTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 06/01/2024.
//

import XCTest
@testable import NimbleSurveys

final class APISecretInfoTests: XCTestCase {

    func testClientID() {
        // When:
        let result = APISecretInfo.clientID
        
        // Then:
        XCTAssertEqual(result, "ofzl-2h5ympKa0WqqTzqlVJUiRsxmXQmt5tkgrlWnOE")
    }
    
    func testClientSecret() {
        // When:
        let result = APISecretInfo.clientSecret
        
        // Then:
        XCTAssertEqual(result, "lMQb900L-mTeU-FVTCwyhjsfBwRCxwwbCitPob96cuU")
    }
}
