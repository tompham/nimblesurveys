//
//  SurveyServiceTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 07/01/2024.
//

import XCTest
import Cuckoo
import RxSwift
@testable import NimbleSurveys

final class SurveyServiceTests: XCTestCase {

    var sut: SurveyService!
    
    var apiClient: MockAPIClientProtocol!
    
    override func setUpWithError() throws {
        apiClient = MockAPIClientProtocol()
        
        sut = SurveyService(apiClient: apiClient)
    }

    override func tearDownWithError() throws {
        Cuckoo.reset(apiClient)
        
        apiClient = nil
        
        sut = nil
    }

    func testFetchSurveyListSuccess() {
        // Given:
        let expected = expectation(description: #function)
        let pageNumber = 1
        let pageSize = 10
        let target = FetchSurveyListTarget(pageNumber: pageNumber, pageSize: pageSize)
        stubRequest(
            apiClient: apiClient,
            target: sameInstanceAs(target),
            fromJSONFile: "survey_list_response"
        )
        
        // When:
        let subscription = sut.fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
            .subscribe(
                onNext: { response in
                    self.assert(response: response)
                    expected.fulfill()
                },
                onError: { _  in
                    XCTFail("It should be success")
                }
            )
        
        // When:
        verify(apiClient).request(target: sameInstanceAs(target))
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testFetchSurveyListFailure() {
        // Given:
        let expected = expectation(description: #function)
        let pageNumber = 1
        let pageSize = 10
        let error = NSError(message: "error message")
        let target = FetchSurveyListTarget(pageNumber: pageNumber, pageSize: pageSize)
        stubRequest(
            apiClient: apiClient,
            target: sameInstanceAs(target),
            expectationResult: .error(error)
        )
        
        // When:
        let subscription = sut.fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
            .subscribe(
                onNext: { response in
                    XCTFail("It should not be called")
                },
                onError: { actualError in
                    XCTAssertEqual(actualError.localizedDescription, error.localizedDescription)
                    expected.fulfill()
                }
            )
        
        // When:
        verify(apiClient).request(target: sameInstanceAs(target))
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
}

// MARK: - Privates
extension SurveyServiceTests {
    
    private func assert(response: SurveyListResponse) {
        XCTAssertEqual(response.data?[0].id, "d5de6a8f8f5f1cfe51bc")
        XCTAssertEqual(response.data?[0].type, "survey")
        XCTAssertEqual(response.data?[0].title, "Scarlett Bangkok")
        XCTAssertEqual(response.data?[0].description, "We'd love ot hear from you!")
        XCTAssertEqual(response.data?[0].coverImageUrl, "https://dhdbhh0jsld0o.cloudfront.net/m/1ea51560991bcb7d00d0_")
        XCTAssertEqual(response.data?[0].surveyType, "Restaurant")
    }
}
