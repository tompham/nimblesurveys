//
//  ForgotPasswordRouterTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 10/01/2024.
//

import XCTest
import Cuckoo
@testable import NimbleSurveys

final class ForgotPasswordRouterTests: XCTestCase {
    
    var sut: ForgotPasswordRouter!
    var messagePresenter: MockDSMessagePresenterProtocol!

    override func setUpWithError() throws {
        messagePresenter = MockDSMessagePresenterProtocol()
        sut = ForgotPasswordRouter(messagePresenter: messagePresenter)
    }

    override func tearDownWithError() throws {
        Cuckoo.reset(messagePresenter)
        
        messagePresenter = nil
        sut = nil
    }
    
    func testShowErrorMessage() {
        // Given:
        let message = "error message"
        stubShowErrorMessage(message: message)
        
        // When:
        sut.showErrorMessage(message)
        
        // Then:
        verify(messagePresenter).showErrorMessage(message)
    }
    
    func testShowConfirmForgotPasswordMessage() {
        // Given:
        let message = "sample message"
        stubShowNotificationView(message: message)
        
        // When:
        sut.showConfirmForgotPasswordMessage(message)
        
        // Then:
        verify(messagePresenter).showNotificationView(title: L10n.App.ForgotPassword.Notification.CheckEmail.title, message: message)
    }
}

extension ForgotPasswordRouterTests {
    
    private func stubShowNotificationView(message: String) {
        Cuckoo.stub(messagePresenter) {
            when($0.showNotificationView(title: L10n.App.ForgotPassword.Notification.CheckEmail.title, message: message))
                .thenDoNothing()
        }
    }
    
    private func stubShowErrorMessage(message: String) {
        Cuckoo.stub(messagePresenter) {
            when($0.showErrorMessage(message)).thenDoNothing()
        }
    }
}
