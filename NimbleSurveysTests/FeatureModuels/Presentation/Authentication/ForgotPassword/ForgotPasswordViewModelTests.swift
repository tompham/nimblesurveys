//
//  ForgotPasswordViewModelTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 10/01/2024.
//

import XCTest
import Cuckoo
import RxSwift
import RxCocoa
@testable import NimbleSurveys

final class ForgotPasswordViewModelTests: XCTestCase {
    
    var sut: ForgotPasswordViewModel!
    var authenticationRepository: MockAuthenticationRepositoryProtocol!
    var dataValidator: MockDataValidatorProtocol!
    
    override func setUpWithError() throws {
        authenticationRepository = MockAuthenticationRepositoryProtocol()
        dataValidator = MockDataValidatorProtocol()
        
        sut = ForgotPasswordViewModel(
            authenticationRepository: authenticationRepository,
            dataValidator: dataValidator
        )
    }
    
    
    override func tearDownWithError() throws {
        Cuckoo.reset(authenticationRepository, dataValidator)
        authenticationRepository = nil
        dataValidator = nil
        sut = nil
    }
    
    func testIsForgotButtonEnabledWhenEmailIsEmpty() {
        // When:
        let expected = expectation(description: #function)
        let subscription = sut.isForgotButtonEnabled
            .drive(onNext: { isEnabled in
                XCTAssertFalse(isEnabled)
                expected.fulfill()
            })
        
        // Then:
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testIsForgotButtonEnabledWhenEmailIsNotEmpty() {
        // When:
        let expected = expectation(description: #function)
        let subscription = sut.isForgotButtonEnabled
            .skip(1)
            .drive(onNext: { isEnabled in
                XCTAssertTrue(isEnabled)
                expected.fulfill()
            })
        sut.email.accept("phamanhtuancross@gmail.com")
        
        // Then:
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testDescription() {
        // When:
        let expected = expectation(description: #function)
        let subscription = sut.description
            .drive(onNext: { description in
                XCTAssertEqual(description, L10n.App.ForgotPassword.description)
                expected.fulfill()
            })
        
        // Then:
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testShowCheckEmailMessageWhenPerformForgotPasswordSuccess() {
        // Given:
        let expected = expectation(description: #function)
        let email = "phamanhtuancross@gmail.com"
        let message = "Please check your email!"
        let metadata = MetaDataEntity(message: message)
        
        stubValidateEmailSuccess(email: email)
        stubForgotPassword(email: email, expectationResult: .just(metadata))
        
        // When:
        let subscription = sut.showCheckEmailMessage
            .drive(onNext: { actualMessage in
                XCTAssertEqual(actualMessage, message)
                expected.fulfill()
            })
        sut.email.accept(email)
        sut.forgotPasswordTrigger.accept(())
        
        // Then:
        verify(dataValidator).validateEmail(email)
        verify(authenticationRepository).forgotPassword(for: email)
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testShowCheckEmailMessageWhenPerformForgotPasswordFailure() {
        // Given:
        let email = "phamanhtuancross@gmail.com"
        let error = NSError(message: "error message")
        
        stubValidateEmailSuccess(email: email)
        stubForgotPassword(email: email, expectationResult: .error(error))
        
        // When:
        let subscription = sut.showCheckEmailMessage
            .drive(onNext: { actualMessage in
                XCTFail("It should be never called")
            })
        sut.email.accept(email)
        sut.forgotPasswordTrigger.accept(())
        
        // Then:
        verify(dataValidator).validateEmail(email)
        verify(authenticationRepository).forgotPassword(for: email)
        subscription.dispose()
    }
    
    func testShowErrorMessageWhenPerformForgotPasswordSuccess() {
        // Given:
        let email = "phamanhtuancross@gmail.com"
        let message = "Please check your email!"
        let metadata = MetaDataEntity(message: message)
        
        stubValidateEmailSuccess(email: email)
        stubForgotPassword(email: email, expectationResult: .just(metadata))
        
        // When:
        let subscription = sut.showErrorMessage
            .drive(onNext: { _ in
                XCTFail("It should be never called")
            })
        sut.email.accept(email)
        sut.forgotPasswordTrigger.accept(())
        
        // Then:
        verify(dataValidator).validateEmail(email)
        verify(authenticationRepository).forgotPassword(for: email)
        subscription.dispose()
    }
    
    func testShowErrorMessageWhenPerformForgotPasswordFailure() {
        // Given:
        let expected = expectation(description: #function)
        let email = "phamanhtuancross@gmail.com"
        let errorMessage = "error message"
        let error = AppError.apiError(error: APIError(message: errorMessage))
        
        stubValidateEmailSuccess(email: email)
        stubForgotPassword(email: email, expectationResult: .error(error))
        
        // When:
        let subscription = sut.showErrorMessage
            .drive(onNext: { actualMessage in
                XCTAssertEqual(actualMessage, errorMessage)
                expected.fulfill()
            })
        sut.email.accept(email)
        sut.forgotPasswordTrigger.accept(())
        
        // Then:
        verify(dataValidator).validateEmail(email)
        verify(authenticationRepository).forgotPassword(for: email)
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testShowErrorMessageWhenEmailIsInValid() {
        // Given:
        let expected = expectation(description: #function)
        let email = "phamanhtuancross@"
        let error = EmailValidationError.invalid
        stubValidateEmailWithThrowError(email: email, error: error)
        
        // When:
        let subscription = sut.showErrorMessage
            .drive(onNext: { actualMessage in
                XCTAssertEqual(actualMessage, error.errorMessage)
                expected.fulfill()
            })
        sut.email.accept(email)
        sut.forgotPasswordTrigger.accept(())
        
        // Then:
        verify(dataValidator).validateEmail(email)
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
}

extension ForgotPasswordViewModelTests {
    
    private func stubForgotPassword(email: String, expectationResult: Observable<MetaDataEntity>) {
        Cuckoo.stub(authenticationRepository) {
            when(
                $0.forgotPassword(for: email)
            )
            .thenReturn(expectationResult)
        }
    }
    
    private func stubValidateEmailWithThrowError(
        email: String,
        error: EmailValidationError
    ) {
        Cuckoo.stub(dataValidator) {
            when($0.validateEmail(email)).thenThrow(error)
        }
    }
    
    private func stubValidateEmailSuccess(
        email: String
    ) {
        Cuckoo.stub(dataValidator) {
            when($0.validateEmail(email)).thenReturn(true)
        }
    }
}
