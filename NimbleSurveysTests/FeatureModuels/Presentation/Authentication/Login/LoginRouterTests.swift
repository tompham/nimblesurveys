//
//  LoginRouterTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 09/01/2024.
//

import XCTest
import Cuckoo
@testable import NimbleSurveys

final class LoginRouterTests: XCTestCase {

    var sut: LoginRouter!
    var navigator: MockAppNavigatorProtocol!
    
    override func setUpWithError() throws {
        navigator = MockAppNavigatorProtocol()
        sut = LoginRouter(navigator: navigator)
    }

    override func tearDownWithError() throws {
        Cuckoo.reset(navigator)
        
        navigator = nil
        sut = nil
    }
    
    func testShowHomePage() {
        // Given:
        let controller = UIViewController()
        stubShow(navigator: navigator, route: .home, from: controller, action: .root)
        
        // When:
        sut.showHomePage(from: controller)
        
        // Then:
        verify(navigator).show(route: Route.home, from: sameInstance(as: controller), action: NavigateAction.root)
    }
}
