//
//  LoginViewModelTest.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 07/01/2024.
//

import XCTest
import RxSwift
import Cuckoo
@testable import NimbleSurveys

final class LoginViewModelTest: XCTestCase {
    
    var sut: LoginViewModel!
    var repository: MockAuthenticationRepositoryProtocol!
    var dataValidator: MockDataValidatorProtocol!
    
    override func setUpWithError() throws {
        repository = MockAuthenticationRepositoryProtocol()
        dataValidator = MockDataValidatorProtocol()
        
        sut = LoginViewModel(repository: repository, dataValidator: dataValidator)
    }
    
    override func tearDownWithError() throws {
        Cuckoo.reset(repository, dataValidator)
        
        repository = nil
        dataValidator = nil
        
        sut = nil
    }
    
    func testIsLoginEnabled() {
        // Given:
        let expected = expectation(description: #function)
        let email = "phamanhtuancross@gmail.com"
        let password = "11111111"
        
        
        // When:
        let subscription = sut.isLoginEnabled
            .skip(2)
            .drive(onNext: { isEnabled in
                XCTAssertTrue(isEnabled)
                expected.fulfill()
            })
        sut.email.accept(email)
        sut.password.accept(password)
        
        // Then:
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testIsLoginEnabledWhenEmailIsEmpty() {
        // Given:
        let expected = expectation(description: #function)
        let password = "11111111"
        
        
        // When:
        let subscription = sut.isLoginEnabled
            .skip(1)
            .drive(onNext: { isEnabled in
                XCTAssertFalse(isEnabled)
                expected.fulfill()
            })
        sut.password.accept(password)
        
        // Then:
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testIsLoginEnabledWhenPasswordIsEmpty() {
        // Given:
        let expected = expectation(description: #function)
        let email = "phamanhtuancross@gmail.com"
        
        // When:
        let subscription = sut.isLoginEnabled
            .skip(1)
            .drive(onNext: { isEnabled in
                XCTAssertFalse(isEnabled)
                expected.fulfill()
            })
        sut.email.accept(email)
        
        // Then:
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testShowErrorMessageWhenEmailIsInValid() {
        let expected = expectation(description: #function)
        let email = "phamanhtuancross@"
        let password = "11111111"
        let error = EmailValidationError.invalid
        
        stubValidateEmailWithThrowError(email: email, error: error)
        // When:
        let subscription = sut.showErrorMessage
            .drive(onNext: { actualMessage in
                XCTAssertEqual(actualMessage, error.errorMessage)
                expected.fulfill()
            })
        
        sut.email.accept(email)
        sut.password.accept(password)
        sut.loginTrigger.accept(())
        
        // Then:
        verify(dataValidator).validateEmail(email)
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testShowHomePageWhenAuthenticationSuccess() {
        // Given:
        let expected = expectation(description: #function)
        let email = "phamanhtuancross@gmail.com"
        let password = "11111111"
        
        stubValidateEmailSuccess(email: email)
        stubAuthenticate(email: email, password: password, expectationResult: .just(()))
        
        // When:
        let subscription = sut.showHomePage
            .drive(onNext: { _ in
                expected.fulfill()
            })
        sut.email.accept(email)
        sut.password.accept(password)
        sut.loginTrigger.accept(())
        
        // Then:
        verify(dataValidator).validateEmail(email)
        verify(repository).authenticate(email: email, password: password)
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }

    func testShowHomePageWhenAuthenticationFailureWithUnAuthorizedError() {
        // Given:
        let email = "phamanhtuancross@gmail.com"
        let password = "11111111"
        
        stubValidateEmailSuccess(email: email)
        stubAuthenticate(email: email, password: password, expectationResult: .error(AppError.unAuthorized))

        // When:
        let subscription = sut.showHomePage
            .drive(onNext: { result in
                XCTFail("It should be failure")
            })
        sut.email.accept(email)
        sut.password.accept(password)
        sut.loginTrigger.accept(())
        
        // Then:
        verify(dataValidator).validateEmail(email)
        verify(repository).authenticate(email: email, password: password)
        subscription.dispose()
    }

    func testShowErrorMessageWhenAuthenticationFailureWithApiError() {
        // Given:
        let expected = expectation(description: #function)
        let email = "phamanhtuancross@gmail.com"
        let password = "11111111"
        let errorMessage = "Incorrect email or password"
        
        stubValidateEmailSuccess(email: email)
        stubAuthenticate(
            email: email,
            password: password,
            expectationResult: .error(AppError.apiError(error: APIError(message: errorMessage)))
        )

        // When:
        let subscription = sut.showErrorMessage
            .drive(onNext: { actualMessage in
                XCTAssertEqual(actualMessage, errorMessage)
                expected.fulfill()
            })
        sut.email.accept(email)
        sut.password.accept(password)
        sut.loginTrigger.accept(())
        
        // Then:
        verify(dataValidator).validateEmail(email)
        verify(repository).authenticate(email: email, password: password)
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
}

// MARK: - Privates

extension LoginViewModelTest {
    
    private func stubAuthenticate(
        email: String,
        password: String,
        expectationResult: Observable<Void>
    ) {
        Cuckoo.stub(repository) {
            when(
                $0.authenticate(
                    email: email,
                    password: password
                )
            )
            .thenReturn(expectationResult)
        }
    }
    
    private func stubValidateEmailWithThrowError(
        email: String,
        error: EmailValidationError
    ) {
        Cuckoo.stub(dataValidator) {
            when($0.validateEmail(email)).thenThrow(error)
        }
    }
    
    private func stubValidateEmailSuccess(
        email: String
    ) {
        Cuckoo.stub(dataValidator) {
            when($0.validateEmail(email)).thenReturn(true)
        }
    }
}
