//
//  SurveyDetailsViewModelTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 10/01/2024.
//

import XCTest
import Cuckoo
import RxSwift
import RxCocoa
@testable import NimbleSurveys

final class SurveyDetailsViewModelTests: XCTestCase {

    var sut: SurveyDetailsViewModel!

    override func tearDownWithError() throws {
        sut = nil
    }
    
    func testTitle() {
        // Given:
        let expected = expectation(description: #function)
        let title = "Sample title"
        let entity = makeSurveyDetailsEntity(title: title)
        sut = SurveyDetailsViewModel(entity: entity)
        
        // When:
        let subscription = sut.title
            .drive(onNext: { actutalTitle in
                XCTAssertEqual(actutalTitle, title)
                expected.fulfill()
            })
        
        // Then:
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testDescription() {
        // Given:
        let expected = expectation(description: #function)
        let description = "Sample description"
        let entity = makeSurveyDetailsEntity(description: description)
        sut = SurveyDetailsViewModel(entity: entity)
        
        // When:
        let subscription = sut.description
            .drive(onNext: { actutalDescription in
                XCTAssertEqual(actutalDescription, description)
                expected.fulfill()
            })
        
        // Then:
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testCoverImageUrl() {
        // Given:
        let expected = expectation(description: #function)
        let coverImageUrl = "http://image.png"
        let entity = makeSurveyDetailsEntity(coverImageUrl: coverImageUrl)
        sut = SurveyDetailsViewModel(entity: entity)
        
        // When:
        let subscription = sut.backgroundImageUrl
            .drive(onNext: { actualBackgroundImageUrl in
                XCTAssertEqual(actualBackgroundImageUrl, coverImageUrl)
                expected.fulfill()
            })
        
        // Then:
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
}

// MARK: - Privates

extension SurveyDetailsViewModelTests {
    
    private func makeSurveyDetailsEntity(
        title: String? = nil,
        description: String? = nil,
        coverImageUrl: String? = nil
    ) -> SurveyEntity {
        return SurveyEntity(
            title: title,
            description: description,
            isActive: true,
            coverImageUrl: coverImageUrl,
            surveyType: "restaurant"
        )
    }
}
