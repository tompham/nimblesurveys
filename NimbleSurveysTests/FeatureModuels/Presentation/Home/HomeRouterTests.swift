//
//  HomeRouterTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 08/01/2024.
//

import XCTest
import Cuckoo

@testable import NimbleSurveys

final class HomeRouterTests: XCTestCase {
    
    var sut: HomeRouter!
    var errorHandler: MockErrorHandlerProtocol!
    var navigator: MockAppNavigatorProtocol!
    
    override func setUpWithError() throws {
        errorHandler = MockErrorHandlerProtocol()
        navigator = MockAppNavigatorProtocol()
        
        sut = HomeRouter(
            errorHandler: errorHandler,
            navigator: navigator
        )
    }
    
    override func tearDownWithError() throws {
        Cuckoo.reset(errorHandler, navigator)
        
        errorHandler = nil
        navigator = nil
        
        sut = nil
    }
    
    func testHandleError() {
        // Given:
        let error = NSError(message: "unknown error")
        let controller = UIViewController()
        stubHandleError(on: controller)
        
        // When:
        sut.handleError(error, on: controller)
        
        // Then:
        verify(errorHandler).handleError(error: any(), on: sameInstance(as: controller))
    }
    
    func testShowLogin() {
        // Given:
        let controller = UIViewController()
        stubShow(navigator: navigator, route: .login, from: controller, action: .root)
        
        // When:
        sut.showLogin(from: controller)
        
        // Then:
        verify(navigator).show(
            route: Route.login,
            from: sameInstance(as: controller),
            action: NavigateAction.root
        )
    }
}

extension HomeRouterTests {
    
    private func stubHandleError(
        on controller: UIViewController
    ) {
        Cuckoo.stub(errorHandler) {
            when(
                $0.handleError(
                    error: any(),
                    on: sameInstance(as: controller)
                )
            )
            .thenDoNothing()
        }
    }
}

extension Route: Matchable {
    public var matcher: ParameterMatcher<Route> {
        return ParameterMatcher {
            $0 == self
        }
    }
}

extension NavigateAction: Matchable {
    
    public var matcher: ParameterMatcher<NavigateAction> {
        return ParameterMatcher {
            $0 == self
        }
    }
}

extension NSError: Matchable {
    
    public var matcher: ParameterMatcher<NSError> {
        return ParameterMatcher {
            $0 == self
        }
    }
}
