//
//  HomeViewModelTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 07/01/2024.
//

import XCTest
import RxSwift
import Cuckoo
@testable import NimbleSurveys

final class HomeViewModelTests: XCTestCase {

    var sut: HomeViewModel!
    var surveyRepository: MockSurveyRepositoryProtocol!
    var authRepository: MockAuthenticationRepositoryProtocol!
    var disposeBag: DisposeBag!
    
    override func setUpWithError() throws {
        
        disposeBag = DisposeBag()
        surveyRepository = MockSurveyRepositoryProtocol()
        authRepository = MockAuthenticationRepositoryProtocol()
        sut = makeSUT()
    }

    override func tearDownWithError() throws {
        Cuckoo.reset(surveyRepository, authRepository)
        
        surveyRepository = nil
        authRepository = nil
        disposeBag = nil
        sut = nil
    }
    
    func testSurveysWhenFetchSurveyListSuccess() {
        // Given:
        let expected = expectation(description: #function)
        let pageNumber = 1
        let pageSize = 10
        let surveyEntity = makeSurveyEntity()
        let surveyListEntity = SurveyListEntity(
            data: [
                surveyEntity
            ],
            meta: .init(
                page: 1,
                pages: 2,
                pageSize: 10,
                records: 20
            )
        )
        let userProfile = makeUserProfileEntity(type: "user", id: "1")
        
        stubFetchSurveyList(pageNumber: pageNumber, pageSize: pageSize, expectationResult: .just(surveyListEntity))
        stubFetchUserProfile(expectationResult: .just(userProfile))
        sut = makeSUT()
        
        // When:
        let subscription = sut.surveys
            .drive(onNext: { models in
                XCTAssertEqual(models.count, 1)
                XCTAssertEqual(models[0].entity, surveyEntity)
                expected.fulfill()
            })
        sut.input.viewDidLoadTrigger.accept(())
        
        // Then:
        verify(surveyRepository).fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
        verify(authRepository).fetchUserProfile()
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testSurveysWhenFetchSurveyListFailure() {
        // Given:        
        let pageNumber = 1
        let pageSize = 10
        let error = AppError.unAuthorized
        let userProfile = makeUserProfileEntity( type: "user", id: "1")
        
        stubFetchSurveyList(pageNumber: pageNumber, pageSize: pageSize, expectationResult: .error(error))
        stubFetchUserProfile(expectationResult: .just(userProfile))
        
        // When:
        sut.surveys
            .drive(onNext: { models in
                XCTFail("It should never called")
            })
            .disposed(by: disposeBag)
        sut.input.viewDidLoadTrigger.accept(())
        
        // Then:
        verify(surveyRepository).fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
        verify(authRepository).fetchUserProfile()
    }
    
    func testHeaderSubTitle() {
        // Given:
        let expected = expectation(description: #function)
        
        // When:
        let subscription = sut.headerSubTitle
            .drive(onNext: { title in
                XCTAssertTrue(title.isNotEmpty) /// Will be improve in next MR by replace the DateTime Repository
                expected.fulfill()
            })
        
        // Then:
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testHeaderTitle() {
        // Given:
        let expected = expectation(description: #function)
        
        // When:
        let subscription = sut.headerTitle
            .drive(onNext: { title in
                XCTAssertEqual(title, L10n.App.Home.Today.title)
                expected.fulfill()
            })
        
        // Then:
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testProfileImageUrlWhenFetchUserProfileSuccess() {
        // Given:
        let expected = expectation(description: #function)
        let pageNumber = 1
        let pageSize = 10
        let surveyListEntity = SurveyListEntity(
            data: nil,
            meta: nil
        )
        let avatarUrl = "https://avatar.png"
        let userProfile = makeUserProfileEntity(
            type: "user",
            id: "1",
            avatarUrl: avatarUrl
        )
        
        stubFetchSurveyList(pageNumber: pageNumber, pageSize: pageSize, expectationResult: .just(surveyListEntity))
        stubFetchUserProfile(expectationResult: .just(userProfile))
        
        // When:
        let subscription = sut.profileImageUrl
            .drive(onNext: { actualUrl in
                XCTAssertEqual(actualUrl, avatarUrl)
                expected.fulfill()
            })
        sut.input.viewDidLoadTrigger.accept(())
        
        // Then:
        verify(surveyRepository).fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
        verify(authRepository).fetchUserProfile()
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testProfileImageUrlWhenFetchUserProfileFailure() {
        // Given:
        let pageNumber = 1
        let pageSize = 10
        let surveyListEntity = SurveyListEntity(
            data: nil,
            meta: nil
        )
        let error = NSError(message: "error message")
        
        stubFetchSurveyList(pageNumber: pageNumber, pageSize: pageSize, expectationResult: .just(surveyListEntity))
        stubFetchUserProfile(expectationResult: .error(error))
        
        // When:
        sut.profileImageUrl
            .drive(onNext: { actualUrl in
                XCTFail("It should never called")
            })
            .disposed(by: disposeBag)
        
        sut.input.viewDidLoadTrigger.accept(())
        
        // Then:
        verify(surveyRepository).fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
        verify(authRepository).fetchUserProfile()
    }
    
    func testTotalPageWhenFetchSurveyListSuccess() {
        // Given:
        let expected = expectation(description: #function)
        let pageNumber = 1
        let pageSize = 10
        let surveyEntity = makeSurveyEntity()
        let surveyListEntity = SurveyListEntity(
            data: [
                surveyEntity,
                surveyEntity,
                surveyEntity,
                surveyEntity,
                surveyEntity,
                surveyEntity
            ],
            meta: .init(
                page: 1,
                pages: 2,
                pageSize: 10,
                records: 20
            )
        )
        let userProfile = makeUserProfileEntity( type: "user", id: "1")
        
        stubFetchSurveyList(pageNumber: pageNumber, pageSize: pageSize, expectationResult: .just(surveyListEntity))
        stubFetchUserProfile(expectationResult: .just(userProfile))
        sut = makeSUT()
        
        // When:
        let subscription = sut.totalPage
            .drive(onNext: { actualTotalPage in
                XCTAssertEqual(actualTotalPage, 6)
                expected.fulfill()
            })
        sut.input.viewDidLoadTrigger.accept(())
        
        // Then:
        verify(surveyRepository).fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
        verify(authRepository).fetchUserProfile()
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testTotalPageWhenFetchSurveyListFailure() {
        // Given:
        let pageNumber = 1
        let pageSize = 10
        let error = AppError.unAuthorized
        let userProfile = makeUserProfileEntity( type: "user", id: "1")
        
        stubFetchSurveyList(pageNumber: pageNumber, pageSize: pageSize, expectationResult: .error(error))
        stubFetchUserProfile(expectationResult: .just(userProfile))
        
        // When:
        sut.totalPage
            .drive(onNext: { _ in
                XCTFail("It should never called")
            })
            .disposed(by: disposeBag)
        sut.input.viewDidLoadTrigger.accept(())
        
        // Then:
        verify(surveyRepository).fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
        verify(authRepository).fetchUserProfile()
    }
    
    func testShowErrorStateWhenFetchSurveyListFailure() {
        // Given:
        let expected = expectation(description: #function)
        let pageNumber = 1
        let pageSize = 10
        let error = AppError.unknown
        let userProfile = makeUserProfileEntity( type: "user", id: "1")
        
        stubFetchSurveyList(pageNumber: pageNumber, pageSize: pageSize, expectationResult: .error(error))
        stubFetchUserProfile(expectationResult: .just(userProfile))
        
        // When:
        let subscription = sut.showErrorState
            .drive(onNext: { actualError in
                XCTAssertEqual(actualError.localizedDescription, error.localizedDescription)
                expected.fulfill()
            })
        sut.input.viewDidLoadTrigger.accept(())
        
        // Then:
        verify(surveyRepository).fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
        verify(authRepository).fetchUserProfile()
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testShowErrorStateWhenFetchSurveyListFailureWithUnAuthorized() {
        // Given:
        let pageNumber = 1
        let pageSize = 10
        let error = AppError.unAuthorized
        let userProfile = makeUserProfileEntity( type: "user", id: "1")
        
        stubFetchSurveyList(pageNumber: pageNumber, pageSize: pageSize, expectationResult: .error(error))
        stubFetchUserProfile(expectationResult: .just(userProfile))
        
        // When:
        sut.showErrorState
            .drive(onNext: { _ in
               XCTFail("It should never called")
            })
            .disposed(by: disposeBag)
        sut.input.viewDidLoadTrigger.accept(())
        
        // Then:
        verify(surveyRepository).fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
        verify(authRepository).fetchUserProfile()
    }
    
    func testLogoutUserWhenFetchSurveyListFailureWithUnAuthorized() {
        // Given:
        let expected = expectation(description: #function)
        let pageNumber = 1
        let pageSize = 10
        let error = AppError.unAuthorized
        let userProfile = makeUserProfileEntity( type: "user", id: "1")
        
        stubFetchSurveyList(pageNumber: pageNumber, pageSize: pageSize, expectationResult: .error(error))
        stubFetchUserProfile(expectationResult: .just(userProfile))
        
        // When:
        let subscription = sut.logoutUser
            .drive(onNext: { actualError in
                expected.fulfill()
            })
        sut.input.viewDidLoadTrigger.accept(())
        
        // Then:
        verify(surveyRepository).fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
        verify(authRepository).fetchUserProfile()
        waitForExpectations(timeout: 0.1) { _ in
            subscription.dispose()
        }
    }
    
    func testLoadMoreTrigger() {
        // Given:
        let surveysExpected = expectation(description: #function)
        let totalPageExpected = expectation(description: #function)
        let pageNumber = 1
        let pageSize = 10
        let surveyEntity = makeSurveyEntity()
        let surveyListEntity = SurveyListEntity(
            data: [
                surveyEntity,
                surveyEntity,
                surveyEntity,
                surveyEntity,
                surveyEntity
            ],
            meta: .init(
                page: 1,
                pages: 2,
                pageSize: 10,
                records: 20
            )
        )
        let userProfile = makeUserProfileEntity( type: "user", id: "1")
        
        stubFetchSurveyList(pageNumber: pageNumber, pageSize: pageSize, expectationResult: .just(surveyListEntity))
        stubFetchSurveyList(pageNumber: pageNumber + 1, pageSize: pageSize, expectationResult: .just(surveyListEntity))
        stubFetchUserProfile(expectationResult: .just(userProfile))
        sut = makeSUT()
        sut.input.viewDidLoadTrigger.accept(())
        
        // When:
        let surveysSubscription = sut.surveys
            .skip(1)
            .drive(onNext: { models in
                XCTAssertEqual(models.count, 10)
                surveysExpected.fulfill()
            })
        
        let totalPageSubscription = sut.totalPage
            .skip(1)
            .drive(onNext: { totalPage in
                XCTAssertEqual(totalPage, 10)
                totalPageExpected.fulfill()
            })
        
        
        sut.input.loadMoreTrigger.accept(())
        
        // Then:
        verify(surveyRepository).fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
        verify(authRepository).fetchUserProfile()
        waitForExpectations(timeout: 0.1) { _ in
            surveysSubscription.dispose()
            totalPageSubscription.dispose()
        }
    }
}

extension HomeViewModelTests {
    
    private func makeSUT() -> HomeViewModel {
        let sut = HomeViewModel(
            surveyRepository: surveyRepository,
            authRepository: authRepository
        )
        return sut
    }

    private func stubFetchUserProfile(
        expectationResult: Observable<UserProfileEntity>
    ) {
        Cuckoo.stub(authRepository) {
            when(
                $0.fetchUserProfile()
            )
            .thenReturn(expectationResult)
        }
    }
    private func stubFetchSurveyList(
        pageNumber: Int = 1,
        pageSize: Int = 5,
        expectationResult: Observable<SurveyListEntity>
    ) {
        Cuckoo.stub(surveyRepository) {
            when(
                $0.fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize)
            )
            .thenReturn(expectationResult)
        }
    }
}
