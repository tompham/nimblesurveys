import Cuckoo
@testable import NimbleSurveys

import Foundation
import RxSwift






 class MockAuthenticationServiceProtocol: AuthenticationServiceProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = AuthenticationServiceProtocol
    
     typealias Stubbing = __StubbingProxy_AuthenticationServiceProtocol
     typealias Verification = __VerificationProxy_AuthenticationServiceProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: AuthenticationServiceProtocol?

     func enableDefaultImplementation(_ stub: AuthenticationServiceProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
    
     func authenticate(email: String, password: String) -> Observable<CredentialsResponse> {
        
    return cuckoo_manager.call(
    """
    authenticate(email: String, password: String) -> Observable<CredentialsResponse>
    """,
            parameters: (email, password),
            escapingParameters: (email, password),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.authenticate(email: email, password: password))
        
    }
    
    
    
    
    
     func fetchUserProfile() -> Observable<UserProfileResponse> {
        
    return cuckoo_manager.call(
    """
    fetchUserProfile() -> Observable<UserProfileResponse>
    """,
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.fetchUserProfile())
        
    }
    
    
    
    
    
     func forgotPassword(for email: String) -> Observable<MetaDataResponse> {
        
    return cuckoo_manager.call(
    """
    forgotPassword(for: String) -> Observable<MetaDataResponse>
    """,
            parameters: (email),
            escapingParameters: (email),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.forgotPassword(for: email))
        
    }
    
    

     struct __StubbingProxy_AuthenticationServiceProtocol: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager
    
         init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        
        
        func authenticate<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(email: M1, password: M2) -> Cuckoo.ProtocolStubFunction<(String, String), Observable<CredentialsResponse>> where M1.MatchedType == String, M2.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String, String)>] = [wrap(matchable: email) { $0.0 }, wrap(matchable: password) { $0.1 }]
            return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationServiceProtocol.self, method:
    """
    authenticate(email: String, password: String) -> Observable<CredentialsResponse>
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func fetchUserProfile() -> Cuckoo.ProtocolStubFunction<(), Observable<UserProfileResponse>> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationServiceProtocol.self, method:
    """
    fetchUserProfile() -> Observable<UserProfileResponse>
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func forgotPassword<M1: Cuckoo.Matchable>(for email: M1) -> Cuckoo.ProtocolStubFunction<(String), Observable<MetaDataResponse>> where M1.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String)>] = [wrap(matchable: email) { $0 }]
            return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationServiceProtocol.self, method:
    """
    forgotPassword(for: String) -> Observable<MetaDataResponse>
    """, parameterMatchers: matchers))
        }
        
        
    }

     struct __VerificationProxy_AuthenticationServiceProtocol: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation
    
         init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }
    
        
    
        
        
        
        @discardableResult
        func authenticate<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(email: M1, password: M2) -> Cuckoo.__DoNotUse<(String, String), Observable<CredentialsResponse>> where M1.MatchedType == String, M2.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String, String)>] = [wrap(matchable: email) { $0.0 }, wrap(matchable: password) { $0.1 }]
            return cuckoo_manager.verify(
    """
    authenticate(email: String, password: String) -> Observable<CredentialsResponse>
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func fetchUserProfile() -> Cuckoo.__DoNotUse<(), Observable<UserProfileResponse>> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return cuckoo_manager.verify(
    """
    fetchUserProfile() -> Observable<UserProfileResponse>
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func forgotPassword<M1: Cuckoo.Matchable>(for email: M1) -> Cuckoo.__DoNotUse<(String), Observable<MetaDataResponse>> where M1.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String)>] = [wrap(matchable: email) { $0 }]
            return cuckoo_manager.verify(
    """
    forgotPassword(for: String) -> Observable<MetaDataResponse>
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
    }
}


 class AuthenticationServiceProtocolStub: AuthenticationServiceProtocol {
    

    

    
    
    
    
     func authenticate(email: String, password: String) -> Observable<CredentialsResponse>  {
        return DefaultValueRegistry.defaultValue(for: (Observable<CredentialsResponse>).self)
    }
    
    
    
    
    
     func fetchUserProfile() -> Observable<UserProfileResponse>  {
        return DefaultValueRegistry.defaultValue(for: (Observable<UserProfileResponse>).self)
    }
    
    
    
    
    
     func forgotPassword(for email: String) -> Observable<MetaDataResponse>  {
        return DefaultValueRegistry.defaultValue(for: (Observable<MetaDataResponse>).self)
    }
    
    
}





import Cuckoo
@testable import NimbleSurveys

import Foundation
import RxSwift






 class MockSurveyServiceProtocol: SurveyServiceProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = SurveyServiceProtocol
    
     typealias Stubbing = __StubbingProxy_SurveyServiceProtocol
     typealias Verification = __VerificationProxy_SurveyServiceProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: SurveyServiceProtocol?

     func enableDefaultImplementation(_ stub: SurveyServiceProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
    
     func fetchSurveyList(pageNumber: Int, pageSize: Int) -> Observable<SurveyListResponse> {
        
    return cuckoo_manager.call(
    """
    fetchSurveyList(pageNumber: Int, pageSize: Int) -> Observable<SurveyListResponse>
    """,
            parameters: (pageNumber, pageSize),
            escapingParameters: (pageNumber, pageSize),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize))
        
    }
    
    

     struct __StubbingProxy_SurveyServiceProtocol: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager
    
         init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        
        
        func fetchSurveyList<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(pageNumber: M1, pageSize: M2) -> Cuckoo.ProtocolStubFunction<(Int, Int), Observable<SurveyListResponse>> where M1.MatchedType == Int, M2.MatchedType == Int {
            let matchers: [Cuckoo.ParameterMatcher<(Int, Int)>] = [wrap(matchable: pageNumber) { $0.0 }, wrap(matchable: pageSize) { $0.1 }]
            return .init(stub: cuckoo_manager.createStub(for: MockSurveyServiceProtocol.self, method:
    """
    fetchSurveyList(pageNumber: Int, pageSize: Int) -> Observable<SurveyListResponse>
    """, parameterMatchers: matchers))
        }
        
        
    }

     struct __VerificationProxy_SurveyServiceProtocol: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation
    
         init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }
    
        
    
        
        
        
        @discardableResult
        func fetchSurveyList<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(pageNumber: M1, pageSize: M2) -> Cuckoo.__DoNotUse<(Int, Int), Observable<SurveyListResponse>> where M1.MatchedType == Int, M2.MatchedType == Int {
            let matchers: [Cuckoo.ParameterMatcher<(Int, Int)>] = [wrap(matchable: pageNumber) { $0.0 }, wrap(matchable: pageSize) { $0.1 }]
            return cuckoo_manager.verify(
    """
    fetchSurveyList(pageNumber: Int, pageSize: Int) -> Observable<SurveyListResponse>
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
    }
}


 class SurveyServiceProtocolStub: SurveyServiceProtocol {
    

    

    
    
    
    
     func fetchSurveyList(pageNumber: Int, pageSize: Int) -> Observable<SurveyListResponse>  {
        return DefaultValueRegistry.defaultValue(for: (Observable<SurveyListResponse>).self)
    }
    
    
}





import Cuckoo
@testable import NimbleSurveys

import Foundation
import RxSwift






 class MockAuthenticationRepositoryProtocol: AuthenticationRepositoryProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = AuthenticationRepositoryProtocol
    
     typealias Stubbing = __StubbingProxy_AuthenticationRepositoryProtocol
     typealias Verification = __VerificationProxy_AuthenticationRepositoryProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: AuthenticationRepositoryProtocol?

     func enableDefaultImplementation(_ stub: AuthenticationRepositoryProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
    
     func authenticate(email: String, password: String) -> Observable<Void> {
        
    return cuckoo_manager.call(
    """
    authenticate(email: String, password: String) -> Observable<Void>
    """,
            parameters: (email, password),
            escapingParameters: (email, password),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.authenticate(email: email, password: password))
        
    }
    
    
    
    
    
     func fetchUserProfile() -> Observable<UserProfileEntity> {
        
    return cuckoo_manager.call(
    """
    fetchUserProfile() -> Observable<UserProfileEntity>
    """,
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.fetchUserProfile())
        
    }
    
    
    
    
    
     func forgotPassword(for email: String) -> Observable<MetaDataEntity> {
        
    return cuckoo_manager.call(
    """
    forgotPassword(for: String) -> Observable<MetaDataEntity>
    """,
            parameters: (email),
            escapingParameters: (email),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.forgotPassword(for: email))
        
    }
    
    

     struct __StubbingProxy_AuthenticationRepositoryProtocol: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager
    
         init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        
        
        func authenticate<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(email: M1, password: M2) -> Cuckoo.ProtocolStubFunction<(String, String), Observable<Void>> where M1.MatchedType == String, M2.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String, String)>] = [wrap(matchable: email) { $0.0 }, wrap(matchable: password) { $0.1 }]
            return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationRepositoryProtocol.self, method:
    """
    authenticate(email: String, password: String) -> Observable<Void>
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func fetchUserProfile() -> Cuckoo.ProtocolStubFunction<(), Observable<UserProfileEntity>> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationRepositoryProtocol.self, method:
    """
    fetchUserProfile() -> Observable<UserProfileEntity>
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func forgotPassword<M1: Cuckoo.Matchable>(for email: M1) -> Cuckoo.ProtocolStubFunction<(String), Observable<MetaDataEntity>> where M1.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String)>] = [wrap(matchable: email) { $0 }]
            return .init(stub: cuckoo_manager.createStub(for: MockAuthenticationRepositoryProtocol.self, method:
    """
    forgotPassword(for: String) -> Observable<MetaDataEntity>
    """, parameterMatchers: matchers))
        }
        
        
    }

     struct __VerificationProxy_AuthenticationRepositoryProtocol: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation
    
         init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }
    
        
    
        
        
        
        @discardableResult
        func authenticate<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(email: M1, password: M2) -> Cuckoo.__DoNotUse<(String, String), Observable<Void>> where M1.MatchedType == String, M2.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String, String)>] = [wrap(matchable: email) { $0.0 }, wrap(matchable: password) { $0.1 }]
            return cuckoo_manager.verify(
    """
    authenticate(email: String, password: String) -> Observable<Void>
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func fetchUserProfile() -> Cuckoo.__DoNotUse<(), Observable<UserProfileEntity>> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return cuckoo_manager.verify(
    """
    fetchUserProfile() -> Observable<UserProfileEntity>
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func forgotPassword<M1: Cuckoo.Matchable>(for email: M1) -> Cuckoo.__DoNotUse<(String), Observable<MetaDataEntity>> where M1.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String)>] = [wrap(matchable: email) { $0 }]
            return cuckoo_manager.verify(
    """
    forgotPassword(for: String) -> Observable<MetaDataEntity>
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
    }
}


 class AuthenticationRepositoryProtocolStub: AuthenticationRepositoryProtocol {
    

    

    
    
    
    
     func authenticate(email: String, password: String) -> Observable<Void>  {
        return DefaultValueRegistry.defaultValue(for: (Observable<Void>).self)
    }
    
    
    
    
    
     func fetchUserProfile() -> Observable<UserProfileEntity>  {
        return DefaultValueRegistry.defaultValue(for: (Observable<UserProfileEntity>).self)
    }
    
    
    
    
    
     func forgotPassword(for email: String) -> Observable<MetaDataEntity>  {
        return DefaultValueRegistry.defaultValue(for: (Observable<MetaDataEntity>).self)
    }
    
    
}





import Cuckoo
@testable import NimbleSurveys

import Foundation
import RxSwift






 class MockSurveyRepositoryProtocol: SurveyRepositoryProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = SurveyRepositoryProtocol
    
     typealias Stubbing = __StubbingProxy_SurveyRepositoryProtocol
     typealias Verification = __VerificationProxy_SurveyRepositoryProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: SurveyRepositoryProtocol?

     func enableDefaultImplementation(_ stub: SurveyRepositoryProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
    
     func fetchSurveyList(pageNumber: Int, pageSize: Int) -> Observable<SurveyListEntity> {
        
    return cuckoo_manager.call(
    """
    fetchSurveyList(pageNumber: Int, pageSize: Int) -> Observable<SurveyListEntity>
    """,
            parameters: (pageNumber, pageSize),
            escapingParameters: (pageNumber, pageSize),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.fetchSurveyList(pageNumber: pageNumber, pageSize: pageSize))
        
    }
    
    

     struct __StubbingProxy_SurveyRepositoryProtocol: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager
    
         init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        
        
        func fetchSurveyList<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(pageNumber: M1, pageSize: M2) -> Cuckoo.ProtocolStubFunction<(Int, Int), Observable<SurveyListEntity>> where M1.MatchedType == Int, M2.MatchedType == Int {
            let matchers: [Cuckoo.ParameterMatcher<(Int, Int)>] = [wrap(matchable: pageNumber) { $0.0 }, wrap(matchable: pageSize) { $0.1 }]
            return .init(stub: cuckoo_manager.createStub(for: MockSurveyRepositoryProtocol.self, method:
    """
    fetchSurveyList(pageNumber: Int, pageSize: Int) -> Observable<SurveyListEntity>
    """, parameterMatchers: matchers))
        }
        
        
    }

     struct __VerificationProxy_SurveyRepositoryProtocol: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation
    
         init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }
    
        
    
        
        
        
        @discardableResult
        func fetchSurveyList<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(pageNumber: M1, pageSize: M2) -> Cuckoo.__DoNotUse<(Int, Int), Observable<SurveyListEntity>> where M1.MatchedType == Int, M2.MatchedType == Int {
            let matchers: [Cuckoo.ParameterMatcher<(Int, Int)>] = [wrap(matchable: pageNumber) { $0.0 }, wrap(matchable: pageSize) { $0.1 }]
            return cuckoo_manager.verify(
    """
    fetchSurveyList(pageNumber: Int, pageSize: Int) -> Observable<SurveyListEntity>
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
    }
}


 class SurveyRepositoryProtocolStub: SurveyRepositoryProtocol {
    

    

    
    
    
    
     func fetchSurveyList(pageNumber: Int, pageSize: Int) -> Observable<SurveyListEntity>  {
        return DefaultValueRegistry.defaultValue(for: (Observable<SurveyListEntity>).self)
    }
    
    
}





import Cuckoo
@testable import NimbleSurveys

import UIKit






 class MockAppNavigatorProtocol: AppNavigatorProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = AppNavigatorProtocol
    
     typealias Stubbing = __StubbingProxy_AppNavigatorProtocol
     typealias Verification = __VerificationProxy_AppNavigatorProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: AppNavigatorProtocol?

     func enableDefaultImplementation(_ stub: AppNavigatorProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
    
     func show(route: Route, from parent: UIViewController?, action: NavigateAction)  {
        
    return cuckoo_manager.call(
    """
    show(route: Route, from: UIViewController?, action: NavigateAction)
    """,
            parameters: (route, parent, action),
            escapingParameters: (route, parent, action),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.show(route: route, from: parent, action: action))
        
    }
    
    

     struct __StubbingProxy_AppNavigatorProtocol: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager
    
         init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        
        
        func show<M1: Cuckoo.Matchable, M2: Cuckoo.OptionalMatchable, M3: Cuckoo.Matchable>(route: M1, from parent: M2, action: M3) -> Cuckoo.ProtocolStubNoReturnFunction<(Route, UIViewController?, NavigateAction)> where M1.MatchedType == Route, M2.OptionalMatchedType == UIViewController, M3.MatchedType == NavigateAction {
            let matchers: [Cuckoo.ParameterMatcher<(Route, UIViewController?, NavigateAction)>] = [wrap(matchable: route) { $0.0 }, wrap(matchable: parent) { $0.1 }, wrap(matchable: action) { $0.2 }]
            return .init(stub: cuckoo_manager.createStub(for: MockAppNavigatorProtocol.self, method:
    """
    show(route: Route, from: UIViewController?, action: NavigateAction)
    """, parameterMatchers: matchers))
        }
        
        
    }

     struct __VerificationProxy_AppNavigatorProtocol: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation
    
         init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }
    
        
    
        
        
        
        @discardableResult
        func show<M1: Cuckoo.Matchable, M2: Cuckoo.OptionalMatchable, M3: Cuckoo.Matchable>(route: M1, from parent: M2, action: M3) -> Cuckoo.__DoNotUse<(Route, UIViewController?, NavigateAction), Void> where M1.MatchedType == Route, M2.OptionalMatchedType == UIViewController, M3.MatchedType == NavigateAction {
            let matchers: [Cuckoo.ParameterMatcher<(Route, UIViewController?, NavigateAction)>] = [wrap(matchable: route) { $0.0 }, wrap(matchable: parent) { $0.1 }, wrap(matchable: action) { $0.2 }]
            return cuckoo_manager.verify(
    """
    show(route: Route, from: UIViewController?, action: NavigateAction)
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
    }
}


 class AppNavigatorProtocolStub: AppNavigatorProtocol {
    

    

    
    
    
    
     func show(route: Route, from parent: UIViewController?, action: NavigateAction)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
    
}





import Cuckoo
@testable import NimbleSurveys

import Foundation
import Resolver






 class MockErrorHandlerProtocol: ErrorHandlerProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = ErrorHandlerProtocol
    
     typealias Stubbing = __StubbingProxy_ErrorHandlerProtocol
     typealias Verification = __VerificationProxy_ErrorHandlerProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: ErrorHandlerProtocol?

     func enableDefaultImplementation(_ stub: ErrorHandlerProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
    
     func handleError(error: Error, on controller: UIViewController)  {
        
    return cuckoo_manager.call(
    """
    handleError(error: Error, on: UIViewController)
    """,
            parameters: (error, controller),
            escapingParameters: (error, controller),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.handleError(error: error, on: controller))
        
    }
    
    

     struct __StubbingProxy_ErrorHandlerProtocol: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager
    
         init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        
        
        func handleError<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(error: M1, on controller: M2) -> Cuckoo.ProtocolStubNoReturnFunction<(Error, UIViewController)> where M1.MatchedType == Error, M2.MatchedType == UIViewController {
            let matchers: [Cuckoo.ParameterMatcher<(Error, UIViewController)>] = [wrap(matchable: error) { $0.0 }, wrap(matchable: controller) { $0.1 }]
            return .init(stub: cuckoo_manager.createStub(for: MockErrorHandlerProtocol.self, method:
    """
    handleError(error: Error, on: UIViewController)
    """, parameterMatchers: matchers))
        }
        
        
    }

     struct __VerificationProxy_ErrorHandlerProtocol: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation
    
         init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }
    
        
    
        
        
        
        @discardableResult
        func handleError<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(error: M1, on controller: M2) -> Cuckoo.__DoNotUse<(Error, UIViewController), Void> where M1.MatchedType == Error, M2.MatchedType == UIViewController {
            let matchers: [Cuckoo.ParameterMatcher<(Error, UIViewController)>] = [wrap(matchable: error) { $0.0 }, wrap(matchable: controller) { $0.1 }]
            return cuckoo_manager.verify(
    """
    handleError(error: Error, on: UIViewController)
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
    }
}


 class ErrorHandlerProtocolStub: ErrorHandlerProtocol {
    

    

    
    
    
    
     func handleError(error: Error, on controller: UIViewController)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
    
}





import Cuckoo
@testable import NimbleSurveys

import Foundation






 class MockDataValidatorProtocol: DataValidatorProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = DataValidatorProtocol
    
     typealias Stubbing = __StubbingProxy_DataValidatorProtocol
     typealias Verification = __VerificationProxy_DataValidatorProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: DataValidatorProtocol?

     func enableDefaultImplementation(_ stub: DataValidatorProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
    
     func validateEmail(_ email: String) throws -> Bool {
        
    return try cuckoo_manager.callThrows(
    """
    validateEmail(_: String) throws -> Bool
    """,
            parameters: (email),
            escapingParameters: (email),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.validateEmail(email))
        
    }
    
    

     struct __StubbingProxy_DataValidatorProtocol: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager
    
         init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        
        
        func validateEmail<M1: Cuckoo.Matchable>(_ email: M1) -> Cuckoo.ProtocolStubThrowingFunction<(String), Bool> where M1.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String)>] = [wrap(matchable: email) { $0 }]
            return .init(stub: cuckoo_manager.createStub(for: MockDataValidatorProtocol.self, method:
    """
    validateEmail(_: String) throws -> Bool
    """, parameterMatchers: matchers))
        }
        
        
    }

     struct __VerificationProxy_DataValidatorProtocol: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation
    
         init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }
    
        
    
        
        
        
        @discardableResult
        func validateEmail<M1: Cuckoo.Matchable>(_ email: M1) -> Cuckoo.__DoNotUse<(String), Bool> where M1.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String)>] = [wrap(matchable: email) { $0 }]
            return cuckoo_manager.verify(
    """
    validateEmail(_: String) throws -> Bool
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
    }
}


 class DataValidatorProtocolStub: DataValidatorProtocol {
    

    

    
    
    
    
     func validateEmail(_ email: String) throws -> Bool  {
        return DefaultValueRegistry.defaultValue(for: (Bool).self)
    }
    
    
}





import Cuckoo
@testable import NimbleSurveys

import Foundation
import Moya
import RxSwift






 class MockAPIClientProtocol: APIClientProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = APIClientProtocol
    
     typealias Stubbing = __StubbingProxy_APIClientProtocol
     typealias Verification = __VerificationProxy_APIClientProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: APIClientProtocol?

     func enableDefaultImplementation(_ stub: APIClientProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
    
     func request(target: TargetType) -> Single<Data> {
        
    return cuckoo_manager.call(
    """
    request(target: TargetType) -> Single<Data>
    """,
            parameters: (target),
            escapingParameters: (target),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.request(target: target))
        
    }
    
    

     struct __StubbingProxy_APIClientProtocol: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager
    
         init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        
        
        func request<M1: Cuckoo.Matchable>(target: M1) -> Cuckoo.ProtocolStubFunction<(TargetType), Single<Data>> where M1.MatchedType == TargetType {
            let matchers: [Cuckoo.ParameterMatcher<(TargetType)>] = [wrap(matchable: target) { $0 }]
            return .init(stub: cuckoo_manager.createStub(for: MockAPIClientProtocol.self, method:
    """
    request(target: TargetType) -> Single<Data>
    """, parameterMatchers: matchers))
        }
        
        
    }

     struct __VerificationProxy_APIClientProtocol: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation
    
         init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }
    
        
    
        
        
        
        @discardableResult
        func request<M1: Cuckoo.Matchable>(target: M1) -> Cuckoo.__DoNotUse<(TargetType), Single<Data>> where M1.MatchedType == TargetType {
            let matchers: [Cuckoo.ParameterMatcher<(TargetType)>] = [wrap(matchable: target) { $0 }]
            return cuckoo_manager.verify(
    """
    request(target: TargetType) -> Single<Data>
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
    }
}


 class APIClientProtocolStub: APIClientProtocol {
    

    

    
    
    
    
     func request(target: TargetType) -> Single<Data>  {
        return DefaultValueRegistry.defaultValue(for: (Single<Data>).self)
    }
    
    
}





import Cuckoo
@testable import NimbleSurveys

import Foundation






 class MockCredentialsProviderProtocol: CredentialsProviderProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = CredentialsProviderProtocol
    
     typealias Stubbing = __StubbingProxy_CredentialsProviderProtocol
     typealias Verification = __VerificationProxy_CredentialsProviderProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: CredentialsProviderProtocol?

     func enableDefaultImplementation(_ stub: CredentialsProviderProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
    
     func getAccessToken() -> String? {
        
    return cuckoo_manager.call(
    """
    getAccessToken() -> String?
    """,
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.getAccessToken())
        
    }
    
    
    
    
    
     func getRefreshToken() -> String? {
        
    return cuckoo_manager.call(
    """
    getRefreshToken() -> String?
    """,
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.getRefreshToken())
        
    }
    
    
    
    
    
     func store(credentials: Data) -> Bool {
        
    return cuckoo_manager.call(
    """
    store(credentials: Data) -> Bool
    """,
            parameters: (credentials),
            escapingParameters: (credentials),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.store(credentials: credentials))
        
    }
    
    
    
    
    
     func retrieveCredentials() -> CredentialsResponse? {
        
    return cuckoo_manager.call(
    """
    retrieveCredentials() -> CredentialsResponse?
    """,
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.retrieveCredentials())
        
    }
    
    
    
    
    
     func clearCredentials() -> Bool {
        
    return cuckoo_manager.call(
    """
    clearCredentials() -> Bool
    """,
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.clearCredentials())
        
    }
    
    

     struct __StubbingProxy_CredentialsProviderProtocol: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager
    
         init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        
        
        func getAccessToken() -> Cuckoo.ProtocolStubFunction<(), String?> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return .init(stub: cuckoo_manager.createStub(for: MockCredentialsProviderProtocol.self, method:
    """
    getAccessToken() -> String?
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func getRefreshToken() -> Cuckoo.ProtocolStubFunction<(), String?> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return .init(stub: cuckoo_manager.createStub(for: MockCredentialsProviderProtocol.self, method:
    """
    getRefreshToken() -> String?
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func store<M1: Cuckoo.Matchable>(credentials: M1) -> Cuckoo.ProtocolStubFunction<(Data), Bool> where M1.MatchedType == Data {
            let matchers: [Cuckoo.ParameterMatcher<(Data)>] = [wrap(matchable: credentials) { $0 }]
            return .init(stub: cuckoo_manager.createStub(for: MockCredentialsProviderProtocol.self, method:
    """
    store(credentials: Data) -> Bool
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func retrieveCredentials() -> Cuckoo.ProtocolStubFunction<(), CredentialsResponse?> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return .init(stub: cuckoo_manager.createStub(for: MockCredentialsProviderProtocol.self, method:
    """
    retrieveCredentials() -> CredentialsResponse?
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func clearCredentials() -> Cuckoo.ProtocolStubFunction<(), Bool> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return .init(stub: cuckoo_manager.createStub(for: MockCredentialsProviderProtocol.self, method:
    """
    clearCredentials() -> Bool
    """, parameterMatchers: matchers))
        }
        
        
    }

     struct __VerificationProxy_CredentialsProviderProtocol: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation
    
         init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }
    
        
    
        
        
        
        @discardableResult
        func getAccessToken() -> Cuckoo.__DoNotUse<(), String?> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return cuckoo_manager.verify(
    """
    getAccessToken() -> String?
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func getRefreshToken() -> Cuckoo.__DoNotUse<(), String?> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return cuckoo_manager.verify(
    """
    getRefreshToken() -> String?
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func store<M1: Cuckoo.Matchable>(credentials: M1) -> Cuckoo.__DoNotUse<(Data), Bool> where M1.MatchedType == Data {
            let matchers: [Cuckoo.ParameterMatcher<(Data)>] = [wrap(matchable: credentials) { $0 }]
            return cuckoo_manager.verify(
    """
    store(credentials: Data) -> Bool
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func retrieveCredentials() -> Cuckoo.__DoNotUse<(), CredentialsResponse?> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return cuckoo_manager.verify(
    """
    retrieveCredentials() -> CredentialsResponse?
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func clearCredentials() -> Cuckoo.__DoNotUse<(), Bool> {
            let matchers: [Cuckoo.ParameterMatcher<Void>] = []
            return cuckoo_manager.verify(
    """
    clearCredentials() -> Bool
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
    }
}


 class CredentialsProviderProtocolStub: CredentialsProviderProtocol {
    

    

    
    
    
    
     func getAccessToken() -> String?  {
        return DefaultValueRegistry.defaultValue(for: (String?).self)
    }
    
    
    
    
    
     func getRefreshToken() -> String?  {
        return DefaultValueRegistry.defaultValue(for: (String?).self)
    }
    
    
    
    
    
     func store(credentials: Data) -> Bool  {
        return DefaultValueRegistry.defaultValue(for: (Bool).self)
    }
    
    
    
    
    
     func retrieveCredentials() -> CredentialsResponse?  {
        return DefaultValueRegistry.defaultValue(for: (CredentialsResponse?).self)
    }
    
    
    
    
    
     func clearCredentials() -> Bool  {
        return DefaultValueRegistry.defaultValue(for: (Bool).self)
    }
    
    
}





import Cuckoo
@testable import NimbleSurveys

import Foundation
import Japx






public class MockDecoderProtocol: DecoderProtocol, Cuckoo.ProtocolMock {
    
    public typealias MocksType = DecoderProtocol
    
    public typealias Stubbing = __StubbingProxy_DecoderProtocol
    public typealias Verification = __VerificationProxy_DecoderProtocol

    public let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: DecoderProtocol?

    public func enableDefaultImplementation(_ stub: DecoderProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
    
    public func decode<T>(_ type: T.Type, from data: Data) throws -> T where T : Decodable {
        
    return try cuckoo_manager.callThrows(
    """
    decode(_: T.Type, from: Data) throws -> T where T : Decodable
    """,
            parameters: (type, data),
            escapingParameters: (type, data),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.decode(type, from: data))
        
    }
    
    

    public struct __StubbingProxy_DecoderProtocol: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager
    
        public init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        
        
        func decode<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable, T>(_ type: M1, from data: M2) -> Cuckoo.ProtocolStubThrowingFunction<(T.Type, Data), T> where M1.MatchedType == T.Type, M2.MatchedType == Data, T : Decodable {
            let matchers: [Cuckoo.ParameterMatcher<(T.Type, Data)>] = [wrap(matchable: type) { $0.0 }, wrap(matchable: data) { $0.1 }]
            return .init(stub: cuckoo_manager.createStub(for: MockDecoderProtocol.self, method:
    """
    decode(_: T.Type, from: Data) throws -> T where T : Decodable
    """, parameterMatchers: matchers))
        }
        
        
    }

    public struct __VerificationProxy_DecoderProtocol: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation
    
        public init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }
    
        
    
        
        
        
        @discardableResult
        func decode<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable, T>(_ type: M1, from data: M2) -> Cuckoo.__DoNotUse<(T.Type, Data), T> where M1.MatchedType == T.Type, M2.MatchedType == Data, T : Decodable {
            let matchers: [Cuckoo.ParameterMatcher<(T.Type, Data)>] = [wrap(matchable: type) { $0.0 }, wrap(matchable: data) { $0.1 }]
            return cuckoo_manager.verify(
    """
    decode(_: T.Type, from: Data) throws -> T where T : Decodable
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
    }
}


public class DecoderProtocolStub: DecoderProtocol {
    

    

    
    
    
    
    public func decode<T>(_ type: T.Type, from data: Data) throws -> T where T : Decodable  {
        return DefaultValueRegistry.defaultValue(for: (T).self)
    }
    
    
}





import Cuckoo
@testable import NimbleSurveys

import Foundation






public class MockEncoderProtocol: EncoderProtocol, Cuckoo.ProtocolMock {
    
    public typealias MocksType = EncoderProtocol
    
    public typealias Stubbing = __StubbingProxy_EncoderProtocol
    public typealias Verification = __VerificationProxy_EncoderProtocol

    public let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: EncoderProtocol?

    public func enableDefaultImplementation(_ stub: EncoderProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
    
    public func encode<T>(_ value: T) throws -> Data where T : Encodable {
        
    return try cuckoo_manager.callThrows(
    """
    encode(_: T) throws -> Data where T : Encodable
    """,
            parameters: (value),
            escapingParameters: (value),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.encode(value))
        
    }
    
    

    public struct __StubbingProxy_EncoderProtocol: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager
    
        public init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        
        
        func encode<M1: Cuckoo.Matchable, T>(_ value: M1) -> Cuckoo.ProtocolStubThrowingFunction<(T), Data> where M1.MatchedType == T, T : Encodable {
            let matchers: [Cuckoo.ParameterMatcher<(T)>] = [wrap(matchable: value) { $0 }]
            return .init(stub: cuckoo_manager.createStub(for: MockEncoderProtocol.self, method:
    """
    encode(_: T) throws -> Data where T : Encodable
    """, parameterMatchers: matchers))
        }
        
        
    }

    public struct __VerificationProxy_EncoderProtocol: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation
    
        public init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }
    
        
    
        
        
        
        @discardableResult
        func encode<M1: Cuckoo.Matchable, T>(_ value: M1) -> Cuckoo.__DoNotUse<(T), Data> where M1.MatchedType == T, T : Encodable {
            let matchers: [Cuckoo.ParameterMatcher<(T)>] = [wrap(matchable: value) { $0 }]
            return cuckoo_manager.verify(
    """
    encode(_: T) throws -> Data where T : Encodable
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
    }
}


public class EncoderProtocolStub: EncoderProtocol {
    

    

    
    
    
    
    public func encode<T>(_ value: T) throws -> Data where T : Encodable  {
        return DefaultValueRegistry.defaultValue(for: (Data).self)
    }
    
    
}





import Cuckoo
@testable import NimbleSurveys

import Foundation
import Moya






 class MockRefreshTokenTargetProviderProtocol: RefreshTokenTargetProviderProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = RefreshTokenTargetProviderProtocol
    
     typealias Stubbing = __StubbingProxy_RefreshTokenTargetProviderProtocol
     typealias Verification = __VerificationProxy_RefreshTokenTargetProviderProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: RefreshTokenTargetProviderProtocol?

     func enableDefaultImplementation(_ stub: RefreshTokenTargetProviderProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
    
     func createRefreshTokenTarget(refreshToken: String) -> TargetType {
        
    return cuckoo_manager.call(
    """
    createRefreshTokenTarget(refreshToken: String) -> TargetType
    """,
            parameters: (refreshToken),
            escapingParameters: (refreshToken),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.createRefreshTokenTarget(refreshToken: refreshToken))
        
    }
    
    

     struct __StubbingProxy_RefreshTokenTargetProviderProtocol: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager
    
         init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        
        
        func createRefreshTokenTarget<M1: Cuckoo.Matchable>(refreshToken: M1) -> Cuckoo.ProtocolStubFunction<(String), TargetType> where M1.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String)>] = [wrap(matchable: refreshToken) { $0 }]
            return .init(stub: cuckoo_manager.createStub(for: MockRefreshTokenTargetProviderProtocol.self, method:
    """
    createRefreshTokenTarget(refreshToken: String) -> TargetType
    """, parameterMatchers: matchers))
        }
        
        
    }

     struct __VerificationProxy_RefreshTokenTargetProviderProtocol: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation
    
         init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }
    
        
    
        
        
        
        @discardableResult
        func createRefreshTokenTarget<M1: Cuckoo.Matchable>(refreshToken: M1) -> Cuckoo.__DoNotUse<(String), TargetType> where M1.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String)>] = [wrap(matchable: refreshToken) { $0 }]
            return cuckoo_manager.verify(
    """
    createRefreshTokenTarget(refreshToken: String) -> TargetType
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
    }
}


 class RefreshTokenTargetProviderProtocolStub: RefreshTokenTargetProviderProtocol {
    

    

    
    
    
    
     func createRefreshTokenTarget(refreshToken: String) -> TargetType  {
        return DefaultValueRegistry.defaultValue(for: (TargetType).self)
    }
    
    
}





import Cuckoo
@testable import NimbleSurveys

import Foundation






 class MockDSMessagePresenterProtocol: DSMessagePresenterProtocol, Cuckoo.ProtocolMock {
    
     typealias MocksType = DSMessagePresenterProtocol
    
     typealias Stubbing = __StubbingProxy_DSMessagePresenterProtocol
     typealias Verification = __VerificationProxy_DSMessagePresenterProtocol

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: DSMessagePresenterProtocol?

     func enableDefaultImplementation(_ stub: DSMessagePresenterProtocol) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
    
     func showNotificationView(title: String, message: String)  {
        
    return cuckoo_manager.call(
    """
    showNotificationView(title: String, message: String)
    """,
            parameters: (title, message),
            escapingParameters: (title, message),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.showNotificationView(title: title, message: message))
        
    }
    
    
    
    
    
     func showErrorMessage(_ message: String)  {
        
    return cuckoo_manager.call(
    """
    showErrorMessage(_: String)
    """,
            parameters: (message),
            escapingParameters: (message),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.showErrorMessage(message))
        
    }
    
    

     struct __StubbingProxy_DSMessagePresenterProtocol: Cuckoo.StubbingProxy {
        private let cuckoo_manager: Cuckoo.MockManager
    
         init(manager: Cuckoo.MockManager) {
            self.cuckoo_manager = manager
        }
        
        
        
        
        func showNotificationView<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(title: M1, message: M2) -> Cuckoo.ProtocolStubNoReturnFunction<(String, String)> where M1.MatchedType == String, M2.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String, String)>] = [wrap(matchable: title) { $0.0 }, wrap(matchable: message) { $0.1 }]
            return .init(stub: cuckoo_manager.createStub(for: MockDSMessagePresenterProtocol.self, method:
    """
    showNotificationView(title: String, message: String)
    """, parameterMatchers: matchers))
        }
        
        
        
        
        func showErrorMessage<M1: Cuckoo.Matchable>(_ message: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(String)> where M1.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String)>] = [wrap(matchable: message) { $0 }]
            return .init(stub: cuckoo_manager.createStub(for: MockDSMessagePresenterProtocol.self, method:
    """
    showErrorMessage(_: String)
    """, parameterMatchers: matchers))
        }
        
        
    }

     struct __VerificationProxy_DSMessagePresenterProtocol: Cuckoo.VerificationProxy {
        private let cuckoo_manager: Cuckoo.MockManager
        private let callMatcher: Cuckoo.CallMatcher
        private let sourceLocation: Cuckoo.SourceLocation
    
         init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
            self.cuckoo_manager = manager
            self.callMatcher = callMatcher
            self.sourceLocation = sourceLocation
        }
    
        
    
        
        
        
        @discardableResult
        func showNotificationView<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(title: M1, message: M2) -> Cuckoo.__DoNotUse<(String, String), Void> where M1.MatchedType == String, M2.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String, String)>] = [wrap(matchable: title) { $0.0 }, wrap(matchable: message) { $0.1 }]
            return cuckoo_manager.verify(
    """
    showNotificationView(title: String, message: String)
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
        
        
        @discardableResult
        func showErrorMessage<M1: Cuckoo.Matchable>(_ message: M1) -> Cuckoo.__DoNotUse<(String), Void> where M1.MatchedType == String {
            let matchers: [Cuckoo.ParameterMatcher<(String)>] = [wrap(matchable: message) { $0 }]
            return cuckoo_manager.verify(
    """
    showErrorMessage(_: String)
    """, callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
        }
        
        
    }
}


 class DSMessagePresenterProtocolStub: DSMessagePresenterProtocol {
    

    

    
    
    
    
     func showNotificationView(title: String, message: String)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
    
    
    
    
     func showErrorMessage(_ message: String)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
    
}




