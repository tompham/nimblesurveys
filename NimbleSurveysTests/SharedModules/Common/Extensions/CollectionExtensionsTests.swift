//
//  CollectionExtensionsTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 06/01/2024.
//

import XCTest
@testable import NimbleSurveys

final class CollectionExtensionsTests: XCTestCase {


    func testIsNotEmptyReturnTrueWhenCollectionHasData() {
        // Given:
        let numbers = [1,2,3,4,5]
        
        // When:
        let result = numbers.isNotEmpty
        
        // Then:
        XCTAssertTrue(result)
    }
    
    func testIsNotEmptyReturnFalseWhenCollectionHasNoData() {
        // Given:
        let numbers = [Int]()
        
        // When:
        let result = numbers.isNotEmpty
        
        // Then:
        XCTAssertFalse(result)
    }
}
