//
//  OptionalExtensionsTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 06/01/2024.
//

import XCTest
@testable import NimbleSurveys

final class OptionalExtensionsTests: XCTestCase {

    func testOrEmptyWhenCurrentIsNil() {

        // Given:
        let username: String? = nil

        // When:
        let expected = username.orEmpty

        XCTAssertEqual(expected, "")
    }

    func testOrEmptyWhenCurrentIsNotNil() {

        // Given:
        let username: String? = "sample user"

        // When:
        let expected = username.orEmpty

        // Then:
        XCTAssertEqual(expected, "sample user")
    }

    func testOrFunctionWithNumberIsNil() {

        // Given:
        let number: Int? = nil

        // When:
        let expected = number.or(1)

        // Then:
        XCTAssertEqual(expected, 1)
    }

    func testOrFunctionWithNumberNoNil() {

        // Given:
        let number: Int? = 10

        // When:
        let expected = number.or(1)

        // Then:
        XCTAssertEqual(expected, 10)
    }

    func testOrFunctionWithObjectIsNil() {

        // Given:
        let fakeObject: FakeryObject? = nil

        // When:
        let expected = fakeObject.or(FakeryObject(value: "test"))

        // Then:
        XCTAssertEqual(expected.value, "test")
    }

    func testOrFunctionWithObjectNotNil() {

        // Given:
        let fakeObject: FakeryObject? = FakeryObject(value: "sample value")

        // When:
        let expected = fakeObject.or(FakeryObject(value: "test"))

        // Then:
        XCTAssertEqual(expected.value, "sample value")
    }
}

// MARK: - OptionalExtensionTest.FakeryObject

extension OptionalExtensionsTests {

    struct FakeryObject {
        let value: String
    }
}
