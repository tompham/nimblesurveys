//
//  StringExtensionsTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 06/01/2024.
//

import XCTest
@testable import NimbleSurveys

final class StringExtensionsTests: XCTestCase {

    func testEmptyString() {
        // When:
        let result = String.empty
        
        // Then:
        XCTAssertEqual(result, "")
    }

}
