//
//  PageInfoTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 07/01/2024.
//

import XCTest
@testable import NimbleSurveys

final class PageInfoTests: XCTestCase {

    var sut: PageInfo!
    
    override func setUpWithError() throws {
        sut = PageInfo(pageNumber: 1, pageSize: 10)
    }

    override func tearDownWithError() throws {
        sut = nil
    }
    
    func testMovingToNextPage() {
        // When:
        sut.moveToNextPage()
        
        // Then:
        XCTAssertEqual(sut.pageNumber, 2)
    }
    
    func testReset() {
        // Given:
        sut.moveToNextPage()
        
        // When:
        sut.reset()
        
        // Then:
        XCTAssertEqual(sut.pageNumber, 1)
    }
    
    func testIsFirstPage() {
        // When:
        let result = sut.isFirstPage
        
        // Then:
        XCTAssertTrue(result)
    }
    
    func testIsFirstPageWhenPageWhenPageNumberIsNotFirst() {
        // Given:
        sut.moveToNextPage()
        
        // When:
        let result = sut.isFirstPage
        
        // Then:
        XCTAssertFalse(result)
    }
}
