//
//  DataValidatorTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 11/01/2024.
//

import XCTest
@testable import NimbleSurveys

final class DataValidatorTests: XCTestCase {

    var sut: DataValidator!
    
    override func setUpWithError() throws {
        sut = DataValidator()
    }
    
    override func tearDownWithError() throws {
        sut = nil
    }
    
    func testValidatorEmailWhenEmailIsValid() throws {
        // Given:
        let validEmails = [
            "phamanhtuancross@gmail.com",
            "phamanhtuancross@yahoo.com",
            "phamanhtuancross@facebook.com"
        ]
        
        // Then:
        try validEmails.forEach { email in
            var result: Bool!
            XCTAssertNoThrow(result = try sut.validateEmail(email))
            XCTAssertTrue(result)
        }
    }
    
    func testValidatorEmailWhenEmailIsInValid() throws {
        // Given:
        let invalidEmails = [
            "phamanhtuancross@.com",
            "phamanhtuancross.com",
            "phamanhtuancross@"
        ]
        
        // Then:
        try invalidEmails.forEach { email in
            XCTAssertThrowsError(try sut.validateEmail(email))
        }
    }
}
