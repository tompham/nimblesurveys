//
//  APIClientTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 06/01/2024.
//

import XCTest
import Cuckoo
import RxSwift
import RxCocoa
import Moya
@testable import NimbleSurveys

final class APIClientTests: XCTestCase {
    
    var sut: APIClient!
    
    var decoder: MockDecoderProtocol!
    var credentialsProvider: MockCredentialsProviderProtocol!
    var refreshTokenTargetProvider: MockRefreshTokenTargetProviderProtocol!
    
    override func setUpWithError() throws {
        decoder = MockDecoderProtocol()
        credentialsProvider = MockCredentialsProviderProtocol()
        refreshTokenTargetProvider = MockRefreshTokenTargetProviderProtocol()
        
        sut = makeSUT()
    }
    
    override func tearDownWithError() throws {
        Cuckoo.reset(decoder, credentialsProvider, refreshTokenTargetProvider)
        
        decoder = nil
        credentialsProvider = nil
        refreshTokenTargetProvider = nil
        
        sut = nil
    }
    
    func testRequestSuccess() {
        // Given:
        let expected = expectation(description: #function)
        let apiTimeout: TimeInterval = 3
        let target = MockAuthorizedTarget()
        
        // When:
        let subscription = requestToken(target: target)
            .subscribe(
                onSuccess: { data in
                    self.assertAuthTokenData(data: data)
                    expected.fulfill()
                }
            )
        
        // Then:
        waitForExpectations(timeout: apiTimeout) { _ in
            subscription.dispose()
        }
    }
    
    func testRequestFailure() {
        // Given:
        let expected = expectation(description: #function)
        let apiTimeout: TimeInterval = 3
        let target = MockErrorTarget(customPath: "/v1/error/path")
        
        // When:
        let subscription = requestToken(target: target)
            .subscribe (
                onSuccess: { token in
                    XCTFail("It should never success")
                },
                onFailure: { error in
                    XCTAssertEqual(error as? AppError, .unknown)
                    expected.fulfill()
                }
            )
        
        // Then:
        waitForExpectations(timeout: apiTimeout) { _ in
            subscription.dispose()
        }
    }
    
    func testRequestUnAuthorizedThenCallRefreshToken() {
        // Given:
        let expected = expectation(description: #function)
        let apiTimeout: TimeInterval = 10
        let refreshToken = "RefreshToken"
        let unAuthorizedTaraget = MockUnAuthorizedTarget()
        
        stubCredentialsProvider(refreshToken: refreshToken)
        stubCreateRefreshTokenTarget(expectation: MockAuthorizedTarget(), refreshToken: refreshToken)
        stubStoreCredentials(expectationResult: true)
        stubClearCredentials()
        
        // When:
        let subscription = requestToken(target: unAuthorizedTaraget)
            .subscribe (
                onFailure: { error in
                    XCTAssertEqual(error as? AppError, .unAuthorized)
                    expected.fulfill()
                }
            )
        
        // Then:
        waitForExpectations(timeout: apiTimeout) { _ in
            subscription.dispose()
        }
    }
}

// MARK: - Privates

extension APIClientTests {
    
    
    private func makeSUT() -> APIClient {
        return APIClient(
            provider: .default,
            credentialsProvider: credentialsProvider,
            refreshTokenTargetProvider: refreshTokenTargetProvider
        )
    }
    
    private func assertAuthTokenData(data: Data) {
        var response: CredentialsResponse!
        XCTAssertNoThrow(response = try JSONDecoder().decode(DataResponseWrapper<CredentialsResponse>.self, from: data).data)
        XCTAssertNotNil(response.id)
        XCTAssertNotNil(response.type)
    }
    
    private func stubCredentialsProvider(
        accessToken: String? = nil,
        refreshToken: String? = nil
    ) {
        Cuckoo.stub(credentialsProvider) {
            when($0.getAccessToken()).thenReturn(accessToken)
            when($0.getRefreshToken()).thenReturn(refreshToken)
        }
    }
    
    private func requestToken(target: TargetType) -> Single<Data> {
        return sut.request(target: target)
    }
    
    private func stubCreateRefreshTokenTarget(expectation: TargetType, refreshToken: String) {
        Cuckoo.stub(refreshTokenTargetProvider) {
            when($0.createRefreshTokenTarget(refreshToken: refreshToken)).thenReturn(expectation)
        }
    }
    
    private func stubStoreCredentials(
        expectationResult: Bool = true
    ) {
        Cuckoo.stub(credentialsProvider) {
            when(
                $0.store(credentials: any(Data.self))
            )
            .thenReturn(expectationResult)
        }
    }
    
    private func stubClearCredentials() {
        Cuckoo.stub(credentialsProvider) {
            when(
                $0.clearCredentials()
            )
            .thenReturn(true)
        }
    }
}
