//
//  CredentialsProviderTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 06/01/2024.
//

import XCTest
import Cuckoo
import KeychainAccess
@testable import NimbleSurveys

final class CredentialsProviderTests: XCTestCase {
    
    var sut: CredentialsProvider!
    var keychain: Keychain!
    var decoder: JSONDecoder!
    
    override func setUpWithError() throws {
        keychain = Keychain(service: "nimble.surveys.testing.workspace")
        decoder = JSONDecoder()
        
        sut = CredentialsProvider(
            keychain: keychain,
            decoder: decoder
        )
    }
    
    override func tearDownWithError() throws {
        decoder = nil
        keychain = nil
        
        sut = nil
    }
    
    func testRetrieveCredentials() {
        // Given:
        let credentials = makeCredentialsResponse(
            id: "mock_id",
            type: "token",
            accessToken: "accessToken",
            tokenType: "token",
            expiresIn: Date().timeIntervalSince1970,
            refreshToken: "refreshToken",
            createdAt: Date().timeIntervalSince1970
        )
        var storedResponse = CredentialsDataWrapper(data: credentials)
        var data: Data!
        XCTAssertNoThrow(data = try JSONEncoder().encode(storedResponse))
        sut.store(credentials: data)
        
        // When:
        let result = sut.retrieveCredentials()
        
        // Then:
        XCTAssertEqual(result?.id, credentials.id)
        XCTAssertEqual(result?.type, credentials.type)
        XCTAssertEqual(result?.accessToken, credentials.accessToken)
        XCTAssertEqual(result?.tokenType, credentials.tokenType)
        XCTAssertEqual(result?.expiresIn, credentials.expiresIn)
        XCTAssertEqual(result?.refreshToken, credentials.refreshToken)
        XCTAssertEqual(result?.createdAt, credentials.createdAt)
    }
    
    func testClearCredentials() {
        // Given:
        let credentials = makeCredentialsResponse(
            id: "mock_id",
            type: "token",
            accessToken: "accessToken",
            tokenType: "token",
            expiresIn: Date().timeIntervalSince1970,
            refreshToken: "refreshToken",
            createdAt: Date().timeIntervalSince1970
        )
        var storedResponse = CredentialsDataWrapper(data: credentials)
        var data: Data!
        XCTAssertNoThrow(data = try JSONEncoder().encode(storedResponse))
        sut.store(credentials: data)
        
        // When:
        sut.clearCredentials()
        
        // Then:
        let retrievedCredentials = sut.retrieveCredentials()
        XCTAssertNil(retrievedCredentials)
    }
    
    func testGetAccessToken() {
        // Given:
        let credentials = makeCredentialsResponse(
            id: "mock_id",
            type: "token",
            accessToken: "accessToken",
            tokenType: "token",
            expiresIn: Date().timeIntervalSince1970,
            refreshToken: "refreshToken",
            createdAt: Date().timeIntervalSince1970
        )
        var storedResponse = CredentialsDataWrapper(data: credentials)
        var data: Data!
        XCTAssertNoThrow(data = try JSONEncoder().encode(storedResponse))
        sut.store(credentials: data)
        
        // When:
        let result = sut.getAccessToken()
        
        // Then:
        XCTAssertEqual(result, "accessToken")
    }
    
    func testGetRefreshToken() {
        // Given:
        let credentials = makeCredentialsResponse(
            id: "mock_id",
            type: "token",
            accessToken: "accessToken",
            tokenType: "token",
            expiresIn: Date().timeIntervalSince1970,
            refreshToken: "refreshToken",
            createdAt: Date().timeIntervalSince1970
        )
        var storedResponse = CredentialsDataWrapper(data: credentials)
        var data: Data!
        XCTAssertNoThrow(data = try JSONEncoder().encode(storedResponse))
        sut.store(credentials: data)
        
        // When:
        let result = sut.getRefreshToken()
        
        // Then:
        XCTAssertEqual(result, "refreshToken")
    }
}


struct CredentialsDataWrapper: Codable {
    var data: CredentialsResponse
}
