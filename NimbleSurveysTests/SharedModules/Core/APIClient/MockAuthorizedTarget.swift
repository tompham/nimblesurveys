//
//  MockRealAPITarget.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import Moya

struct MockAuthorizedTarget: TargetType {
    
    var baseURL: URL {
        return URL(string: "https://survey-api.nimblehq.co/api")!
    }
    
    var path: String {
        return "v1/oauth/token"
    }
    
    var task: Task {
        return .requestParameters(
            parameters: [
                "grant_type": "password",
                "email": "phamanhtuancross@gmail.com",
                "password": "11111111",
                "client_id": "6GbE8dhoz519l2N_F99StqoOs6Tcmm1rXgda4q__rIw",
                "client_secret": "_ayfIm7BeUAhx2W1OUqi20fwO3uNxfo1QstyKlFCgHw"
            ],
            encoding: URLEncoding.httpBody
        )
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var headers: [String : String]? {
        return nil
    }
}
