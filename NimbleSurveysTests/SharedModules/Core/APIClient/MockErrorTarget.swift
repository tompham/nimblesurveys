//
//  MockRealErrorAPITarget.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import Moya

struct MockErrorTarget: TargetType {
    
    let customPath: String
    
    var baseURL: URL {
        return URL(string: "https://survey-api.nimblehq.co/api")!
    }
    
    var path: String {
        return customPath
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var headers: [String : String]? {
        return nil
    }
}
