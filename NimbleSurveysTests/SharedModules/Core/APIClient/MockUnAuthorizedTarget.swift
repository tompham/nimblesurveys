//
//  MockUnAuthorizedTarget.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import Moya

struct MockUnAuthorizedTarget: TargetType {
    
    var baseURL: URL {
        return URL(string: "https://survey-api.nimblehq.co/api")!
    }
    
    var path: String {
        return "v1/oauth/token"
    }
    
    var task: Task {
        return .requestParameters(
            parameters: [
                "grant_type": "password",
                "email": "phamanhtuancross@gmail.com",
                "password": "11111111",
            ],
            encoding: URLEncoding.httpBody
        )
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var headers: [String : String]? {
        return nil
    }
}
