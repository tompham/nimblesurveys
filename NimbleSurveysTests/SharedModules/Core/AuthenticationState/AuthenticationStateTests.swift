//
//  AuthenticationStateTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 06/01/2024.
//

import XCTest
import Cuckoo
@testable import NimbleSurveys

final class AuthenticationStateTests: XCTestCase {
    
    var sut: AuthenticationState!
    var credentialsProvider: MockCredentialsProviderProtocol!

    override func setUpWithError() throws {
        credentialsProvider = MockCredentialsProviderProtocol()
        sut = AuthenticationState(credentialsProvider: credentialsProvider)
    }

    override func tearDownWithError() throws {
        Cuckoo.reset(credentialsProvider)
        
        credentialsProvider = nil
        sut = nil
    }
    
    func testIsAuthenticatedWhenHasCredentialInStorage() {
        // Given:
        let credentials = makeCredentialsResponse(
            id: "mock_id",
            type: "token",
            accessToken: "access_toke",
            tokenType: "token",
            expiresIn: Date().timeIntervalSince1970,
            refreshToken: "refreshToken",
            createdAt: Date().timeIntervalSince1970
        )
        stubRetrieveCredentials(expectationResult: credentials)
        
        // When:
        let result = sut.isAuthenticated
        
        // Then:
        verify(credentialsProvider).retrieveCredentials()
        XCTAssertTrue(result)
    }
    
    func testIsAuthenticatedWhenHasNoCredentialInStorage() {
        // Given:
        stubRetrieveCredentials(expectationResult: nil)
        
        // When:
        let result = sut.isAuthenticated
        
        // Then:
        verify(credentialsProvider).retrieveCredentials()
        XCTAssertFalse(result)
    }
}

// MARK: - Privates

extension AuthenticationStateTests {
    
    func stubRetrieveCredentials(
        expectationResult: CredentialsResponse?
    ) {
        Cuckoo.stub(credentialsProvider) {
            when($0.retrieveCredentials()).thenReturn(expectationResult)
        }
    }
}
