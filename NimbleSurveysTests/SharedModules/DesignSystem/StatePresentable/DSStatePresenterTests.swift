//
//  DSStatePresenterTests.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 07/01/2024.
//

import XCTest
@testable import NimbleSurveys

final class DSStatePresenterTests: XCTestCase {

    var sut: DSStatePresenter!

    var stateContainerView: UIView!
    var loadingStateView: UIView!
    var emptyStateView: UIView!
    var errorStateView: UIView!

    override func setUp() {
        super.setUp()

        stateContainerView = makeContainerView()
        loadingStateView = makeLoadingView()
        errorStateView = makeErrorView()
        emptyStateView = makeEmptyView()

        sut = DSStatePresenter(
            stateContainerView: stateContainerView,
            loadingStateView: loadingStateView,
            emptyStateView: emptyStateView,
            errorStateView: errorStateView
        )
    }

    override func tearDown() {

        stateContainerView = nil
        loadingStateView = nil
        errorStateView = nil
        emptyStateView = nil
        sut = nil
        super.tearDown()
    }

    func testShowLoadingStateViewWhenStartLoading() {

        // Given:
        let expected = expectation(description: #function)

        // When:
        sut.startLoading(animated: false) {
            XCTAssertNotNil(self.sut.loadingStateView)
            XCTAssertEqual(self.sut.loadingStateView.isHidden, false)
            XCTAssertNotNil(self.sut.loadingStateView.superview)
            expected.fulfill()
        }

        // Then:
        waitForExpectations(timeout: 0.1)
    }

    func testShowErrorStateViewWhenEndLoadingWithError() {

        // Given:
        let expected = expectation(description: #function)
        sut.startLoading(animated: false, completionHandler: nil)

        // When:
        sut.endLoading(hasError: true, hasContent: false) {
            XCTAssertNotNil(self.sut.loadingStateView)
            XCTAssertNil(self.sut.loadingStateView.superview)
            XCTAssertNotNil(self.sut.errorStateView)
            XCTAssertNotNil(self.sut.errorStateView?.superview)
            expected.fulfill()
        }

        // Then:
        waitForExpectations(timeout: 0.1)
    }

    func testShowEmptyStateViewWhenLoadingReturnEmptyData() {

        // Given:
        let expected = expectation(description: #function)
        sut.startLoading(animated: false, completionHandler: nil)

        // When:
        sut.endLoading(hasError: false, hasContent: false) {
            XCTAssertNotNil(self.sut.loadingStateView)
            XCTAssertNil(self.sut.loadingStateView.superview)
            XCTAssertNotNil(self.sut.emptyStateView)
            XCTAssertNotNil(self.sut.emptyStateView?.superview)
            expected.fulfill()
        }

        // Then:
        waitForExpectations(timeout: 0.1)
    }

    func testErrorStateViewHiddenWhenLoadedSuccessfully() {
        // Given:
        let expected = expectation(description: #function)
        sut.startLoading(animated: false, completionHandler: nil)

        // When:
        sut.endLoading(hasError: false, hasContent: true) {
            XCTAssertNotNil(self.sut.loadingStateView)
            XCTAssertNil(self.sut.loadingStateView.superview)
            XCTAssertNotNil(self.sut.errorStateView)
            XCTAssertNil(self.sut.errorStateView?.superview)
            expected.fulfill()
        }

        // Then:
        waitForExpectations(timeout: 0.1)
    }

    func testEmptyStateViewHiddenWhenContentLoadedSuccessfullyAndNotEmpty() {
        // Given:
        let expected = expectation(description: #function)
        sut.startLoading(animated: false, completionHandler: nil)

        // When:
        sut.endLoading(hasError: false, hasContent: true) {
            XCTAssertNotNil(self.sut.loadingStateView)
            XCTAssertNil(self.sut.loadingStateView.superview)
            XCTAssertNotNil(self.sut.emptyStateView)
            XCTAssertNil(self.sut.emptyStateView?.superview)
            expected.fulfill()
        }

        // Then:
        waitForExpectations(timeout: 0.1)
    }

    func testErrorViewIsNotDisplayedIfItIsNotProvided() {
        // Given:
        let expected = expectation(description: #function)
        sut = DSStatePresenter(stateContainerView: stateContainerView, loadingStateView: loadingStateView)
        sut.startLoading(animated: false, completionHandler: nil)

        // When:
        sut.endLoading(hasError: true, hasContent: false){
            XCTAssertNotNil(self.sut.loadingStateView)
            XCTAssertNil(self.sut.loadingStateView.superview)
            XCTAssertNil(self.sut.errorStateView)
            expected.fulfill()
        }
        // Then:
        waitForExpectations(timeout: 0.1)
    }

    func testEmptyViewIsNotDisplayedIfItIsNotProvided() {
        // Given:
        let expected = expectation(description: #function)
        sut = DSStatePresenter(stateContainerView: stateContainerView, loadingStateView: loadingStateView)
        sut.startLoading(animated: false, completionHandler: nil)

        // When:
        sut.endLoading(hasError: true, hasContent: false) {
            XCTAssertNotNil(self.sut.loadingStateView)
            XCTAssertNil(self.sut.loadingStateView.superview)
            XCTAssertNil(self.sut.emptyStateView)
            expected.fulfill()
        }

        // Then:
        waitForExpectations(timeout: 0.1)
    }
}

extension DSStatePresenterTests {

    private func makeContainerView() -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    private func makeLoadingView() -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    private func makeErrorView() -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    private func makeEmptyView() -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

}
