//
//  TestHelper.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation

final class TestHelper: NSObject {
    
    public static func loadJsonData(forResource resource: String) throws -> Data {
        guard let resFileUrl = Bundle(for: TestHelper.classForCoder())
            .url(forResource: resource, withExtension: "json") else {
            throw NSError(domain: "Test", code: 0, userInfo: [NSLocalizedDescriptionKey: "Resource not found"])
        }
        return try Data(contentsOf: resFileUrl)
    }
}
