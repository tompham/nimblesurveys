//
//  Ultils.swift
//  NimbleSurveysTests
//
//  Created by Tuan Pham on 06/01/2024.
//

import Foundation
import XCTest
import Cuckoo
import Moya
import RxSwift
@testable import NimbleSurveys

extension CredentialsResponse: Matchable {
    public var matcher: ParameterMatcher<CredentialsResponse> {
        return ParameterMatcher { $0.id == self.id && $0.type == self.type }
    }
}

extension XCTestCase {
    
    func sameInstanceAs(_ target: LoginTarget) -> ParameterMatcher<TargetType> {
        return ParameterMatcher {
            guard let castedTarget = $0 as? LoginTarget else {
                return false
            }
            
            return castedTarget.email == target.email
            && castedTarget.password == target.password
        }
    }
    
    func sameInstanceAs(_ target: UserProfileTarget) -> ParameterMatcher<TargetType> {
        return ParameterMatcher {
            guard let castedTarget = $0 as? UserProfileTarget else {
                return false
            }
            
            return castedTarget.path == target.path
        }
    }
    
    func sameInstanceAs(_ target: FetchSurveyListTarget) -> ParameterMatcher<TargetType> {
        return ParameterMatcher {
            guard let castedTarget = $0 as? FetchSurveyListTarget else {
                return false
            }
            
            return castedTarget.pageSize == target.pageSize
            && castedTarget.pageNumber == target.pageNumber
        }
    }
    
    func sameInstanceAs(_ credentials: CredentialsResponse) -> ParameterMatcher<CredentialsResponse> {
        return ParameterMatcher { $0.id == credentials.id && $0.type == credentials.type }
    }
    
    func stubRequest(
        apiClient: MockAPIClientProtocol,
        target: ParameterMatcher<TargetType>,
        fromJSONFile filename: String
    ) {
        
        var data: Data!
        XCTAssertNoThrow(data = try TestHelper.loadJsonData(forResource: filename))
        stubRequest(apiClient: apiClient, target: target, expectationResult: .just(data))
    }
    
    func stubRequest(
        apiClient: MockAPIClientProtocol,
        target: ParameterMatcher<TargetType>,
        expectationResult: Single<Data>
    ) {
        Cuckoo.stub(apiClient) {
            when($0.request(target: target)).thenReturn(expectationResult)
        }
    }
    
    func stubEncode<T: Encodable>(
        encoder: MockEncoderProtocol,
        object: ParameterMatcher<T>,
        expectationData: Data
    ) {
        Cuckoo.stub(encoder) {
            when($0.encode(object)).thenReturn(expectationData)
        }
    }
    
    
    func stubShow(
        navigator: MockAppNavigatorProtocol,
        route: Route,
        from parent: UIViewController,
        action: NavigateAction
    ) {
        Cuckoo.stub(navigator) {
            when(
                $0.show(
                    route: route,
                    from: sameInstance(as: parent),
                    action: action
                )
            )
            .thenDoNothing()
        }
    }
    
    
    func makeUserProfileEntity(
        type: String,
        id: String,
        email: String? = nil,
        name: String? = nil,
        avatarUrl: String? = nil
    ) -> UserProfileEntity {
        
        return .init(
            type: type,
            id: id,
            email: email,
            name: name,
            avatarUrl: avatarUrl
        )
    }
    
    func makeCredentialsResponse(
        id: String,
        type: String,
        accessToken: String? = nil,
        tokenType: String? = nil,
        expiresIn: Double? = nil,
        refreshToken: String? = nil,
        createdAt: Double? = nil
    ) -> CredentialsResponse {
        return .init(
            id: id,
            type: type,
            accessToken: accessToken,
            tokenType: tokenType,
            expiresIn: expiresIn,
            refreshToken: refreshToken,
            createdAt: createdAt
        )
    }
    
    func makeSurveyResponse(
        id: String,
        type: String,
        title: String? = nil,
        description: String? = nil,
        isActive: Bool? = nil,
        coverImageUrl: String? = nil,
        surveyType: String? = nil
    ) -> SurveyResponse {
        return .init(
            id: id,
            type: type,
            title: title,
            description: description,
            isActive: isActive,
            coverImageUrl: coverImageUrl,
            surveyType: surveyType
        )
    }
    
    func makeSurveyEntity(
        title: String? = nil,
        description: String? = nil,
        isActive: Bool? = nil,
        coverImageUrl: String? = nil,
        surveyType: String? = nil
    ) -> SurveyEntity {
        return .init(
            title: title,
            description: description,
            isActive: isActive,
            coverImageUrl: coverImageUrl,
            surveyType: surveyType
        )
    }
}
