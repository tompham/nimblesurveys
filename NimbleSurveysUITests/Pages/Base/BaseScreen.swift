//
//  BaseScreen.swift
//  NimbleSurveysUITests
//
//  Created by Tuan Pham on 10/01/2024.
//

import XCTest


public enum ScreenError: Error {
    case expectElementTimeout(String)
}

open class BaseScreen {

    public enum Timeout: TimeInterval {
        case short = 1
        case medium = 10
        case long = 30
        case veryLong = 60
    }

    public let app: XCUIApplication

    public init(app: XCUIApplication) throws {
        self.app = app
    }

    public func LS(_ key: String) -> String {
        guard let path = Bundle(for: type(of: self)).path(forResource: "en", ofType: "lproj"),
              let bundle = Bundle(path: path) else { return key }
        return NSLocalizedString(key, bundle: bundle, comment: "")
    }

    public func expect(_ predicate: NSPredicate, element: XCUIElement, timeout: Timeout, file: String, line: UInt) throws {
        let expectation = XCTNSPredicateExpectation(predicate: predicate, object: element)
        let result = XCTWaiter.wait(for: [expectation], timeout: timeout.rawValue)

        if result == .timedOut {
            throw ScreenError.expectElementTimeout(expectation.description)
        }
    }

    public func tap(_ element: XCUIElement, timeout: Timeout = .medium) throws {
        if element.elementType == .button {
            try expectHittable(element, timeout: timeout)
            element.tap()
            return
        }

        element.forceTap()
    }

    public func dismissKeyboardIfPresent() {
        if app.keyboards.element(boundBy: 0).exists {
            if UIDevice.current.userInterfaceIdiom == .pad {
                app.keyboards.buttons["Hide keyboard"].tap()
            } else {
                app.toolbars.buttons["Done"].tap()
            }
        }
    }
}

// MARK: - Convenience Expectation UI element helpers
extension BaseScreen {
    public func expectExists(_ element: XCUIElement, timeout: Timeout = .medium, file: String = #file, line: UInt = #line) throws {
        try expect(condition: "exists == true", element: element, timeout: timeout, file: file, line: line)
    }
    public func expectNotExists(_ element: XCUIElement, timeout: Timeout = .medium, file: String = #file, line: UInt = #line) throws {
        try expect(condition: "exists == false", element: element, timeout: timeout, file: file, line: line)
    }
    public func expectHittable(_ element: XCUIElement, timeout: Timeout = .medium, file: String = #file, line: UInt = #line) throws {
        try expect(condition: "hittable == true", element: element, timeout: timeout, file: file, line: line)
    }
    public func expectNotHittable(_ element: XCUIElement, timeout: Timeout = .medium, file: String = #file, line: UInt = #line) throws {
        try expect(condition: "hittable == false", element: element, timeout: timeout, file: file, line: line)
    }
    public func expect(condition: String, element: XCUIElement, timeout: Timeout, file: String, line: UInt) throws {
        try expect(NSPredicate(format: condition), element: element, timeout: timeout, file: file, line: line)
    }
}

extension XCUIElement {

    func forceTap() {
        if self.isHittable {
            self.tap()
        } else {
            coordinate(withNormalizedOffset: CGVector(dx: 0.5, dy: 0.5)).tap()
        }
    }

}
