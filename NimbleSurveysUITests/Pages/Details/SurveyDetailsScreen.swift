//
//  SurveyDetailsScreen.swift
//  NimbleSurveysUITests
//
//  Created by Tuan Pham on 11/01/2024.
//

import XCTest

final class SurveyDetailsScreen: BaseScreen {
    
    private var titleLabel: XCUIElement {
        return app.staticTexts["details_title_label"].firstMatch
    }
    
    private var descriptionLabel: XCUIElement {
        return app.staticTexts["details_description_label"].firstMatch
    }

    private var startButton: XCUIElement {
        return app.buttons["details_start_button"].firstMatch
    }
    
    override init(app: XCUIApplication) throws {
        try super.init(app: app)
        try expectExists(titleLabel)
        try expectExists(descriptionLabel)
        try expectExists(startButton)
    }
}
