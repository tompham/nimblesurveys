//
//  ForgotPasswordScreen.swift
//  NimbleSurveysUITests
//
//  Created by Tuan Pham on 11/01/2024.
//

import XCTest

final class ForgotPasswordScreen: BaseScreen {

    var emailTextField: XCUIElement {
        return app.textFields["forgot_password_email_textfield"]
    }
    
    var resetButton: XCUIElement {
        return app.buttons["forgot_password_reset_button"]
    }
    
    override init(app: XCUIApplication) throws {
        try super.init(app: app)        
        try expectExists(emailTextField)
        try expectExists(resetButton)
    }
    
    func fillUp(email: String) -> Self {
        emailTextField.tap()
        emailTextField.typeText(email)
        return self
    }
    
    func tapReset() {
        resetButton.forceTap()
    }
}
