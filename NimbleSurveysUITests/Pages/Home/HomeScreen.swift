//
//  HomeScreen.swift
//  NimbleSurveysUITests
//
//  Created by Tuan Pham on 10/01/2024.
//

import XCTest

final class HomeScreen: BaseScreen {
    
    private var descTitleLabel: XCUIElement {
        return app.staticTexts["home_desc_title_label"].firstMatch
    }
    
    private var descDescriptionLabel: XCUIElement {
        return app.staticTexts["home_desc_description_label"].firstMatch
    }

    private var continueButton: XCUIElement {
        return app.buttons["home_desc_continue_button"].firstMatch
    }
    
    var collectionView: XCUIElement {
        return app.collectionViews["home_collection_view"].firstMatch
    }
    
    override init(app: XCUIApplication) throws {
        try super.init(app: app)
        try expectExists(descTitleLabel)
        try expectExists(descDescriptionLabel)
        try expectExists(continueButton)
        try expectExists(collectionView)
    }
    
    func verifyDescription(title: String, description: String) {
        XCTAssertNotNil(descTitleLabel.staticTexts[title])
        XCTAssertNotNil(descDescriptionLabel.staticTexts[description])
    }
    
    func tapContinue() {
        continueButton.forceTap()
    }
    
    func moveNextPage() {
        collectionView.swipeLeft(velocity: .fast)
    }
    
    func tapViewSurveyDetails() {
        continueButton.forceTap()
    }
}
