//
//  LoginScreen.swift
//  NimbleSurveysUITests
//
//  Created by Tuan Pham on 10/01/2024.
//

import XCTest

final class LoginScreen: BaseScreen {
    
    var emailTextField: XCUIElement {
        return app.textFields["login_email_textfield"]
    }
    
    var passwordTextField: XCUIElement {
        return app.secureTextFields["login_password_textfield"]
    }
    
    var loginButton: XCUIElement {
        return app.buttons["login_login_button"]
    }
    
    var forgotPasswordButton: XCUIElement {
        return app.buttons["login_forgot_password_button"]
    }
    
    override init(app: XCUIApplication) throws {
        try super.init(app: app)
        try expectExists(emailTextField)
        try expectExists(passwordTextField)
        try expectExists(loginButton)
        try expectExists(forgotPasswordButton)
    }
    
    @discardableResult
    func fillUp(email: String, password: String) -> Self {
        emailTextField.tap()
        emailTextField.typeText(email)
        
        passwordTextField.tap()
        passwordTextField.typeText(password)
        return self
    }
    
    func tapLogin() {
        loginButton.forceTap()
    }
    
    func tapForgotPassword() {
        forgotPasswordButton.forceTap()
    }
}
