//
//  BaseUITests.swift
//  NimbleSurveysUITests
//
//  Created by Tuan Pham on 10/01/2024.
//

import XCTest
import Swifter

class BaseUITests: XCTestCase {

    var app: XCUIApplication!
    var bundle: Bundle!
    var server: HttpServer!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        continueAfterFailure = false
        server = HttpServer()
        app = XCUIApplication()
        bundle = Bundle(for: type(of: self))
    }
    
    func startServer(_ port: UInt16 = 8080) {
        do {
            try server.start(port)
        } catch {
            XCTFail("Failed to start the server with error: \(error)")
        }
    }

    override func tearDownWithError() throws {
        
        server.stop()
        try super.tearDownWithError()
    }
}
