//
//  ForgotPasswordUITests.swift
//  NimbleSurveysUITests
//
//  Created by Tuan Pham on 11/01/2024.
//

import XCTest
import Swifter

final class ForgotPasswordUITests: BaseUITests {
    
    func testResetPassword() throws {
        // Given:
        let email = "phamanhtuancross@gmail.com"
        
        server["/api/v1/passwords"] = { _ in
            return self.makeHTTPResponse(filename: "stub_http_forgot_password_response")
        }
        startServer()

        try UITestLauncher(app: app)
            .launch()
        try LoginScreen(app: app).tapForgotPassword()
        
        let forgotPasswordScreen = try ForgotPasswordScreen(app: app)
            .fillUp(email: email)
        
        // When:
        forgotPasswordScreen.tapReset()
        
        // Then:
        XCTAssertNoThrow(try ForgotPasswordScreen(app: app))
    }
    
    func testResetPasswordError() throws {
        // Given:
        let email = "phamanhtuancross@gmail.com"
        
        server["/api/v1/passwords"] = { _ in
            return HttpResponse.badRequest(.none)
        }
        startServer()

        try UITestLauncher(app: app)
            .launch()
        try LoginScreen(app: app).tapForgotPassword()
        
        let forgotPasswordScreen = try ForgotPasswordScreen(app: app)
            .fillUp(email: email)
        
        // When:
        forgotPasswordScreen.tapReset()
        
        // Then:
        XCTAssertNoThrow(try ForgotPasswordScreen(app: app))
    }
}
