//
//  HomeUITests.swift
//  NimbleSurveysUITests
//
//  Created by Tuan Pham on 11/01/2024.
//

import XCTest
//import OHHTTPStubs

final class HomeUITests: BaseUITests {
    
    func testShowHomePage() throws {
        // Given:
        let email = "phamanhtuancross@gmail.com"
        let password = "12345678"
        
        server["/api/v1/oauth/token"] = { request in
            return self.makeHTTPResponse(filename: "stub_http_login_response")
        }
        server["/api/v1/surveys?page[number]=1&page[size]=5"] = { request in
            return self.makeHTTPResponse(filename: "stub_http_list_surveys")
        }
        server["/api/v1/me"] = { request in
            return self.makeHTTPResponse(filename: "stub_http_profile_response")
        }
        
        startServer()
        
        try UITestLauncher(app: app)
            .launch()
        
        try LoginScreen(app: app)
            .fillUp(email: email, password: password)
            .tapLogin()
        
        // When:
        _ = app.waitForExistence(timeout: 1)
        let homeScreen = try HomeScreen(app: app)
        
        // Then:
        homeScreen.verifyDescription(
            title: "FirstPage - UITesting - title",
            description: "FirstPage - UITesting - description"
        )
    }
    
    func testMoveToNextPage() throws {
        // Given:
        let email = "phamanhtuancross@gmail.com"
        let password = "12345678"
        
        server["/api/v1/oauth/token"] = { request in
            return self.makeHTTPResponse(filename: "stub_http_login_response")
        }
        server["/api/v1/surveys?page[number]=1&page[size]=5"] = { request in
            return self.makeHTTPResponse(filename: "stub_http_list_surveys")
        }
        server["/api/v1/me"] = { request in
            return self.makeHTTPResponse(filename: "stub_http_profile_response")
        }
        
        startServer()
        
        try UITestLauncher(app: app)
            .launch()
        
        try LoginScreen(app: app)
            .fillUp(email: email, password: password)
            .tapLogin()
        _ = app.waitForExistence(timeout: 1)
        let homeScreen = try HomeScreen(app: app)
        homeScreen.verifyDescription(
            title: "FirstPage - UITesting - title",
            description: "FirstPage - UITesting - description"
        )
        
        // When:
        homeScreen.moveNextPage()
        
        // Then:
        homeScreen.verifyDescription(
            title: "SecondPage - UITesting - title",
            description: "SecondPage - UITesting - description"
        )
    }
    
    func testViewSurveyDetails() throws {
        // Given:
        let email = "phamanhtuancross@gmail.com"
        let password = "12345678"
        
        server["/api/v1/oauth/token"] = { request in
            return self.makeHTTPResponse(filename: "stub_http_login_response")
        }
        server["/api/v1/surveys?page[number]=1&page[size]=5"] = { request in
            return self.makeHTTPResponse(filename: "stub_http_list_surveys")
        }
        server["/api/v1/me"] = { request in
            return self.makeHTTPResponse(filename: "stub_http_profile_response")
        }
        
        startServer()
        
        try UITestLauncher(app: app)
            .launch()
        
        try LoginScreen(app: app)
            .fillUp(email: email, password: password)
            .tapLogin()
        _ = app.waitForExistence(timeout: 1)
        let homeScreen = try HomeScreen(app: app)
        homeScreen.verifyDescription(
            title: "FirstPage - UITesting - title",
            description: "FirstPage - UITesting - description"
        )
        
        // When:
        homeScreen.tapViewSurveyDetails()
        
        // Then:
        XCTAssertNoThrow(try SurveyDetailsScreen(app: app))
        
    }
}
