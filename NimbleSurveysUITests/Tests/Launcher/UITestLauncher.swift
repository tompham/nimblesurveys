//
//  UITestLauncher.swift
//  NimbleSurveysUITests
//
//  Created by Tuan Pham on 10/01/2024.
//

import XCTest

final class UITestLauncher {

    let app: XCUIApplication
    private var launchConfigs = UITestLaunchConfigs()

    init(app: XCUIApplication) {
        self.app = app
        launchConfigs.toFreshStart = true
        launchConfigs.isTestingEnabled = true
    }
    
    @discardableResult
    func launch() throws -> BaseScreen {
        app.launchArguments += ["-AppleLanguages", "(en)"]
        app.launchArguments += launchConfigs.launchArguments()
        app.launch()

        return try BaseScreen(app: app)
    }
}
