//
//  LoginUITests.swift
//  NimbleSurveysUITests
//
//  Created by Tuan Pham on 10/01/2024.
//

import XCTest
import OHHTTPStubs
import Swifter

final class LoginUITests: BaseUITests {
    
    func testLogin() throws {
        // Given:
        let email = "phamanhtuancross@gmail.com"
        let password = "12345678"
        
        server["/api/v1/oauth/token"] = { request in
            return self.makeHTTPResponse(filename: "stub_http_login_response")
        }
        server["/api/v1/surveys?page[number]=1&page[size]=5"] = { request in
            return self.makeHTTPResponse(filename: "stub_http_list_surveys")
        }
        server["/api/v1/me"] = { request in
            return self.makeHTTPResponse(filename: "stub_http_profile_response")
        }
        
        startServer()
        
        try UITestLauncher(app: app)
            .launch()
        
        let loginScreen = try LoginScreen(app: app)
            .fillUp(email: email, password: password)
        
        
        // When:
        loginScreen.tapLogin()
        
        // Then:
        _ = app.waitForExistence(timeout: 1)
        XCTAssertNoThrow(try HomeScreen(app: app))
    }
    
    func testLoginFailure() throws {
        // Given:
        let email = "phamanhtuancross@gmail.com"
        let password = "12345678"
        
        server["/api/v1/oauth/token"] = { request in
            return HttpResponse.badRequest(.none)
        }
        
        startServer()
        
        try UITestLauncher(app: app)
            .launch()
        
        let loginScreen = try LoginScreen(app: app)
            .fillUp(email: email, password: password)
        
        
        // When:
        loginScreen.tapLogin()
        
        // Then:
        XCTAssertNoThrow(try LoginScreen(app: app))
    }
    
    
    func testForgotPassword() throws {
        // Given:
        try UITestLauncher(app: app)
            .launch()
        let loginScreen = try LoginScreen(app: app)
        
        // When:
        loginScreen.tapForgotPassword()
        
        // Then:
        XCTAssertNoThrow(try ForgotPasswordScreen(app: app))
    }
}
