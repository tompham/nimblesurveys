//
//  Utils.swift
//  NimbleSurveysUITests
//
//  Created by Tuan Pham on 11/01/2024.
//

import Foundation
import Swifter
import XCTest

@testable import NimbleSurveys

extension XCTestCase {
    
    public func makeHTTPResponse(filename: String) -> HttpResponse {
        var data: Data!
        XCTAssertNoThrow(data = try TestHelper.loadJsonData(forResource: filename))
        return .ok(.data(data, contentType: "application/json"))
    }
}
