#!/bin/sh

# Read parameters
PROJECT_DIR="$1"
PROJECT_NAME="$2"
PODS_ROOT="$3"

# Define output file. Change "$PROJECT_DIR/${PROJECT_NAME}Tests" to your test's root source folder, if it's not the default name.
OUTPUT_FILE="$PROJECT_DIR/${PROJECT_NAME}Tests/Mock/GeneratedMocks.swift"
echo "Generated Mocks File = $OUTPUT_FILE"

# Define input directory. Change "${PROJECT_DIR}/${PROJECT_NAME}" to your project's root source folder, if it's not the default name.
INPUT_DIR="${PROJECT_DIR}/${PROJECT_NAME}"
SRC_DIR="${INPUT_DIR}"

echo "Mocks Input Directory = $INPUT_DIR"

# add filename only if the file is in $SRC_DIR
FILES=( \
"DecoderProtocol.swift" \
"CredentialsProviderProtocol.swift" \
"RefreshTokenTargetProviderProtocol.swift" \
"APIClientProtocol.swift" \
"DecoderProtocol.swift" \
"EncoderProtocol.swift" \
"AuthenticationRepositoryProtocol.swift" \
"AuthenticationServiceProtocol.swift" \
"SurveyRepositoryProtocol.swift" \
"SurveyServiceProtocol.swift" \
"AppNavigatorProtocol.swift" \
"ErrorHandlerProtocol.swift" \
"DSMessagePresenterProtocol.swift" \
"DataValidatorProtocol.swift" \
)
#################
### procedure ###

INPUT_FILES=""

appendInputFiles () {
    FILE_DIR=$1
    shift
    FILES_LIST=("$@")
    for i in "${FILES_LIST[@]}"
    do
        FPS=()
        while IFS=  read -r -d $'\n'; do
            FPS+=("$REPLY")
        done <<<"$(find "${FILE_DIR}" -name "$i" -exec echo {} \;)"

        if [ -z "${FPS[@]}" ] ; then
            echo "Warning: \"$i\" can't be found. You can check it and remove the line in the script."
            continue
        fi

        SUBS=()
        for j in "${FPS[@]}"
        do
            SUBS+=("$j")
            INPUT_FILES="${INPUT_FILES} \"$j\""
        done

        if [ "${#SUBS[@]}" -ne "1" ] ; then
            echo "Warning: $i need be checked (count=${#SUBS[@]})"
            echo "         all of the result will be put into array."
            for j in "${FPS[@]}"
            do
                echo "    => path: $j"
            done
        fi
    done
}

appendInputFiles "${SRC_DIR}" "${FILES[@]}"

echo "Generating Mock Files for Unit Tests..."
CMD="\"${PODS_ROOT}/Cuckoo/run\" generate --testable \"${PROJECT_NAME}\" --no-header --no-timestamp --output \"${OUTPUT_FILE}\" ${INPUT_FILES}"
eval $CMD
echo "Generated Mock Files for Unit Tests = $OUTPUT_FILE"

# After running once, locate `GeneratedMocks.swift` and drag it into your Xcode test target group.
